def gv
pipeline {

    agent any

    tools {
        gradle 'Gradle'
    }


    environment {
        //Build
        VERSION = ''
        group = ''
        artifactId = ''

        // Nexus
        registry = "https://nexus.byteterm.de/"
        registryCredentials = 'ByteTerm-Nexus-Username'
    }
    stages {

        // init
        stage('init') {
            steps {
                script {
                    gv = load "jenkins.groovy"
                    VERSION = gv.getVersion()
                    group = gv.getGroup()
                    artifactId = gv.getArtifactId()

                    sh '''
                        touch gradle.properties
                        echo 'org.gradle.java.home=/usr/lib/jvm/java-17-oracle/' > gradle.properties
                       '''
                }
            }
        }

        // build
        stage('build') {
            steps {
                script {
                    gv.build()
                }
            }
        }

        // merge-request
        stage('merge-request') {
            when{
                expression {
                    BRANCH_NAME.startsWith('MR')
                }
            }

            steps {
                script {

                    def windows = "<project xmlns=\"http://maven.apache.org/POM/4.0.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd\">\n<modelVersion>4.0.0</modelVersion>\n<groupId>${group}</groupId>\n<artifactId>${artifactId}-windows</artifactId>\n<version>${VERSION}</version>\n<packaging>pom</packaging>\n</project>"
                    writeFile(file: "${artifactId}-windows-${VERSION}.pom", text: windows)

                    def linux = "<project xmlns=\"http://maven.apache.org/POM/4.0.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd\">\n<modelVersion>4.0.0</modelVersion>\n<groupId>${group}</groupId>\n<artifactId>${artifactId}-linux</artifactId>\n<version>${VERSION}</version>\n<packaging>pom</packaging>\n</project>"
                    writeFile(file: "${artifactId}-linux-${VERSION}.pom", text: linux)

                    def mac = "<project xmlns=\"http://maven.apache.org/POM/4.0.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd\">\n<modelVersion>4.0.0</modelVersion>\n<groupId>${group}</groupId>\n<artifactId>${artifactId}-macos</artifactId>\n<version>${VERSION}</version>\n<packaging>pom</packaging>\n</project>"
                    writeFile(file: "${artifactId}-macos-${VERSION}.pom", text: mac)

                    gv.deployPublic()
                }
            }
        }
    }
}