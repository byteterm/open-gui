# Open-Gui

## Using

### Setup Maven
If you want to use Maven I have something for you here.
Depending on which operating system you use, note that the native abbreviation must change.
You can see a list here:
- Windows: `open-gui-windows`
- Linux: `open-gui-linux`
- MacOs: `open-gui-macos`

You need to change the artifactID below with one of the natives you ar needed!
```xml
<dependency>
  <groupId>de.byteterm</groupId>
  <artifactId>open-gui-linux</artifactId>
  <version>1.0.1-SNAPSHOT</version>
</dependency>
```
### Setup Gradle
If you use Gradle let me help with this:
```groovy
//Absolute need.
repositories {
    maven {
        url = 'https://nexus.byteterm.de/repository/maven-public/'
    }
}


//This for windows x64 systems.
implementation 'de.byteterm:open-gui-windows:1.0.1-SNAPSHOT'

//This for linux x64 systems.
implementation 'de.byteterm:open-gui-linux:1.0.1-SNAPSHOT'

//This for macos x64 systems.
implementation 'de.byteterm:open-gui-macos:1.0.1-SNAPSHOT'
```

### Setup in classes
Here you can see how to use our library in a project.
Please note that our library must be `inherited` in order for it to work.
With the private constructor you then call the `launch` method which initializes the library.
As you can see, it's really easy!

```Java
public class YourMain extends OpenGUI {

    private YourMain(String[] args) {
        this.launch(args);
    }
    
    public static void main(String[] args) {
        new YourMain(args);
    }
    
} 
```

## Fonts

### General information

A font is used to change the general look of text.
You can use pretty much any font you want with us.
Unfortunately, only the `Font.registerFont(Font)` function can be used at the moment.
However, later you can also do this via css.
There are some important syntax formats that must be strictly adhered to! These are:

- The file name must be look like: `Fontname-face.ttf`

If a file name does not look like this, it will not be loaded.
Please do not create bug tickets dealing with something like this.
It is also important to note that our system only uses .ttf (TrueTypeFont) files! Loading emojis is currently not possible!

### Register a new font

```Java
public final class YourMain {

    public static void main(String[] args) {
        Font.registerFont(new Font("your/path", "fontName"));
    }

}
```

You can see it is very easy to register. You should only consider when you register a font that it is not yet designed.
Since every renderer can access it, care must be taken to work with a FontMeta class.
This allows us to beautify the font and thus also the text.
As soon as you register the font, all faces that are in the folder with the correct syntax are automatically registered too!
The registered name of your faces is then the name of the font - the name of the face instance, for example Regular.