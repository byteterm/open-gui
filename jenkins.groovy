def getVersion() {
    gradle_build = readProperties file:"${WORKSPACE}/build.gradle"
    return gradle_build['version'].replace("'", "")
}

def getGroup() {
    gradle_build = readProperties file:"${WORKSPACE}/build.gradle"
    return gradle_build['group'].replace("'", "")
}

def getArtifactId() {
    gradle_properties = readProperties file:"${WORKSPACE}/settings.gradle"
    return gradle_properties['rootProject.name'].replace("'", "")
}

def build() {
    sh 'gradle windowBuild'
    sh 'gradle linuxBuild'
    sh 'gradle macBuild'
}

def deployPublic() {
    repo = ''

    if (VERSION.endsWith("SNAPSHOT")) {
        repo = 'maven-snapshots'
    } else {
        repo = 'maven-releases'
    }

    nexusArtifactUploader(
            nexusVersion: 'nexus3',
            protocol: 'https',
            nexusUrl: 'nexus.byteterm.de',
            groupId: "$group",
            version: "$VERSION",
            repository: "$repo",
            credentialsId: "$registryCredentials",
            artifacts: [
                    [artifactId: "${artifactId}-windows",
                     classifier: '',
                     file: "build/libs/${artifactId}-windows-${VERSION}.jar",
                     type: 'jar'],
                    [artifactId: "${artifactId}-windows",
                     classifier: '',
                     file: "${artifactId}-windows-${VERSION}.pom",
                     type: 'pom'],
                    [artifactId: "${artifactId}-linux",
                     classifier: '',
                     file: "build/libs/${artifactId}-linux-${VERSION}.jar",
                     type: 'jar'],
                    [artifactId: "${artifactId}-linux",
                     classifier: '',
                     file: "${artifactId}-linux-${VERSION}.pom",
                     type: 'pom'],
                    [artifactId: "${artifactId}-macos",
                     classifier: '',
                     file: "build/libs/${artifactId}-macos-${VERSION}.jar",
                     type: 'jar'],
                    [artifactId: "${artifactId}-macos",
                     classifier: '',
                     file: "${artifactId}-macos-${VERSION}.pom",
                     type: 'pom']
            ]
    )
}

return this