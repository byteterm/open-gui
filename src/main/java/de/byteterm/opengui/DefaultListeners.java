package de.byteterm.opengui;

import de.byteterm.event.listener.Listener;
import de.byteterm.opengui.events.listener.ButtonDefaultListener;
import de.byteterm.opengui.events.listener.CheckBoxDefaultListener;

public enum DefaultListeners {

    BUTTON_LISTENER(new ButtonDefaultListener()),
    CHECKBOX_LISTENER(new CheckBoxDefaultListener());

    private final Listener toRegister;

    DefaultListeners(Listener toRegister) {
        this.toRegister = toRegister;
    }

    public Listener getToRegister() {
        return toRegister;
    }
}
