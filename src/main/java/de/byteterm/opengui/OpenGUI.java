package de.byteterm.opengui;

import de.byteterm.event.EventAPI;
import de.byteterm.event.listener.Listener;
import de.byteterm.jlogger.Logger;
import de.byteterm.opengui.events.listener.ButtonDefaultListener;
import de.byteterm.opengui.events.system.OpenGUIInitializeEvent;
import de.byteterm.opengui.exceptions.GLFWInitializeException;
import de.byteterm.opengui.system.Monitor;
import de.byteterm.opengui.system.OpenProperties;
import de.byteterm.opengui.system.OpenPropertiesBuilder;
import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * OpenGUI version 1.0.2
 *
 * OpenGUI is an open source library for creating graphics.
 * With our library it is possible to create a new GUI in a few seconds.
 * In order to work properly with the library, we recommend using our wiki, which you can find here: [LINK].
 * OpenGUI is a library that runs over LWJGL 3 and NanoVG.
 * This means that to use this library you have to meet the requirements of the other libraries!
 * Of course, some bugs can arise when using the library.
 * Please report crashes that can be traced back to your graphics card to the provider of the card.
 * If there is a mistake with the beginning [OG], you can report it to us!
 * Otherwise, have fun coding! :D
 * @author Daniel Ramke
 * @since 1.0.0
 */
@SuppressWarnings("unused")
public abstract class OpenGUI {

    private final Logger logger = Logger.getLogger();

    private static OpenProperties properties;

    /**
     * This method starts the library and create the first steps for the users.
     * This means the events a loading or the opengl instance will be created.
     * Without this method the library doesn't work.
     * @param arguments - program arguments
     */
    protected void launch(String[] arguments) {
        properties = new OpenPropertiesBuilder()
                .windowLimit(5)
                .debugMode(true)
                .build();
        this.initialize();
        this.loadMonitors();
        this.registerDefaultListeners();
        EventAPI.callEvent(new OpenGUIInitializeEvent(getClass(), Arrays.asList(arguments)));
    }

    private void registerDefaultListeners() {
        for (DefaultListeners listener : DefaultListeners.values()) {
            EventAPI.registerListener(listener.getToRegister());
        }
    }

    /**
     * This method initialized the library and change the properties for us.
     * Touched properties joml and java-awt.
     * LWJGL try to initialize GLFW.
     * @throws GLFWInitializeException - if the handler can't create a glfw state for us.
     * @see GLFW
     */
    private void initialize() {
        logger.info("Create GLFW statement...");
        if(!(GLFW.glfwInit())) {
            logger.fatal(new GLFWInitializeException("GLFW can't create instance of it self!"));
        }
        System.setProperty("joml.nounsafe", Boolean.TRUE.toString());
        System.setProperty("java.awt.headless", Boolean.TRUE.toString());
    }

    /**
     * This method creates instances of monitors.
     * You can get the monitors by calling Monitor.getMonitors().
     * If you wish to get a monitor by name you can use this method: Monitor.getMonitor(String identifier).
     */
    private void loadMonitors() {
        PointerBuffer buffer = GLFW.glfwGetMonitors();
        if(buffer == null) {
            logger.error("Can't create pointer for monitors... Abort action!");
            return;
        }
        for(int i = 0; i < buffer.capacity(); i++) {
            long address = buffer.get(i);
            boolean primary = (address == GLFW.glfwGetPrimaryMonitor());
            GLFWVidMode videoMode = GLFW.glfwGetVideoMode(address);
            if(videoMode == null) {
                logger.warn("Cannot found video modes for [ " + address + " ]");
                continue;
            }

            try(MemoryStack stack = MemoryStack.stackPush()) {
                IntBuffer physicalWidth = stack.callocInt(1);
                IntBuffer physicalHeight = stack.callocInt(1);
                GLFW.glfwGetMonitorPhysicalSize(address, physicalWidth, physicalHeight);

                int[] bits = new int[3];
                bits[0] = videoMode.redBits();
                bits[1] = videoMode.greenBits();
                bits[2] = videoMode.blueBits();

                new Monitor("screen-" + i, address, GLFW.glfwGetMonitorName(address)
                        , videoMode.width(), videoMode.height(), physicalWidth.get(), physicalHeight.get()
                        , videoMode.refreshRate(), bits, primary);
            }
        }
    }

    /**
     * @return OpenProperties - all configurations of this library instance.
     */
    public static OpenProperties getProperties() {
        return properties;
    }
}
