package de.byteterm.opengui;

import de.byteterm.jlogger.Logger;
import de.byteterm.opengui.graphics.font.Font;
import de.byteterm.opengui.graphics.paint.Image;
import de.byteterm.opengui.ui.Window;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Registry is the class for registering all used functions of this library.
 * Note that not only windows are created here, but also fonts, renderers and pipelines themselves.
 * Working with this class is more pleasant than writing everything yourself.
 * We also save this process in your projects.
 * If you want to register a new font you can do it here for all existing windows.
 * Note that this class should only be called to set or get information.
 * It is best not to change them to avoid mistakes.
 * @author Daniel Ramke
 * @since 1.0.0
 */
@SuppressWarnings("unused")
public final class Registry {

    private static final Logger logger = Logger.getLogger();

    private static final List<Window> registeredWindows = new ArrayList<>();
    private static final List<Image> registeredImages = new ArrayList<>();

    private static final List<Font> registeredFonts = new ArrayList<>();

    /*
     * *************************************************
     * Window registry
     * *************************************************
     */

    /**
     * This method creates and registered a new window.
     * @param identifier - the final name of the window and thread.
     * @param title - the title for the created window.
     * @param width - the dimensional width of the window.
     * @param height - the dimensional height of the window.
     * @param positionX - the desktop x location.
     * @param positionY - the desktop y location.
     * @param terminateAble - have the window the permission to abort the program by closing it?
     * @return Window - the created and finished window object.
     */
    public static Window createWindow(@NotNull String identifier, String title
            , double width, double height, double positionX, double positionY, boolean terminateAble) {
        return new Window(identifier, title, width, height, positionX, positionY, terminateAble);
    }

    /**
     * This method creates and registered a new window.
     * @param identifier - the final name of the window and thread.
     * @param title - the title for the created window.
     * @param width - the dimensional width of the window.
     * @param height - the dimensional height of the window.
     * @param positionX - the desktop x location.
     * @param positionY - the desktop y location.
     * @return Window - the created and finished window object.
     */
    public static Window createWindow(@NotNull String identifier, String title
            , double width, double height, double positionX, double positionY) {
        return createWindow(identifier, title, width, height, positionX, positionY, false);
    }

    /**
     * This method creates and registered a new window.
     * @param identifier - the final name of the window and thread.
     * @param title - the title for the created window.
     * @param width - the dimensional width of the window.
     * @param height - the dimensional height of the window.
     * @param terminateAble - have the window the permission to abort the program by closing it?
     * @return Window - the created and finished window object.
     */
    public static Window createWindow(@NotNull String identifier, String title
            , double width, double height, boolean terminateAble) {
        return createWindow(identifier, title, width, height, -1, -1, terminateAble);
    }

    /**
     * This method creates and registered a new window.
     * @param identifier - the final name of the window and thread.
     * @param title - the title for the created window.
     * @param width - the dimensional width of the window.
     * @param height - the dimensional height of the window.
     * @return Window - the created and finished window object.
     */
    public static Window createWindow(@NotNull String identifier, String title
            , double width, double height) {
        return createWindow(identifier, title, width, height, false);
    }

    /**
     * This method creates and registered a new window.
     * @param identifier - the final name of the window and thread.
     * @param title - the title for the created window.
     * @return Window - the created and finished window object.
     */
    public static Window createWindow(@NotNull String identifier, String title) {
        return createWindow(identifier, title, 800, 600);
    }

    /**
     * This method registered new windows by the identifier.
     * You can't use duplicated identifier names!
     * @param window - the window which need registering.
     */
    public static void registerWindow(@NotNull Window window) {
        if(getWindow(window.getIdentifier()) != null) {
            logger.warn("Window [ " + window.getIdentifier() + " ] is already exists! Can't create a new one with the same name!");
            return;
        }
        registeredWindows.add(window);
    }

    /**
     * This method searched for a window in the windows list.
     * @param identifier - the search key.
     * @return window - which was founded by the given key.
     */
    public static Window getWindow(@NotNull String identifier) {
        Window window = null;
        for(Window windows : registeredWindows) {
            if(windows.getIdentifier().equals(identifier)) {
                window = windows;
                break;
            }
        }
        return window;
    }

    /**
     * This method searched for a window in the windows list.
     * @param windowID - the search key.
     * @return window - which was founded by the given key.
     */
    public static Window getWindow(long windowID) {
        Window window = null;
        for(Window windows : registeredWindows) {
            if(windows.getWindowID() == windowID) {
                window = windows;
                break;
            }
        }
        return window;
    }

    /**
     * @return List<Window> - the complete window list.
     */
    public static List<Window> getRegisteredWindows() {
        return registeredWindows;
    }

    /*
     * *************************************************
     * Image registry
     * *************************************************
     */

    /**
     * This method registered a new image.
     * This image can be created in all classes you wish.
     * But note that this method is called in the image constructor.
     * @param image - to register image object.
     */
    public static void registerImage(@NotNull Image image) {
        if(getImage(image.getName()) != null) {
            logger.warn("Image [ " + image.getName() + " ] already exist!");
            return;
        }
        registeredImages.add(image);
    }

    /**
     * This method returned a registered image object by the given name.
     * @param name - the final name from the image. Is the file name without extension.
     * @return image - the founded image object, can be null if it wasn't found by name.
     */
    public static Image getImage(@NotNull String name) {
        Image image = null;
        for(Image images : registeredImages) {
            if(images.getName().equals(name)) {
                image = images;
                break;
            }
        }
        return image;
    }

    /**
     * @return List<Image> - the complete list of registered images.
     */
    public static List<Image> getRegisteredImages() {
        return registeredImages;
    }

    /*
     * *************************************************
     * Font registry
     * *************************************************
     */

    /**
     * This method registered a new font, the font as true font type.
     * The font can than used in all actives windows which ar registered.
     * @param font - the new font object, is ignored if the font was found in the list!
     */
    public static void registerFont(Font font) {
        if(isFontRegistered(font)) {
            return;
        }
        if(font == null) {
            logger.warn("You can't register a null font!");
            return;
        }
        logger.debug("Registered new font [ " + font.getName() + " ]");
    }

    /**
     * This method checks the font state.
     * @param font - font to check.
     * @return boolean - true if the font was found.
     */
    public static boolean isFontRegistered(Font font) {
        boolean state = false;
        if(font == null) {
            return false;
        }
        if(!registeredFonts.isEmpty()) {
            for(Font fonts : registeredFonts) {
                if(fonts.getName().equals(font.getName())) {
                    state = true;
                    break;
                }
            }
        }
        return state;
    }

    /**
     * This method searched for the given font name.
     * @param name - the name of font.
     * @return Font - Fallback font Default if the name was not found.
     */
    public static Font getFont(String name) {
        Font font = Font.FALLBACK;
        if(!registeredFonts.isEmpty()) {
            for (Font fonts : registeredFonts) {
                if(fonts.getName().equals(name)) {
                    font = fonts;
                    break;
                }
            }
        }
        return font;
    }

    /**
     * @return Font - the general main fallback font.
     */
    public static Font getFallbackFont() {
        return Font.FALLBACK;
    }

    /**
     * @return List<Font> - the registered fonts as list.
     */
    public static List<Font> getRegisteredFonts() {
        return registeredFonts;
    }
}
