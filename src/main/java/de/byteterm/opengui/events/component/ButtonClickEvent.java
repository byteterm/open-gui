package de.byteterm.opengui.events.component;

import de.byteterm.event.Event;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.system.input.Action;
import de.byteterm.opengui.system.input.Input;
import de.byteterm.opengui.ui.Component;
import de.byteterm.opengui.ui.Window;

/**
 * This class is a subclass of the event class.
 * The ButtonClickEvent is called when a mouse click was detected.
 * It can be used to interact with objects like components or windows.
 * Note that this event is called in the window class, by #mouseButtonCallback().
 * For more information to events for this library you can visit our website: [LINK].
 * @author Daniel Ramke
 * @since 1.0.1
 * @see Event
 */
public class ButtonClickEvent extends Event {

    private final Window window;
    private final Component component;

    private final Input input;
    private final Action action;
    private final int mods;

    /**
     * This constructor is called if a mouse button was pressed or released.
     * @param windowID - the window which detect the input.
     * @param component - the component which was triggered.
     * @param button - the button code.
     * @param action - the action code.
     * @param mods - the mod code.
     */
    public ButtonClickEvent(long windowID, Component component, int button, int action, int mods) {
        this.window = Registry.getWindow(windowID);
        this.component = component;
        this.input = Input.get(button);
        this.action = Action.get(action);
        this.mods = mods;
    }

    /**
     * @return Window - the caller.
     */
    public Window getWindow() {
        return window;
    }

    /**
     * @return Component - the triggered component.
     */
    public Component getComponent() {
        return component;
    }

    /**
     * @return Input - the input data.
     */
    public Input getInput() {
        return input;
    }

    /**
     * @return Action - the action data.
     */
    public Action getAction() {
        return action;
    }

    /**
     * @return integer - the glfw mod id code.
     */
    public int getMods() {
        return mods;
    }
}
