package de.byteterm.opengui.events.component;

import de.byteterm.event.Event;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.ui.Component;
import de.byteterm.opengui.ui.Window;

/**
 * This class is a subclass of the event class.
 * The ComponentScrollEvent is called when a mouse wheel rotate was detected.
 * It can be used to interact with objects like components or windows.
 * Note that this event is called in the window class, by #scrollCallback().
 * For more information to events for this library you can visit our website: [LINK].
 * @author Daniel Ramke
 * @since 1.0.2
 * @see Event
 */
public class ComponentScrollEvent extends Event {

    private final Window window;
    private final Component component;

    private final double offsetX;
    private final double offsetY;

    /**
     * This constructor is called if a mouse scroll input send.
     * @param windowID - the window which detect the action.
     * @param component - the component which was hovered or selected.
     * @param offsetX - the x offset of the scroll wheel.
     * @param offsetY - the y offset of the scroll wheel.
     */
    public ComponentScrollEvent(long windowID, Component component, double offsetX, double offsetY) {
        this.window = Registry.getWindow(windowID);
        this.component = component;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }

    /**
     * @return Window - which detect the action.
     */
    public Window getWindow() {
        return window;
    }

    /**
     * @return Component - which interact with the scroll input.
     */
    public Component getComponent() {
        return component;
    }

    /**
     * @return double - current x scroll offset.
     */
    public double getOffsetX() {
        return offsetX;
    }

    /**
     * @return double - current y scroll offset.
     */
    public double getOffsetY() {
        return offsetY;
    }
}
