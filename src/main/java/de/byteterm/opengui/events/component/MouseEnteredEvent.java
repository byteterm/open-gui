package de.byteterm.opengui.events.component;

import de.byteterm.event.Event;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.ui.Component;
import de.byteterm.opengui.ui.Window;

/**
 * This class is a subclass of the event class.
 * The MouseEnteredEvent is called when the mouse location is entered a bounds.
 * It can be used to interact with objects like components or windows.
 * Note that this event is called in the window class, by #mousePositionCallback().
 * For more information to events for this library you can visit our website: [LINK].
 * @author Daniel Ramke
 * @since 1.0.1
 * @see Event
 */
public class MouseEnteredEvent extends Event {

    private final Window window;
    private final Component component;

    /**
     * This constructor is called by cursor entered the component.
     * @param component - the component which detect the entered state.
     * @param windowID - the window mem address.
     */
    public MouseEnteredEvent(long windowID, Component component) {
        this.window = Registry.getWindow(windowID);
        this.component = component;
    }

    /**
     * @return Window - holder of the entered component.
     */
    public Window getWindow() {
        return window;
    }

    /**
     * @return Component - the component which was entered.
     */
    public Component getComponent() {
        return component;
    }
}
