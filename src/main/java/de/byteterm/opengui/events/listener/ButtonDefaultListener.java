package de.byteterm.opengui.events.listener;

import de.byteterm.event.listener.Listener;
import de.byteterm.event.listener.annotation.HandleEvent;
import de.byteterm.opengui.events.component.MouseEnteredEvent;
import de.byteterm.opengui.events.component.MouseLeavingEvent;
import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.style.Style;
import de.byteterm.opengui.ui.Component;
import de.byteterm.opengui.ui.control.Button;

/**
 * @author Daniel Ramke
 * @since 1.0.2
 */
public class ButtonDefaultListener implements Listener {

    @HandleEvent
    public void onMouseEntered(MouseEnteredEvent event) {
        Component component = event.getComponent();
        if(component instanceof Button button) {
            Style style = button.getStyle();
            style.setBackgroundColor(Color.WHITE);
            button.setTextColor(Color.BLACK);
        }
    }

    @HandleEvent
    public void onMouseLeaving(MouseLeavingEvent event) {
        Component component = event.getComponent();
        if(component instanceof Button button) {
            Style style = button.getStyle();
            style.setBackgroundColor(Color.transparent);
            button.setTextColor(Color.WHITE);
        }
    }

}
