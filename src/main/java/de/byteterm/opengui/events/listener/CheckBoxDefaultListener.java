package de.byteterm.opengui.events.listener;

import de.byteterm.event.listener.Listener;
import de.byteterm.event.listener.annotation.HandleEvent;
import de.byteterm.opengui.events.component.ButtonClickEvent;
import de.byteterm.opengui.system.input.Action;
import de.byteterm.opengui.ui.Component;
import de.byteterm.opengui.ui.control.CheckBox;

/**
 * @author Daniel Ramke
 * @since 1.0.2
 */
public class CheckBoxDefaultListener implements Listener {

    @HandleEvent
    public void onButtonClick(ButtonClickEvent event) {
        if(event.getAction().equals(Action.PRESS)) {
            Component component = event.getComponent();
            if (component instanceof CheckBox box) {
                box.setMarked(!box.isMarked());
            }
        }
    }

}
