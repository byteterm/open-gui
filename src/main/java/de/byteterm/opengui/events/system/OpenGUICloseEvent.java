package de.byteterm.opengui.events.system;

import de.byteterm.event.Event;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.ui.Window;

/**
 * This event is called if the library is ready to shut down.
 * Note that this event is called by the terminateAble window.
 * @author Daniel Ramke
 * @since 1.0.0
 * @see Event
 */
public class OpenGUICloseEvent extends Event {

    private final Window caller;

    /**
     * This constructor is triggered when the library will close.
     * @param caller the window witch called the shot down command.
     */
    public OpenGUICloseEvent(long caller) {
        this.caller = Registry.getWindow(caller);
    }

    /**
     * @return the trigger window.
     */
    public Window getCaller() {
        return this.caller;
    }


}
