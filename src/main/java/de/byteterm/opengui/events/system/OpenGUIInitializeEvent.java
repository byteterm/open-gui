package de.byteterm.opengui.events.system;

import de.byteterm.event.Event;
import de.byteterm.opengui.OpenGUI;

import java.util.List;

/**
 * This event is called at the first time by starting the library.
 * It can be used for grabbing information of the library.
 * @author Daniel Ramke
 * @since 1.0.2
 * @see Event
 */
public class OpenGUIInitializeEvent extends Event {

    private final List<String> arguments;

    private final Class<? extends OpenGUI> classObject;
    private final String className;

    /**
     * This constructor is called by the simple call trigger EventAPI.
     * @param classObject - the class which used this library.
     * @param arguments - the given java program arguments as list.
     */
    public OpenGUIInitializeEvent(Class<? extends OpenGUI> classObject, List<String> arguments) {
        this.classObject = classObject;
        this.className = classObject != null ? classObject.getSimpleName() : "no-class";
        this.arguments = arguments;
    }

    /**
     * @return Class<? extends OpenGUI> - the caller class of the library.
     */
    public Class<? extends OpenGUI> getClassObject() {
        return classObject;
    }

    /**
     * @return String - the caller class name.
     */
    public String getClassName() {
        return className;
    }

    /**
     * @return List<String> - the list of the java program arguments.
     */
    public List<String> getArguments() {
        return arguments;
    }
}
