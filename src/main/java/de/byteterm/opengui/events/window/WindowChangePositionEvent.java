package de.byteterm.opengui.events.window;

import de.byteterm.event.Event;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.ui.Window;

/**
 * This event is called if a window change his position.
 * The call can be triggered by moving or setting a new position.
 * @author Daniel Ramke
 * @since 1.0.0
 * @see Event
 */
public class WindowChangePositionEvent extends Event {

    private final Window window;
    private final double positionX;
    private final double positionY;
    private final double oldX;
    private final double oldY;

    /**
     * This constructor is called by the event trigger in window class.
     * @param windowID the called window.
     * @param positionX the window new x position.
     * @param positionY the window new y position.
     * @param oldX the storage old x position.
     * @param oldY the storage old y position.
     */
    public WindowChangePositionEvent(long windowID, double positionX, double positionY, double oldX, double oldY) {
        this.window = Registry.getWindow(windowID);
        this.positionX = positionX;
        this.positionY = positionY;
        this.oldX = oldX;
        this.oldY = oldY;
    }

    /**
     * @return the called window.
     */
    public Window getWindow() {
        return this.window;
    }

    /**
     * @return the new x position.
     */
    public double getPositionX() {
        return this.positionX;
    }

    /**
     * @return the new y position.
     */
    public double getPositionY() {
        return this.positionY;
    }

    /**
     * @return the old x position.
     */
    public double getOldX() {
        return this.oldX;
    }

    /**
     * @return the old y position.
     */
    public double getOldY() {
        return this.oldY;
    }
}

