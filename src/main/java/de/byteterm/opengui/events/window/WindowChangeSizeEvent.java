package de.byteterm.opengui.events.window;

import de.byteterm.event.Event;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.ui.Window;

/**
 * This event is called if a window change his size.
 * The call can be triggered by set a new size.
 * @author Daniel Ramke
 * @since 1.0.0
 * @see Event
 */
public class WindowChangeSizeEvent extends Event {

    private final Window window;
    private final double width;
    private final double height;

    /**
     * This constructor is called by the event trigger in window class.
     * @param windowID the called window.
     * @param width the window width.
     * @param height the window height.
     */
    public WindowChangeSizeEvent(long windowID, double width, double height) {
        this.window = Registry.getWindow(windowID);
        this.width = width;
        this.height = height;
    }

    /**
     * @return the called window.
     */
    public Window getWindow() {
        return this.window;
    }

    /**
     * @return the current width of the window.
     */
    public double getWidth() {
        return this.width;
    }

    /**
     * @return the current height of the window.
     */
    public double getHeight() {
        return this.height;
    }
}

