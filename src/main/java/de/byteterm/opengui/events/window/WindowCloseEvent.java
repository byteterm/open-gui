package de.byteterm.opengui.events.window;

import de.byteterm.event.Event;
import de.byteterm.event.EventAPI;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.events.system.OpenGUICloseEvent;
import de.byteterm.opengui.ui.Window;

/**
 * This event is called if a window try to close.
 * After call the event the window is finished with the process.
 * @author Daniel Ramke
 * @since 1.0.0
 * @see Event
 */
public class WindowCloseEvent extends Event {

    private final Window window;

    private final boolean terminateAble;

    /**
     * This constructor is triggered when the window will close.
     * @param windowID the window witch will close.
     */
    public WindowCloseEvent(long windowID) {
        this.window = Registry.getWindow(windowID);
        this.terminateAble = window.isTerminateAble();
        if(terminateAble) {
            EventAPI.callEvent(new OpenGUICloseEvent(windowID));
        }
    }

    /**
     * @return the closed window.
     */
    public Window getWindow() {
        return this.window;
    }

    /**
     * @return state of terminateAble, true the program will abort.
     */
    public boolean isTerminateAble() {
        return this.terminateAble;
    }
}

