package de.byteterm.opengui.events.window;

import de.byteterm.event.Event;
import de.byteterm.opengui.ui.Window;

/**
 * This event is called by the creation of a new window.
 * The state is reached by the end of the window #initialize() method.
 * @author Daniel Ramke
 * @since 1.0.0
 * @see Event
 */
public class WindowCreatedEvent extends Event {

    private final String identifier;

    private final Window created;

    /**
     * This constructor is triggered by window creations.
     * @param identifier the final name of the window thread.
     * @param created the created window object.
     */
    public WindowCreatedEvent(String identifier, Window created) {
        this.identifier = identifier;
        this.created = created;
    }

    /**
     * @return the final name of the window witch was created.
     */
    public String getIdentifier() {
        return this.identifier;
    }

    /**
     * @return the created window object.
     */
    public Window getCreated() {
        return this.created;
    }


}
