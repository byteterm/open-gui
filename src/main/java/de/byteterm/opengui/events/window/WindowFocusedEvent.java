package de.byteterm.opengui.events.window;

import de.byteterm.event.Event;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.ui.Window;

/**
 * This event is called if a window set on focus.
 * The call can be triggered by set the focus by method call and by operating system the window.
 * @author Daniel Ramke
 * @since 1.0.0
 * @see Event
 */
public class WindowFocusedEvent extends Event {

    private final Window window;
    private final boolean focused;

    /**
     * This constructor is called by the event trigger in window class.
     * @param windowID the called window.
     * @param focused the current focus state.
     */
    public WindowFocusedEvent(long windowID, boolean focused) {
        this.window = Registry.getWindow(windowID);
        this.focused = focused;
    }

    /**
     * @return the called window.
     */
    public Window getWindow() {
        return this.window;
    }

    /**
     * @return the current focus state.
     */
    public boolean isFocused() {
        return this.focused;
    }
}

