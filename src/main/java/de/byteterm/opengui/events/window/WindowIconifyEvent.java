package de.byteterm.opengui.events.window;

import de.byteterm.event.Event;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.ui.Window;

/**
 * This event is called if a window set on iconified.
 * The call can be triggered by set the iconified by method call and by operating system the window.
 * @author Daniel Ramke
 * @since 1.0.0
 * @see Event
 */
public class WindowIconifyEvent extends Event {

    private final Window window;
    private final boolean iconified;

    /**
     * This constructor is called by the event trigger in window class.
     * @param windowID the called window.
     * @param iconified the current iconify state.
     */
    public WindowIconifyEvent(long windowID, boolean iconified) {
        this.window = Registry.getWindow(windowID);
        this.iconified = iconified;
    }

    /**
     * @return the called window.
     */
    public Window getWindow() {
        return this.window;
    }

    /**
     * @return the current iconify state.
     */
    public boolean isIconified() {
        return this.iconified;
    }
}

