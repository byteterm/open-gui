package de.byteterm.opengui.events.window;

import de.byteterm.event.Event;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.ui.Window;

/**
 * This event is called if a window set on focus.
 * The call can be triggered by set the maximized by method call and by operating system the window.
 * @author Daniel Ramke
 * @since 1.0.0
 * @see Event
 */
public class WindowMaximizedEvent extends Event {

    private final Window window;
    private final boolean maximized;

    /**
     * This constructor is called by the event trigger in window class.
     * @param windowID the called window.
     * @param maximized the current maximize state.
     */
    public WindowMaximizedEvent(long windowID, boolean maximized) {
        this.window = Registry.getWindow(windowID);
        this.maximized = maximized;
    }

    /**
     * @return the called window.
     */
    public Window getWindow() {
        return this.window;
    }

    /**
     * @return the current maximize state.
     */
    public boolean isMaximized() {
        return this.maximized;
    }
}

