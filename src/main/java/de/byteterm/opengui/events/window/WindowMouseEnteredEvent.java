package de.byteterm.opengui.events.window;

import de.byteterm.event.Event;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.ui.Window;

/**
 * This event is called if the mouse entered the window.
 * The detection is by hitting the outer bounds of the window.
 * @author Daniel Ramke
 * @since 1.0.0
 * @see Event
 */
public class WindowMouseEnteredEvent extends Event {

    private final Window window;
    private final boolean entered;

    /**
     * This constructor is called if the mouse hitting the window outer bounds.
     * @param windowID - the window which detect the bounds hitting.
     * @param state - the state of the mouse.
     */
    public WindowMouseEnteredEvent(long windowID, boolean state) {
        this.window = Registry.getWindow(windowID);
        this.entered = state;
    }

    /**
     * @return Window - the current used window.
     */
    public Window getWindow() {
        return window;
    }

    /**
     * @return boolean - the state of the mouse.
     */
    public boolean isEntered() {
        return entered;
    }
}
