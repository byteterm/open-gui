package de.byteterm.opengui.events.window;

import de.byteterm.event.Event;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.ui.Window;

/**
 * This event is called if a window was refreshed.
 * The call can be triggered by change maximize or iconify.
 * @author Daniel Ramke
 * @since 1.0.0
 * @see Event
 */
public class WindowRefreshEvent extends Event {

    private final Window window;

    /**
     * This constructor is called by the event trigger in window class.
     *
     * @param windowID the called window.
     */
    public WindowRefreshEvent(long windowID) {
        this.window = Registry.getWindow(windowID);
    }

    /**
     * @return the called window.
     */
    public Window getWindow() {
        return this.window;
    }
}

