package de.byteterm.opengui.example;

import de.byteterm.jlogger.Logger;
import de.byteterm.opengui.OpenGUI;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.graphics.paint.Gradient;
import de.byteterm.opengui.style.types.Background;
import de.byteterm.opengui.tools.formal.Insets;
import de.byteterm.opengui.tools.formal.Radius;
import de.byteterm.opengui.tools.geometry.Alignment;
import de.byteterm.opengui.ui.View;
import de.byteterm.opengui.ui.Window;
import de.byteterm.opengui.ui.control.AvatarView;
import de.byteterm.opengui.ui.control.Button;
import de.byteterm.opengui.ui.control.CheckBox;
import de.byteterm.opengui.ui.shapes.Rectangle;

public class ExperimentalWindow extends OpenGUI {

    private static final Logger logger = Logger.getLogger();

    private ExperimentalWindow(String[] args) {
        this.launch(args);
    }

    public static void main(String[] args) {
        new ExperimentalWindow(args);

        Window window = Registry.createWindow("DebugWindow", "Experimental Window");

        Rectangle main = new Rectangle();
        main.setSize(800, 600);
        main.setStretchSizeToParent(true);
        Gradient gradient = new Gradient(Color.ORANGE, Color.PURPLE);
        gradient.setAngle(Gradient.Direction.TOP_LEFT_TO_BOTTOM_RIGHT);
        main.getStyle().setBackgroundGradient(gradient);

        Rectangle frame = new Rectangle();
        frame.setSize(400, 500);
        frame.getStyle().setBackgroundColor(Color.rgba(0, 0, 0, 0.5));
        frame.getStyle().setBackgroundRadius(new Radius(20));
        frame.setAlignment(Alignment.CENTER);

        Button button = new Button("Sign Up");
        button.setAlignment(Alignment.NOTHING);
        button.setSize(200, 50);
        button.setFontSize(20);
        button.setTextColor(Color.WHITE);
        button.getStyle().setBackgroundColor(Color.transparent);
        button.getStyle().setBorderColor(Color.WHITE);
        button.getStyle().setBorderStroke(new Insets(1));
        button.setLayoutPosition(100, 275);

        AvatarView avatarView = new AvatarView();
        avatarView.setAlignment(Alignment.NOTHING);
        avatarView.setSize(150, 150);
        avatarView.setLayoutPosition(125, 25);
        Gradient profileGradient = Gradient.get(Color.PURPLE, Color.CYAN);
        profileGradient.setAngle(Gradient.Direction.TOP_LEFT_TO_BOTTOM_RIGHT);
        avatarView.genImageByUserName("Felix", "Jaeger", Background.get(profileGradient), Color.WHITE);
        avatarView.setShape(View.ViewForm.CIRCLE);

        CheckBox checkBox = new CheckBox("Remember me", false);
        checkBox.setAlignment(Alignment.NOTHING);
        checkBox.setLayoutPosition(100, 400);
        checkBox.setSize(200, 50);
        checkBox.setMarkedSize(35);
        checkBox.setFontSize(20);
        checkBox.setMarked(true);
        checkBox.getStyle().setBackgroundColor(Color.transparent);

        main.addChild(frame);
        frame.addChild(avatarView);
        frame.addChild(button);
        frame.addChild(checkBox);

        window.addComponent(main);
    }


}
