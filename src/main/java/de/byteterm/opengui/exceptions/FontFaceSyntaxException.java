package de.byteterm.opengui.exceptions;

/**
 * This is the exception when a Font fond by checking faces a syntax error.
 * If you have throwing this exception than check your font folder or checkout
 * our website: [LINK] for mor information.
 * @author Daniel Ramke
 * @since 1.0.1
 * @see Throwable
 */
public class FontFaceSyntaxException extends Throwable {

    /**
     * This constructor overrides the current throwable to set an own exception name.
     * @param message the error message which is sent too.
     */
    public FontFaceSyntaxException(String message) {
        super(message);
    }


}
