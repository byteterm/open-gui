package de.byteterm.opengui.exceptions;

/**
 * This is the exception when a OpenGUI instance can't be initialized.
 * If you have throwing this exception than check your Java version or checkout
 * our website: [LINK] for mor information.
 * @author Daniel Ramke
 * @since 1.0.2
 * @see Throwable
 * @see RuntimeException
 */
public class GLFWInitializeException extends RuntimeException {

    /**
     * Override constructor.
     * The program will abort after this throw.
     * @param message - the error message.
     */
    public GLFWInitializeException(String message) {
        super(message);
    }

}
