package de.byteterm.opengui.graphics;

import de.byteterm.opengui.ui.Component;
import de.byteterm.opengui.ui.Scene;
import de.byteterm.opengui.ui.Window;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

/**
 * This abstract class controls all UI renderers for us.
 * The default and abstract methods will help for the correct performance.
 * If you try to create your own renderer which used our window class, then use our Renderer interface instanceof this.
 * If you use this class you a needed all your graphic objects as component.
 * This is not recommended and use to many resources.
 * For more information visit our website: [LINK].
 * @author Daniel Ramke
 * @since 1.0.1
 * @see Renderer
 */
public abstract class AbstractRenderer implements Renderer {

    private final List<Component> toRender;
    private static final List<String> names = new ArrayList<>();

    private final String name;
    private RenderPriority priority;
    private boolean initialized;
    private boolean changeDetect;
    private Window window;

    /**
     * This created an empty constructor.
     */
    public AbstractRenderer() {
        this(null);
    }

    /**
     * This constructor created a new object of a renderer.
     * You can choose your wish name to find the renderer better.
     * @param name - the final render name.
     */
    public AbstractRenderer(String name) {
        this.name = name(name);
        this.initialized = false;
        this.changeDetect = false;
        this.toRender = new ArrayList<>();
    }

    /**
     * This method checks the name if it duplicated.
     * @param name - to checked.
     * @return string - the result of the checking.
     */
    private String name(String name) {
        String result;
        if(name == null || name.isBlank() || name.isEmpty()) {
            result = generateName();
        } else {
            result = name;
            if(names.contains(result)) {
                result = generateName();
            }
        }
        names.add(result);
        Collections.sort(names);
        return result;
    }

    /**
     * This method generates a new name.
     * The building from the name is (className + index of founded classNames).
     * @return string - the result name.
     */
    private String generateName() {
        String result;
        Stream<String> stream = names.stream().filter(value -> value.startsWith(getClass().getSimpleName()));
        int index = stream.toList().size();
        result = getClass().getSimpleName() + "-" + index;
        stream.close();
        return result;
    }

    @Override
    public void initialize() {
        this.initialized = true;
    }

    @Override
    public void render(Window window) {
        if(window == null) {
            return;
        }
        if(this.window == null) {
            this.window = window;
            this.initializeUtils(window);
        }
        Scene scene = window.getScene();
        if(scene == null) {
            return;
        }
        for(Component component : scene.getChildren()) {
            component.setWindow(window);
            this.addToRender(component);
        }
        this.render(toRender);
    }

    /**
     * This method added a new element to the render que.
     * @param component - to added element.
     */
    private void addToRender(Component component) {
        if(exist(component.getLocalizedName())) {
            return;
        }
        this.toRender.add(component);
        if(component.isParent()) {
            for(Component child : component.getChildren()) {
                this.addToRender(child);
            }
        }
    }

    /**
     * This method checks if an element exist or not.
     * @param localizedName - the final name of the element.
     * @return boolean - true if was found!
     */
    private boolean exist(String localizedName) {
        boolean state = false;
        for(Component component : toRender) {
            if(component.getLocalizedName().equals(localizedName)) {
                state = true;
                break;
            }
        }
        return state;
    }

    /**
     * This method allows the implemented class to initialize needed thinks like TextUtils or PaintUtils.
     * You can create objects here which need a one time initialize.
     * @param window - the window which used this render thinks.
     */
    protected abstract void initializeUtils(Window window);

    /**
     * This method allows the implemented class to render.
     * @param components - a list of elements which need rendering.
     */
    protected abstract void render(List<Component> components);

    @Override
    public void destroy() {
        this.toRender.clear();
    }

    @Override
    public String getName() {
        return this.name == null ? getClass().getSimpleName() : name;
    }

    @Override
    public boolean isInitialized() {
        return this.initialized;
    }

    @Override
    public void setPriority(RenderPriority priority) {
        this.priority = priority;
        this.setChangeDetect(true);
    }

    @Override
    public RenderPriority getPriority() {
        return priority;
    }

    @Override
    public boolean changeDetected() {
        return changeDetect;
    }

    @Override
    public void setChangeDetect(boolean changeDetect) {
        this.changeDetect = changeDetect;
    }

    /**
     * @return window - the window which used this class.
     */
    public Window getWindow() {
        return this.window;
    }

}
