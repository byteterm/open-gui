package de.byteterm.opengui.graphics;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation represent a custom render component.
 * If a component flagged by this annotation the ui renderer will call its render(Window) method.
 * Make sure you have a method with the name render and the parameter Window.
 * It will throw errors if the method wasn't found by the selected class.
 * @author Daniel Ramke
 * @since 1.0.2
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CustomRenderer { }
