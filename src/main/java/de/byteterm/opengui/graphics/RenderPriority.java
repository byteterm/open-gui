package de.byteterm.opengui.graphics;

/**
 * This enum contains all needed render priorities which ar exist.
 * Please note that the IMPORTANT render priority is only usefully by limited use.
 * If you need it makes sure that's it absolute important for your project.
 * Else use the MODERATE priority for the normal use of this library.
 * Note all AbstractRenderers used the MODERATE priority.
 * @author Daniel Ramke
 * @since 1.0.2
 */
public enum RenderPriority {

    LOWEST(0),
    LOW(1),
    MEDIUM(2),
    MODERATE(3),
    HIGH(4),
    VERY_HIGH(5),
    IMPORTANT(6);

    private final int priority;

    RenderPriority(int priority) {
        this.priority = priority;
    }

    /**
     * @return integer - the priority identifier.
     */
    public int getPriority() {
        return priority;
    }

    /**
     * This searched a priority by the given identifier.
     * @param priority - the identifier.
     * @return RenderPriority - the founded priority, if it wasn't found then returned it MODERATE.
     */
    public static RenderPriority get(int priority) {
        RenderPriority value = RenderPriority.MODERATE;
        for(RenderPriority renderPriority : values()) {
            if(renderPriority.getPriority() == priority) {
                value = renderPriority;
                break;
            }
        }
        return value;
    }
}
