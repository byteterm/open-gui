package de.byteterm.opengui.graphics;

import de.byteterm.opengui.ui.Window;

/**
 * The renderer interface is used to allow any user to add their own renderer.
 * Note that our default renderers are created using the AbstractRenderer class.
 * If you want to create a new renderer that works with scenes, you can do that with the abstract class.
 * For more complex rendering, however, you can only use the interface to write a complete custom renderer.
 * It should be noted here that this renderer is still added via the addRenderer() method in the window class.
 * If you have any further questions, please visit our website: [LINK]
 * @author Daniel Ramke
 * @since 1.0.1
 * @see RenderPriority
 * @see Window
 */
@SuppressWarnings("unused")
public interface Renderer {

    /**
     * This method returned the name of the renderer.
     * This name need be final and can't duplicate!
     * @return the final name as identifier.
     */
    String getName();

    /**
     * This method returned the render priority of the renderer.
     * This can be change all time.
     * @return RenderPriority - the current render priority.
     */
    default RenderPriority getPriority() {
        return RenderPriority.MODERATE;
    }

    /**
     * This method change the current render priority.
     * @param priority - the new render priority.
     */
    void setPriority(RenderPriority priority);

    /**
     * This boolean will be return true if it was priority changes detected.
     * @return boolean - true by change detect.
     */
    boolean changeDetected();

    /**
     * This method is used by the render() method in the window class.
     * Please don't change thinks here!
     * @param need - needed an update or not (Priority update!).
     */
    void setChangeDetect(boolean need);

    /**
     * This method returned true if the renderer was successfully initialized.
     * @return the state of initialization.
     */
    boolean isInitialized();

    /**
     * This method is called at first if the renderer is starting.
     * Include here methods to called one time by starting the renderer.
     * Don't loop thinks in this method!
     */
    void initialize();

    /**
     * This method is called every tick of system.
     * This is needed to update the render all time and render graphics.
     * @param window the window witch need to be rendered.
     */
    void render(Window window);

    /**
     * This method clear and destroyed the current renderer.
     * After calling this method, the renderer is not enabled more and can't be drawing thinks.
     */
    void destroy();

}
