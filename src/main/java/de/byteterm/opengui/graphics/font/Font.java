package de.byteterm.opengui.graphics.font;

import de.byteterm.jlogger.Logger;
import de.byteterm.jlogger.util.FileUtils;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.tools.PathSystem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * This class stored a created font from the given ttf file.
 * The class caught all font faces from the font folder and save them to a list.
 * The face will be stored with his bytebuffer for nvg usage.
 * For mor information about fonts, visit our website [LINK].
 * @author Daniel Ramke
 * @since 1.0.2
 * @see FontFace
 */
@SuppressWarnings("unused")
public class Font {

    private final Logger logger = Logger.getLogger();

    private String path;
    private String name;

    private boolean created;
    private boolean updated;
    private String reason;

    private final List<FontFace> faces = new ArrayList<>();

    /**
     * This constructor creates a new font object.
     * @param path - the font main path without font name.
     * @param name - the font name.
     */
    public Font(String path, String name) {
        this(path + "/" + name);
    }

    /**
     * This constructor creates a new font with the given path.
     * The path must be contained the name of the font.
     * @param absolutePath - the complete font path.
     */
    public Font(String absolutePath) {
        if(absolutePath == null || absolutePath.isEmpty() || absolutePath.isBlank()) {
            logger.warn("The library is trying to create a font but the path is not readable!");
            return;
        }
        this.name = FileUtils.getFileNameFromPath(absolutePath);
        this.path = absolutePath.substring(0, (absolutePath.length() - name.length()));
        this.loadFaces();
        if(created) {
            logger.debug("A new font was created with the name [ " + name + " ]");
            Registry.registerFont(this);
            return;
        }
        logger.error("Can't create font [ " + name + " ] reason: " + reason);
    }

    /**
     * This method updated the current font and his faces.
     */
    public void update() {
        updated = false;
        this.loadFaces();
        logger.debug("Font [ " + getName() + " ] is now updated!");
    }

    /**
     * This method loaded the font face in the font folder.
     * Files which can't be sorted to face, will ignore.
     */
    private void loadFaces() {
        if(created && updated) {
            return;
        }
        File folder = new File(getAbsolutePath());
        if(!folder.exists()) {
            reason = "Font folder doesn't exists!";
            return;
        }

        File[] files = folder.listFiles();
        if(files == null) {
            reason = "No font files was found in the font folder!";
            return;
        }
        int goodFiles = 0, badFiles = 0, duplicated = 0;
        for(File file : files) {
            String fileName = file.getName();
            if(!(fileName.endsWith(".ttf"))) {
                badFiles++;
                continue;
            }

            if(!(fileName.contains("-"))) {
                badFiles++;
                continue;
            }
            String[] build = fileName.split("-");

            if(build.length > 2) {
                badFiles++;
                continue;
            }
            String fontName = build[0];
            String faceName = build[1];

            if(!(getName().equals(fontName))) {
                badFiles++;
                continue;
            }
            String[] array = faceName.split("\\.");

            if(array.length != 2) {
                badFiles++;
                continue;
            }
            String saveName = getName() + "-" + array[0];

            if(getFace(saveName) != null) {
                if(!updated) {
                    continue;
                }
                duplicated++;
                continue;
            }
            faces.add(new FontFace(getAbsolutePath(), saveName));
            goodFiles++;
        }
        logger.debug("Finished face load with ( Loaded: " + goodFiles + " | Errors: " + badFiles + " | Duplicated: " + duplicated + " ) for [ " + getName() + " ]");
        this.created = true;
        this.updated = true;
    }

    /**
     * This method searched for a FontFace object by name.
     * @param name - the face name.
     * @return FontFace - the founded FontFace object.
     */
    public FontFace getFace(String name) {
        if(faces.isEmpty()) return null;
        String formatted;
        if(!name.contains("-")) {
            formatted = getName() + "-" + name;
        } else {
            formatted = name;
        }
        FontFace fontFace = null;
        for(FontFace faces : faces) {
            if(faces.getName().equals(formatted)) {
                fontFace = faces;
                break;
            }
        }
        return fontFace;
    }

    /**
     * @return List<FontFace> - of available faces.
     */
    public List<FontFace> getFaces() {
        return faces;
    }

    /**
     * @return String - the complete font path.
     */
    public String getAbsolutePath() {
        return getPath() + getName();
    }

    /**
     * @return String - only the font path.
     */
    public String getPath() {
        return path;
    }

    /**
     * @return String - the correct font name.
     */
    public String getName() {
        return name;
    }

    /**
     * @return boolean - the creation state of this font.
     * True if the font was created.
     */
    public boolean isCreated() {
        return created;
    }

    /**
     * This is the fallback font.
     */
    public static Font FALLBACK = new Font(PathSystem.INTERNAL("/assets/font/"), "Default");
}
