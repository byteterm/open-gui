package de.byteterm.opengui.graphics.paint;

/**
 * This class was created for helping the Gradient class.
 * The gradient can display more than 2 colors with this class.
 * For more information to this class (record) look for aur website: [LINK].
 * @author Daniel Ramke
 * @since 1.0.2
 * <p></p>
 * This record controls a color with stops.
 * The stops mean that this color have an area.
 * @param color - the color.
 * @param portion - the portion of the colored area.
 */
public record ColorStop(Color color, float portion) {

    /**
     * This record constructor checks the current values and fix them.
     * @param color - the color.
     * @param portion - the portion of the colored area.
     */
    public ColorStop {
        if (color == null) {
            color = Color.GRAY;
        }
        if(portion < 0) {
            portion = 0;
        }
    }
}
