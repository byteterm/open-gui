package de.byteterm.opengui.graphics.paint;

import de.byteterm.jlogger.Logger;

import java.util.List;

/**
 * The gradient class controls the colored gradient.
 * You can choose multi colors to draw it to the window.
 * Please note that currently it will work with 2 colors!
 * If you choose more than 2 colors it will throw an error and abort the library.
 * @author Daniel Ramke
 * @since 1.0.2
 * @see Color
 */
@SuppressWarnings("unused")
public class Gradient {

    private Color start;
    private Color end;
    private final ColorStop[] complexGradient;
    private Direction direction;
    private float angle;

    /**
     * This constructor creates a new gradient with the specified colors.
     * Please don't choose more than 2 colors!
     * @param colors - choose 2 colors for create a color gradient.
     */
    public Gradient(Color... colors) {
        if(colors == null) {
            Logger logger = Logger.getLogger();
            logger.warn("A Gradient need min 2 colors!");
            this.complexGradient = null;
            return;
        }
        if(colors.length == 2) {
            start = colors[0];
            end = colors[1];
        } else {
            start = colors[0];
            end = colors[colors.length-1];
        }
        this.direction = Direction.TOP_TO_BOTTOM;
        setAngle(direction.getAngle());
        this.complexGradient = new ColorStop[colors.length];
        for(int i = 0; i < colors.length; i++) {
            complexGradient[i] = new ColorStop(colors[i], (float) i / (float) (colors.length-1));
        }
    }

    /**
     * @return Color - the current selected start color.
     */
    public Color getStart() {
        return start;
    }

    /**
     * @return Color - the current selected end color.
     */
    public Color getEnd() {
        return end;
    }

    /**
     * @return Color[] - all chosen colors.
     */
    public ColorStop[] getColors() {
        return complexGradient;
    }

    /**
     * @return Direction - the current gradient direction.
     *  Is NONE if the angle not in a correct direction.
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * This method set the current angle.
     * You can choose the direction you wish from 0 to 360.
     * But you can switch the sum by 1 to 1 instanceof the setAngle(Direction) method.
     * @param angle - the angle from 0 to 360.
     */
    public void setAngle(float angle) {
        if(angle < 0 || angle > 360) {
            angle = Direction.TOP_TO_BOTTOM.getAngle();
        }
        this.angle = angle;
        this.direction = Direction.get(angle);
    }

    /**
     * This method set the current angle.
     * You can choose the direction you wish from 0 to 360.
     * @param direction - the wish direction.
     */
    public void setAngle(Direction direction) {
        this.setAngle(direction.getAngle());
    }

    /**
     * @return float - the current angle.
     */
    public float getAngle() {
        return angle;
    }

    /**
     * This enum constance stored the known css gradient values.
     * With these values you can change the displayed direction of your gradient.
     * Note that this enum is only used for the gradient class and will not work if you try it in other classes.
     * @author Daniel Ramke
     * @since 1.0.2
     */
    public enum Direction {

        TOP_LEFT_TO_BOTTOM_RIGHT("to bottom right", 45),
        TOP_TO_BOTTOM("to bottom", 90),
        TOP_RIGHT_TO_BOTTOM_LEFT("to bottom left", 135),
        CENTER_RIGHT_TO_CENTER_LEFT("to left", 180),
        BOTTOM_RIGHT_TO_TOP_LEFT("to top left", 225),
        BOTTOM_TO_TOP("to top", 270),
        BOTTOM_LEFT_TO_TOP_RIGHT("to top right", 315),
        CENTER_LEFT_TO_CENTER_RIGHT("to right", 360),
        NONE("none", 0);

        private final String cssName;
        private final float angle;

        Direction(String cssName, float angle) {
            this.cssName = cssName;
            this.angle = angle;
        }

        /**
         * @return string - the identifier css value.
         */
        public String getCssName() {
            return cssName;
        }

        public float getAngle() {
            return angle;
        }

        /**
         * This method returned the founded enum object by css code.
         * @param css - the linear-gradient(to right) code.
         * @return Direction - the founded direction.
         */
        public static Direction get(String css) {
            Direction direction = NONE;
            for(Direction directions : values()) {
                if(directions.getCssName().equals(css)) {
                    direction = directions;
                    break;
                }
            }
            return direction;
        }

        /**
         * This method returned the founded enum object by angle.
         * @param angle - the angle direction.
         * @return Direction - the founded direction.
         */
        public static Direction get(float angle) {
            Direction direction = NONE;
            for(Direction directions : values()) {
                if(directions.getAngle() == angle) {
                    direction = directions;
                    break;
                }
            }
            return direction;
        }
    }

    /**
     * This method creates a new gradient with 2 colors.
     * @param start - the first color.
     * @param end - the second color.
     * @return Gradient - the finished gradient object.
     */
    public static Gradient get(Color start, Color end) {
        return new Gradient(start, end);
    }

    /**
     * This method creates a new gradient from the list of colors.
     * @param colors - the colored list.
     * @return Gradient - the finished gradient object.
     */
    public static Gradient get(List<Color> colors) {
        Color[] array = new Color[colors.size()];
        for(int i = 0; i < colors.size(); i++) {
            array[i] = colors.get(i);
        }
        return get(array);
    }

    /**
     * This method creates a new gradient from the array of colors.
     * @param colors - the colored array.
     * @return Gradient - the finished gradient object.
     */
    public static Gradient get(Color... colors) {
        return new Gradient(colors);
    }

}
