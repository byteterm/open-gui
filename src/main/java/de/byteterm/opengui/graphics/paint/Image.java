package de.byteterm.opengui.graphics.paint;

import de.byteterm.jlogger.Logger;
import de.byteterm.jlogger.util.FileUtils;
import de.byteterm.opengui.Registry;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is for creating an image object.
 * Note that these objects are not loaded into the intended NanoVG context.
 * The creation of an OpenGL id is done in the renderer itself, since the correct context is also available there.
 * In addition, the methods getWidth() and getHeight() depend on the creation of the OpenGL id.
 * For more information visit our website: [LINK].
 * @author Daniel Ramke
 * @since 1.0.2
 */
@SuppressWarnings("unused")
public class Image {

    private final Logger logger = Logger.getLogger();

    private final String[] allowedFormats = new String[]{"png", "jpg", "jpeg", "svg", "gif"};

    private final Map<Long, Integer> gl_func_id_list = new HashMap<>();
    private final String path;
    private final String name;

    private int imageWidth;
    private int imageHeight;
    private int[] pixelData;

    private float alpha;

    /**
     * This constructor creates an image object which stored the needed information.
     * The constructor can't create the needed id.
     * This id is created by trying rendering the image.
     * @param path - the correct image path.
     */
    public Image(@NotNull String path) {
        this.path = path;
        this.name = FileUtils.getOnlyFileName(FileUtils.getFileNameFromPath(path));
        this.setAlpha(1.0f);
        this.createInformation();
        Registry.registerImage(this);
    }

    /**
     * This method added a new context with id.
     * Please don't use this method by your self.
     * This method is used in internal function, which need this id.
     * If you need an identifier for the image use the getName() method instance of this.
     * @param context - the nvg render context.
     * @param gl_func_id - the created id from nvg.
     */
    public void addId(long context, int gl_func_id) {
        if(gl_func_id_list.containsKey(context)) {
            return;
        }
        gl_func_id_list.put(context, gl_func_id);
    }

    /**
     * This method gets the id by contextID.
     * Note that this id is only useful to opengl.
     * The return value can be -1, this means that this context have no image id!
     * @param context - the nvg context which stored the correct id.
     * @return integer - the opengl id from nvg renderer.
     */
    public int getGl_Func_Id(long context) {
        return gl_func_id_list.getOrDefault(context, -1);
    }

    /**
     * @return string - the current image path.
     */
    public String getPath() {
        return path;
    }

    /**
     * This returned the file name, this is useful as identifier.
     * @return string - final name of this image.
     */
    public String getName() {
        return name;
    }

    /**
     * @return integer - the image width. (final width)
     */
    public int getImageWidth() {
        return imageWidth;
    }

    /**
     * @return integer - the image height. (final height)
     */
    public int getImageHeight() {
        return imageHeight;
    }

    /**
     * This method changed the current alpha value of this image.
     * The alpha component is only supported by PNG formats.
     * This method checks the given image file for the formats.
     * The default value of alpha component is 1.0f.
     * @param alpha - the opacity of the image.
     */
    public void setAlpha(float alpha) {
        if(alpha > 1.0f || alpha < 0.0f) {
            alpha = 1.0f;
        }
        if(!getFormat().equals(".png")) {
            alpha = 1.0f;
        }
        this.alpha = alpha;
    }
    /**
     * @return float - the current alpha value.
     */
    public float getAlpha() {
        return alpha;
    }

    /**
     * @return integer - the current pixels in the image.
     */
    public int getPixels() {
        return imageWidth * imageHeight;
    }

    /**
     * This method returned the created pixel data array.
     * The array contains all colors for the pixels.
     * @return integer[] - the complete array of colored pixels.
     */
    public int[] getPixelData() {
        return pixelData;
    }

    /**
     * @return boolean - true if the id list is not empty.
     */
    public boolean isCreated() {
        return !gl_func_id_list.isEmpty();
    }

    /**
     * @return string - the file extension which defined the image format.
     */
    public String getFormat() {
        String extension = "";
        String[] array = FileUtils.getFileNameFromPath(path).split("\\.");
        if(array.length == 2) {
            extension = array[1];
        }
        return "." + extension;
    }

    /**
     * This method created the needed image information for us.
     */
    private void createInformation() {
        try {
            BufferedImage image = ImageIO.read(FileUtils.inputStream(path));
            this.imageWidth = image.getWidth();
            this.imageHeight = image.getHeight();
            this.pixelData = new int[imageWidth * imageHeight];
            int[] tmpData = new int[imageWidth * imageHeight];
            image.getRGB(0, 0, imageWidth, imageHeight, tmpData, 0, imageWidth);
            for(int i = 0; i < tmpData.length; i++) {
                pixelData[i] = (tmpData[i] >> 16) & 0xff;
                pixelData[i] = (tmpData[i] >> 8) & 0xff;
                pixelData[i] = (tmpData[i]) & 0xff;
                pixelData[i] = (tmpData[i] >> 24) & 0xff;
            }
        } catch (IOException exception) {
            logger.error(exception);
        }
    }

    /**
     * This method create a new image by the given path.
     * If the path be found than it will return the existing image.
     * @param path - the new image path.
     * @param alpha - the image alpha value.
     * @return Image - the image witch was found or new created.
     */
    public static Image create(@NotNull String path, float alpha) {
        Image image = Image.get(FileUtils.getOnlyFileName(FileUtils.getFileNameFromPath(path)));
        if(image == null) {
            image = new Image(path);
            image.setAlpha(alpha);
        }
        return image;
    }

    /**
     * This method create a new image by the given path.
     * If the path be found than it will return the existing image.
     * @param path - the new image path.
     * @return Image - the image witch was found or new created.
     */
    public static Image create(@NotNull String path) {
        return create(path, 1.0f);
    }

    /**
     * This method get a created image by name.
     * @param name - the final image name (without extension).
     * @return Image the founded image can be null!
     */
    public static Image get(@NotNull String name) {
        return Registry.getImage(name);
    }

}
