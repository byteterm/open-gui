package de.byteterm.opengui.graphics.render;

import de.byteterm.opengui.graphics.AbstractRenderer;
import de.byteterm.opengui.graphics.utils.NanoBorder;
import de.byteterm.opengui.style.types.Border;
import de.byteterm.opengui.ui.Component;
import de.byteterm.opengui.ui.Window;

import java.util.List;

/**
 * This renderer is used to render the ui borders.
 * For render, it correctly we will use the Border class which can use css.
 * @author Daniel Ramke
 * @since 1.0.2
 * @see de.byteterm.opengui.graphics.Renderer
 * @see AbstractRenderer
 */
public final class UIComponentBorderRenderer extends AbstractRenderer {

    private NanoBorder nanoBorder;

    @Override
    protected void initializeUtils(Window window) {
        this.nanoBorder = new NanoBorder(window);
    }

    @Override
    protected void render(List<Component> components) {
        for(Component component : components) {
            Border border = component.getStyle().getBorder();
            nanoBorder.draw(component.getAbsoluteX(), component.getAbsoluteY(), component.getWidth(), component.getHeight(), border);
        }
    }
}
