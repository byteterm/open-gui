package de.byteterm.opengui.graphics.render;

import de.byteterm.jlogger.Logger;
import de.byteterm.opengui.graphics.AbstractRenderer;
import de.byteterm.opengui.graphics.CustomRenderer;
import de.byteterm.opengui.ui.Component;
import de.byteterm.opengui.ui.Window;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class UIComponentCustomRenderer extends AbstractRenderer {

    private final Logger logger = Logger.getLogger();

    @Override
    protected void initializeUtils(Window window) {

    }

    @Override
    protected void render(List<Component> components) {
        for(Component component : components) {
            Class<?> checked = component.getClass();
            if(!checked.isAnnotationPresent(CustomRenderer.class)) {
                continue;
            }
            try {
                Method method = checked.getMethod("render", Window.class);
                method.setAccessible(true);
                method.invoke(component, getWindow());
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException exception) {
                logger.error(exception);
            }
        }
    }
}
