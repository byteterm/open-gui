package de.byteterm.opengui.graphics.render;

import de.byteterm.opengui.graphics.AbstractRenderer;
import de.byteterm.opengui.graphics.utils.NanoText;
import de.byteterm.opengui.ui.Component;
import de.byteterm.opengui.ui.Labeled;
import de.byteterm.opengui.ui.Window;

import java.util.List;

/**
 * This renderer is used to render the ui text for labels.
 * For render, it correctly we will use the Labeled interface.
 * @author Daniel Ramke
 * @since 1.0.2
 * @see de.byteterm.opengui.graphics.Renderer
 * @see AbstractRenderer
 */
public final class UIComponentTextRenderer extends AbstractRenderer {

    private NanoText nanoText;

    @Override
    protected void initializeUtils(Window window) {
        this.nanoText = new NanoText(window);
    }

    @Override
    protected void render(List<Component> components) {
        for(Component component : components) {
            if(component instanceof Labeled labeled) {
                nanoText.text(labeled);
            }
        }
    }
}
