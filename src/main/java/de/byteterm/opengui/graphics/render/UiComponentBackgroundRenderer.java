package de.byteterm.opengui.graphics.render;

import de.byteterm.opengui.graphics.AbstractRenderer;
import de.byteterm.opengui.graphics.utils.NanoBackground;
import de.byteterm.opengui.style.types.Background;
import de.byteterm.opengui.ui.Component;
import de.byteterm.opengui.ui.Window;

import java.util.List;

/**
 * This renderer is used to render the ui backgrounds.
 * For render, it correctly we will use the Background class which can use css.
 * @author Daniel Ramke
 * @since 1.0.2
 * @see de.byteterm.opengui.graphics.Renderer
 * @see AbstractRenderer
 */
public final class UiComponentBackgroundRenderer extends AbstractRenderer {

    private NanoBackground nanoBackground;

    @Override
    protected void initializeUtils(Window window) {
        nanoBackground = new NanoBackground(window);
    }

    @Override
    protected void render(List<Component> components) {
        for(Component component : components) {
            Background background = component.getStyle().getBackground();
            nanoBackground.draw(component.getAbsoluteX(), component.getAbsoluteY(), component.getWidth(), component.getHeight(), background);
        }
    }
}
