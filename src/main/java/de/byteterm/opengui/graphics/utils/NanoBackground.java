package de.byteterm.opengui.graphics.utils;

import de.byteterm.jlogger.util.FileUtils;
import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.graphics.paint.ColorStop;
import de.byteterm.opengui.graphics.paint.Gradient;
import de.byteterm.opengui.graphics.paint.Image;
import de.byteterm.opengui.style.types.Background;
import de.byteterm.opengui.tools.formal.Radius;
import de.byteterm.opengui.ui.Window;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.nanovg.NVGPaint;
import org.lwjgl.nanovg.NanoVG;
import org.lwjgl.system.MemoryUtil;

/**
 * This class help renderers to render backgrounds for us.
 * A background is an object which stored the background information.
 * The background information contains images, colors and gradient.
 * This class sorted the backgrounds and used the correct methods to draw the background.
 * Note that you can use this class without a background object, by using a draw method.
 * @author Daniel Ramke
 * @since 1.0.2
 */
//Todo: include Cache system.
@SuppressWarnings("unused")
public class NanoBackground {

    private static final Color FALLBACK_COLOR = Color.GRAY;
    private final long context;

    /**
     * This constructor creates an instance of this.
     * The constructor is absolute needed for using this class.
     * @param window - the window which give us the contextID.
     */
    public NanoBackground(@NotNull Window window) {
        this.context = window.getContextID();
    }

    /**
     * This method draws the background object to the window.
     * Note this method filtered by the background type.
     * @param positionX - the x position of the drawing object to the screen.
     * @param positionY - the y position of the drawing object to the screen.
     * @param width - the width of the drawing object to the screen.
     * @param height - the height of the drawing object to the screen.
     * @param background - background object which was created before.
     */
    public void draw(double positionX, double positionY, double width, double height, Background background) {
        if(context <= MemoryUtil.NULL) {
            return;
        }
        if(background == null) {
            this.drawColor(positionX, positionY, width, height, FALLBACK_COLOR, new Radius(0));
            return;
        }

        Radius radius = background.getRadius();
        switch (background.getType()) {
            case COLOR -> this.drawColor(positionX, positionY, width, height, background.getColor(), radius);
            case IMAGE -> this.drawImage(positionX, positionY, width, height, background.getImage(), radius);
            case LINEAR_GRADIENT -> this.drawGradient(positionX, positionY, width, height, background.getGradient(), radius);
        }
    }
    /**
     * This method draws the background object to the window.
     * @param positionX - the x position of the drawing object to the screen.
     * @param positionY - the y position of the drawing object to the screen.
     * @param width - the width of the drawing object to the screen.
     * @param height - the height of the drawing object to the screen.
     * @param color - the wish color object.
     * @param radius - the background radius.
     */
    public void drawColor(double positionX, double positionY, double width, double height, Color color, Radius radius) {
        if(color == null) {
            color = FALLBACK_COLOR;
        }
        if(radius == null) {
            radius = new Radius(0);
        }
        NanoVG.nvgBeginPath(context);
        NanoVG.nvgPathWinding(context, NanoVG.NVG_SOLID);
        this.createRect(positionX, positionY, width, height, radius);
        NanoVG.nvgFillColor(context, color.getNanoVGColor());
        NanoVG.nvgFill(context);
        NanoVG.nvgClosePath(context);
    }

    /**
     * This method draws the background object to the window.
     * @param positionX - the x position of the drawing object to the screen.
     * @param positionY - the y position of the drawing object to the screen.
     * @param width - the width of the drawing object to the screen.
     * @param height - the height of the drawing object to the screen.
     * @param image - the image which was created before.
     * @param radius - the background radius.
     */
    public void drawImage(double positionX, double positionY, double width, double height, Image image, Radius radius) {
        if(image == null) {
            this.drawColor(positionX, positionY, width, height, FALLBACK_COLOR, new Radius(0));
            return;
        }
        if(radius == null) {
            radius = new Radius(0);
        }
        NanoVG.nvgBeginPath(context);
        try(NVGPaint imagePaint = NVGPaint.calloc()) {
            int imageID = this.createImage(image);
            if(!image.isCreated()) {
                return;
            }
            NanoVG.nvgImagePattern(context, (float) positionX, (float) positionY, (float) width, (float) height
                    , 0, imageID, image.getAlpha(), imagePaint);
            this.createRect(positionX, positionY, width, height, radius);
            NanoVG.nvgFillPaint(context, imagePaint);
            NanoVG.nvgFill(context);
            NanoVG.nvgClosePath(context);
        }
    }

    /**
     * This method draws the background object to the window.
     * @param positionX - the x position of the drawing object to the screen.
     * @param positionY - the y position of the drawing object to the screen.
     * @param width - the width of the drawing object to the screen.
     * @param height - the height of the drawing object to the screen.
     * @param gradient - the gradient which was created before.
     * @param radius - the background radius.
     */
    public void drawGradient(double positionX, double positionY, double width, double height, Gradient gradient, Radius radius) {
        if(gradient == null) {
            this.drawColor(positionX, positionY, width, height, FALLBACK_COLOR, new Radius(0));
            return;
        }
        if(radius == null) {
            radius = new Radius(0);
        }
        ColorStop[] colors = gradient.getColors();
        for(int i = 0; i < colors.length - 1; i++) {
            try (NVGPaint coloredPaint = NVGPaint.calloc()) {
                float angle = gradient.getAngle();

                float centerX = (float)positionX + (float)width*0.5f;
                float centerY = (float)positionY + (float)height*0.5f;
                float xx = centerX - (float)((Math.cos(Math.toRadians(angle)) * width) * 0.5 + 0.5);
                float yy = centerY - (float)((Math.sin(Math.toRadians(angle)) * height) * 0.5 + 0.5);
                float dirX = (float) Math.cos(Math.toRadians(angle));
                float dirY = (float) Math.sin(Math.toRadians(angle));
                float step = colors[i].portion();
                float nextstep = colors[i+1].portion();
                float startX = xx + (dirX * step * (float)width);
                float startY = yy + (dirY * step * (float)height);
                float endX = xx + (dirX * nextstep * (float)width);
                float endY = yy + (dirY * nextstep * (float)height);


                Color start = colors[i].color();
                if(i > 0) {
                    start = Color.transparent;
                }
                Color end = colors[i+1].color();

                NanoVG.nvgLinearGradient(context, startX, startY, endX, endY
                        , start.getNanoVGColor()
                        , end.getNanoVGColor(), coloredPaint);

                NanoVG.nvgBeginPath(context);
                this.createRect(positionX, positionY, width, height, radius);
                NanoVG.nvgFillPaint(context, coloredPaint);
                NanoVG.nvgFill(context);
                NanoVG.nvgClosePath(context);
            }
        }
    }

    /**
     * This private method create our image for opengl.
     * The method here was written by the lwjgl team.
     * @param image - the raw image object with the information.
     * @return integer - the created opengl id.
     */
    private int createImage(Image image) {
        int reference = image.getGl_Func_Id(context);
        if(reference <= -1) {
            reference = NanoVG.nvgCreateImageMem(context, 0, FileUtils.resourceToByteBuffer(image.getPath()));
            if(reference > -1) {
                image.addId(context, reference);
            }
        }
        return reference;
    }

    /**
     * This method creates a new rounded rect for us.
     * @param x - the render x position.
     * @param y - the render y position.
     * @param width - the rect width.
     * @param height - the rect height.
     * @param radius - the rect radius.
     */
    private void createRect(double x, double y, double width, double height, Radius radius) {
        NanoVG.nvgRoundedRectVarying(context, (float) x, (float) y, (float) width, (float) height
                , (float) radius.getTopLeft()
                , (float) radius.getTopRight()
                , (float) radius.getBottomRight()
                , (float) radius.getBottomLeft());
    }

}
