package de.byteterm.opengui.graphics.utils;

import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.graphics.paint.Gradient;
import de.byteterm.opengui.style.types.Border;
import de.byteterm.opengui.tools.formal.Insets;
import de.byteterm.opengui.tools.formal.Radius;
import de.byteterm.opengui.ui.Window;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.nanovg.NanoVG;
import org.lwjgl.system.MemoryUtil;

/**
 * This class help renderers to render borders for us.
 * A border is an object which stored the border information.
 * The border information contains form, colors and gradient.
 * This class sorted the border and used the correct methods to draw the borders.
 * Note that you can use this class without a border object, by using a draw method.
 * @author Daniel Ramke
 * @since 1.0.2
 */
//Todo: better calculation for the borders, and Cache system.
public class NanoBorder {

    private static final Color FALLBACK_COLOR = Color.GRAY;
    private final long context;

    /**
     * This constructor creates an instance of this.
     * The constructor is absolute needed for using this class.
     * @param window - the window which give us the contextID.
     */
    public NanoBorder(@NotNull Window window) {
        this.context = window.getContextID();
    }

    /**
     * This method draws a border with the specified information.
     * The method used the #drawColor() and #drawGradient method.
     * @param positionX - this defined the x position of the border.
     * @param positionY - this defined the y position of the border.
     * @param width - this defined the border width.
     * @param height - this defined the border height.
     * @param border - the general border information like radius, stroke and style.
     */
    public void draw(double positionX, double positionY, double width, double height, Border border) {
        if(context <= MemoryUtil.NULL) {
            return;
        }
        if(border == null) {
            border = new Border(FALLBACK_COLOR);
        }
        if(border.getStyle().equals(Border.Style.NONE)) {
            return;
        }

        switch (border.getType()) {
            case COLOR -> drawColor(positionX, positionY, width, height, border.getColor(), border.getStyle(), border.getStroke(), border.getRadius());
            case LINEAR_GRADIENT -> drawGradient(positionX, positionY, width, height, border.getGradient(), border.getStyle(), border.getStroke(), border.getRadius());
        }
    }

    /**
     * This method draws a border with a color.
     * @param positionX - this defined the x position of the border.
     * @param positionY - this defined the y position of the border.
     * @param width - this defined the border width.
     * @param height - this defined the border height.
     * @param color - the border color.
     * @param style - the border style.
     * @param stroke - the border stroke.
     * @param radius - the border radius.
     */
    public void drawColor(double positionX, double positionY, double width, double height, Color color, Border.Style style, Insets stroke, Radius radius) {
        if(radius == null) {
            radius = new Radius(0);
        }
        if(stroke == null) {
            stroke = new Insets(1);
        }

        NanoVG.nvgBeginPath(context);
        NanoVG.nvgFillColor(context, color.getNanoVGColor());

        NanoVG.nvgRoundedRectVarying(context, (float) positionX + 2, (float) positionY + 2, (float) width - 4, (float) height - 4
                , (float) (radius.getTopLeft() - 2), (float) (radius.getTopRight() - 2)
                , (float) (radius.getBottomRight() - 2), (float) (radius.getBottomLeft() - 2));
        NanoVG.nvgPathWinding(context, NanoVG.NVG_HOLE);

        float xx = (float) (positionX - stroke.getLeft());
        float yy = (float) (positionY - stroke.getTop());
        float ww = (float) (width + stroke.getWidth());
        float hh = (float) (height + stroke.getHeight());
        NanoVG.nvgRoundedRectVarying(context, xx, yy, ww, hh,
                (float) radius.getTopLeft(), (float) radius.getTopRight(), (float) radius.getBottomRight(), (float) radius.getBottomLeft());
        NanoVG.nvgPathWinding(context, NanoVG.NVG_SOLID);

        NanoVG.nvgFill(context);
        NanoVG.nvgClosePath(context);

    }

    /**
     * This method draws a border with a color gradient.
     * @param positionX - this defined the x position of the border.
     * @param positionY - this defined the y position of the border.
     * @param width - this defined the border width.
     * @param height - this defined the border height.
     * @param gradient - the border color gradient.
     * @param style - the border style.
     * @param stroke - the border stroke.
     * @param radius - the border radius.
     */
    public void drawGradient(double positionX, double positionY, double width, double height, Gradient gradient, Border.Style style, Insets stroke, Radius radius) {

    }


}
