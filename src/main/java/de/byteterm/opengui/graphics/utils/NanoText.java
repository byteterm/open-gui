package de.byteterm.opengui.graphics.utils;

import de.byteterm.opengui.graphics.font.Font;
import de.byteterm.opengui.style.types.FontPattern;
import de.byteterm.opengui.tools.formal.Insets;
import de.byteterm.opengui.tools.geometry.TextDirection;
import de.byteterm.opengui.ui.Component;
import de.byteterm.opengui.ui.Labeled;
import de.byteterm.opengui.ui.Window;
import org.jetbrains.annotations.NotNull;
import org.joml.Vector2f;
import org.lwjgl.nanovg.NVGTextRow;
import org.lwjgl.nanovg.NanoVG;
import org.lwjgl.system.MemoryUtil;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * This class help renderers to render fonts for us.
 * A font is an object which stored the text information.
 * The font information contains family, color, size, lineHeight, blur and more.
 * This class sorted the font and used the correct methods to draw the text.
 * Note that you can use this class without a text object, by using a draw method.
 * @author Daniel Ramke
 * @since 1.0.2
 */
@SuppressWarnings("unused")
public class NanoText {

    private final long context;
    private final Window window;

    /**
     * This constructor creates an instance of this.
     * The constructor is absolute needed for using this class.
     * @param window - the window which give us the contextID.
     */
    public NanoText(@NotNull Window window) {
        this.context = window.getContextID();
        this.window = window;
    }

    /**
     * This method rendered a text to the window.
     * The text can be split in multiply lines.
     * @param labeled - the labeled component.
     */
    public void text(@NotNull Labeled labeled) {
        if(checks(labeled)) return;

        String text = labeled.getText();
        if(!(canRenderText(text))) return;

        Component container = labeled.getContainer();

        ByteBuffer buffer;
        List<TextLine> lines;
        Vector2f box;
        if(Cache.contains(container.getLocalizedName())) {
            Cache cache = Cache.get(container.getLocalizedName());
            if(!labeled.isUpdated()) {
                buffer = MemoryUtil.memUTF8(text, false);
                lines = generateLines(buffer, labeled);
                cache.setBuffer(buffer);
                cache.setLines(lines);
                cache.setBox(calculateBox(labeled, lines));
                labeled.update();
            }
            box = cache.getBox();
            lines = cache.getLines();
        } else {
            Cache cache = new Cache(container.getLocalizedName());
            buffer = MemoryUtil.memUTF8(text, false);
            lines = generateLines(buffer, labeled);
            cache.setBuffer(buffer);
            cache.setLines(lines);
            cache.setBox(calculateBox(labeled, lines));
            box = cache.getBox();
            Cache.put(cache);
            labeled.update();
        }
        textLocation(labeled, box);

        for(TextLine line : lines) {
            display(labeled.getTextAbsoluteX(), labeled.getTextAbsoluteY() + (labeled.getFontPattern().getFontSize() * line.line()), labeled.getFontPattern(), line.start(), line.end());
        }

        if(window.isClosed()) {
            for(Cache cache : Cache.getCacheList()) {
                ByteBuffer data = cache.getBuffer();
                if(data != null) MemoryUtil.memFree(data);
                List<TextLine> textLines = cache.getLines();
                if(textLines != null && !textLines.isEmpty()) textLines.clear();
            }
            Cache.getCacheList().clear();
        }
    }

    public void editableTextArea() {

    }

    /**
     * This method checks the default pattern for errors.
     * If there was found a problem then returned this method true.
     * @param labeled - the label to check.
     * @return boolean - the check state.
     */
    private boolean checks(Labeled labeled) {
        if(context <= MemoryUtil.NULL) return true;

        this.createFontMem(labeled);
        Font font = labeled.getFont();
        FontPattern pattern = labeled.getFontPattern();
        if(font == null) return true;
        if(pattern == null) return true;

        String faceName = pattern.getFontFace();
        if(!faceName.contains("-")) faceName = font.getName() + "-" + faceName;
        NanoVG.nvgFontFace(context, faceName);
        NanoVG.nvgFontSize(context, (float) pattern.getFontSize());
        NanoVG.nvgFontBlur(context, (float) pattern.getFontBlur());
        NanoVG.nvgTextAlign(context, NanoVG.NVG_ALIGN_LEFT | NanoVG.NVG_ALIGN_TOP);
        return false;
    }

    /**
     * This method alignment the text box.
     * @param labeled - the labeled component which stored the information.
     * @param box - the text box.
     */
    private void textLocation(Labeled labeled, Vector2f box) {
        Component holder = labeled.getContainer();
        Insets padding = holder.getStyle().getPadding();

        double minX = (holder.getAbsoluteX() + padding.getLeft()) + (holder.getWidth() - labeled.getLabelWidth());
        double minY = holder.getAbsoluteY() + padding.getTop();
        double maxX = ((holder.getAbsoluteX() + labeled.getLabelWidth()) - (box.x() + padding.getRight()));
        double maxY = ((holder.getAbsoluteY() + labeled.getLabelHeight()) - (box.y() + padding.getBottom()));

        double textX = minX + labeled.getTextPositionX();
        double textY = minY + labeled.getTextPositionY();

        textX = Math.min(textX, maxX);
        textX = Math.max(textX, minX);
        textY = Math.min(textY, maxY);
        textY = Math.max(textY, minY);

        switch (labeled.getTextAlignment()) {
            case TOP_LEFT -> labeled.setTextAbsolute(minX, minY);
            case TOP_RIGHT -> labeled.setTextAbsolute(maxX, minY);
            case TOP_CENTER -> {
                double centerX = minX + ((labeled.getLabelWidth() / 2) - (box.x() / 2));
                labeled.setTextAbsolute(centerX, minY);
            }
            case BOTTOM_LEFT -> labeled.setTextAbsolute(minX, maxY);
            case BOTTOM_RIGHT -> labeled.setTextAbsolute(maxX, maxY);
            case BOTTOM_CENTER -> {
                double centerX = minX + ((labeled.getLabelWidth() / 2) - (box.x() / 2));
                labeled.setTextAbsolute(centerX, maxY);
            }
            case CENTER_LEFT -> {
                double centerY = minY + ((holder.getHeight() / 2) - (box.y() / 2));
                labeled.setTextAbsolute(minX, centerY);
            }
            case CENTER_RIGHT -> {
                double centerY = minY + ((holder.getHeight() / 2) - (box.y() / 2));
                labeled.setTextAbsolute(maxX, centerY);
            }
            case CENTER -> {
                double centerX = minX + ((labeled.getLabelWidth() / 2) - (box.x() / 2));
                double centerY = minY + ((holder.getHeight() / 2) - (box.y() / 2));
                labeled.setTextAbsolute(centerX, centerY);
            }
            case NOTHING -> labeled.setTextAbsolute(textX, textY);
        }
    }

    /**
     * This method creates a list with the generated text lines.
     * If the text to long for one line, this method will help us.
     * It's break the text in multiply lines and save them as TextLine record to the list.
     * @param in - the bytebuffer which stored the text.
     * @param labeled - the labeled component which give the font options.
     * @return List<TextLine> - the finished text line list.
     */
    private List<TextLine> generateLines(ByteBuffer in, Labeled labeled) {
        List<TextLine> lines = new ArrayList<>();
        int maxRows = 1;
        if(labeled.isWarp()) {
            maxRows = labeled.getMaxRows();
        }

        long pointerStart = MemoryUtil.memAddress(in);
        long pointerEnd = pointerStart + in.remaining();
        try(NVGTextRow.Buffer buffer = NVGTextRow.calloc(maxRows)) {
            int locatedLines = NanoVG.nnvgTextBreakLines(context, pointerStart, pointerEnd
                    , (float) (labeled.getFontPattern().getTextDirection() == TextDirection.HORIZONTAL ? labeled.getLabelWidth() : labeled.getLabelHeight())
                    , MemoryUtil.memAddress(buffer), maxRows);
            if(locatedLines != 0) {
                int line = 0;
                for(NVGTextRow row : buffer) {
                    if(row.start() != 0 && row.end() != 0) {
                        lines.add(new TextLine(line, row.start(), row.end()));
                    }
                    line++;
                }
            }
            buffer.clear();
        }
        return lines;
    }

    /**
     * This method displayed the given text.
     * @param x - the render x position.
     * @param y - the render y position.
     * @param pattern - the font design pattern.
     * @param start - the long start of text.
     * @param end - the long end of text.
     */
    private void display(double x, double y, FontPattern pattern, long start, long end) {
        NanoVG.nvgSave(context);
        NanoVG.nvgBeginPath(context);

        NanoVG.nvgFillColor(context, pattern.getTextColor().getNanoVGColor());

        NanoVG.nnvgText(context, (float) x, (float) y, start, end);
        NanoVG.nvgRestore(context);
    }

    /**
     * This method calculates the current text box.
     * @param labeled - the labeled component for the options.
     * @param lines - the lines for the box.
     * @return Vector - the finished text box.
     */
    private Vector2f calculateBox(Labeled labeled, List<TextLine> lines) {
        Vector2f vector2f = new Vector2f(0, 0);
        int width = 0;
        int height = 0;
        for(TextLine line : lines) {
            int tmpWidth = calculateTextWidth(longToText(line.start(), line.end()), labeled);
            if(width < tmpWidth) {
                width = tmpWidth;
            }
            height = (int) (height + labeled.getFontPattern().getFontSize());
        }
        vector2f.set(width, height);
        return vector2f;
    }

    /**
     * This method generates a text form longs.
     * @param start - the start text pointer.
     * @param end - the end text pointer.
     * @return String - the build text string.
     */
    private String longToText(long start, long end) {
        return MemoryUtil.memUTF8(start, (MemoryUtil.memUTF8(start).length() - MemoryUtil.memUTF8(end).length()));
    }

    /**
     * This method calculate the current text width and returned the width.
     * @param text - the text for the check.
     * @param labeled - the labeled component for stored information.
     * @return integer - the calculated text width.
     */
    private int calculateTextWidth(String text, Labeled labeled) {
        float[] buffer = new float[4];

        Font font = labeled.getFont();
        FontPattern pattern = labeled.getFontPattern();
        if(font == null) return 0;
        if(pattern == null) return 0;
        String faceName = pattern.getFontFace();
        if(!faceName.contains("-")) faceName = font.getName() + "-" + faceName;
        if(NanoVG.nvgFindFont(context, faceName) <= -1) return 0;

        NanoVG.nvgFontFace(context, faceName);
        NanoVG.nvgFontSize(context, (float) pattern.getFontSize());
        NanoVG.nvgTextBounds(context, 0, 0, text, buffer);
        return (int) buffer[2];
    }

    /**
     * This method create a new font reserved memory address.
     * The font can than called by the given font id.
     * @param labeled - the labeled which stored the font face.
     */
    private void createFontMem(Labeled labeled) {
        Font font = labeled.getFont();
        FontPattern pattern = labeled.getFontPattern();
        if(font == null) return;
        if(pattern == null) return;

        String faceName = pattern.getFontFace();
        if(!faceName.contains("-")) faceName = font.getName() + "-" + faceName;
        if(NanoVG.nvgFindFont(context, faceName) >= 0) return;
        NanoVG.nvgCreateFontMem(context, faceName, font.getFace(faceName).getBuffer(), 0);
    }

    /**
     * This method checks a text for readable.
     * If the text not readable then returned this method false.
     * @param text - the text constance.
     * @return boolean - the text state.
     */
    private boolean canRenderText(String text) {
        if(text == null) {
            return false;
        }
        return !text.isBlank() || !text.isEmpty();
    }

    /**
     * This record save a line for the multi line usage.
     * If a text to large for one line than will the renderer split this line.
     * The lines will be saved as this record for easy usage.
     * @param line - the line index number.
     * @param start - the long start index.
     * @param end - the long end index.
     */
    private record TextLine(int line, long start, long end) { }

    /**
     * The cache class is in private use for the main nano class.
     * The cache is used to save calculations, buffers or list, which doesn't need more times creation.
     * This cache can be cleared if the window ready to close.
     * @author Daniel Ramke
     * @since 1.0.2
     */
    private static class Cache {

        private static final List<Cache> cacheList = new ArrayList<>();

        private final String objectName;
        private ByteBuffer buffer;
        private List<TextLine> lines;
        private Vector2f box;

        /**
         * This constructor creates a new Cache object for stored thinks.
         * The thinks ar methods which calculate math.
         * @param objectName - the objective component name (like localizedNames).
         */
        public Cache(String objectName) {
            this.objectName = objectName;
        }

        /**
         * @return String - the final object name.
         */
        public String getObjectName() {
            return objectName;
        }

        /**
         * This is the stored text bytebuffer.
         * Need by change after change the text.
         * @param buffer - the new text buffer.
         */
        public void setBuffer(ByteBuffer buffer) {
            this.buffer = buffer;
        }

        /**
         * @return ByteBuffer - the current used buffered text.
         */
        public ByteBuffer getBuffer() {
            return buffer;
        }

        /**
         * This method change the list of text lines if a change detected.
         * @param lines - the new lines array.
         */
        public void setLines(List<TextLine> lines) {
            this.lines = lines;
        }

        /**
         * @return List<TextLine> - the current text lines.
         */
        public List<TextLine> getLines() {
            return lines;
        }

        /**
         * This saved the calculated text box.
         * @param box - the new text box.
         */
        public void setBox(Vector2f box) {
            this.box = box;
        }

        /**
         * @return Vector - the current text box.
         */
        public Vector2f getBox() {
            return box;
        }

        /**
         * This method but a new cache object to the handle list.
         * @param cache - the created cache object.
         */
        public static void put(Cache cache) {
            if(contains(cache.getObjectName())) return;
            cacheList.add(cache);
        }

        /**
         * This method checks if an object cached or not.
         * @param objectName - the final name of the object.
         * @return boolean - the state of exist.
         */
        public static boolean contains(String objectName) {
            return get(objectName) != null;
        }

        /**
         * This method returned the fonded cache object by name.
         * @param objectName - the final object name for search.
         * @return Cache - the cache instance of the founded object. Can be null!
         */
        public static Cache get(String objectName) {
            Cache cache = null;
            for(Cache caches : cacheList) {
                if(caches.getObjectName().equals(objectName)) {
                    cache = caches;
                    break;
                }
            }
            return cache;
        }

        /**
         * @return List<Cache> - the complete cache list.
         */
        public static List<Cache> getCacheList() {
            return cacheList;
        }
    }

}
