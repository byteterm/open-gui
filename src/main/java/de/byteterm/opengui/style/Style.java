package de.byteterm.opengui.style;

import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.graphics.paint.Gradient;
import de.byteterm.opengui.graphics.paint.Image;
import de.byteterm.opengui.style.types.Background;
import de.byteterm.opengui.style.types.Border;
import de.byteterm.opengui.tools.formal.Insets;
import de.byteterm.opengui.tools.formal.Radius;

/**
 * This class defined the optical look-out for components or all thinks which used this class.
 * If you need to change the design of a component then use css or the hard code StyleBuilder.
 * For a better usage go to our website: [LINK].
 * @author Daniel Ramke
 * @since 1.0.1
 * @see StyleBuilder
 */
@SuppressWarnings("unused")
public class Style {

    private int zIndex;
    private Background background;
    private Border border;
    private Insets padding;
    private Insets margin;

    /**
     * This constructor is the custom style sheet constructor for elements.
     * You can use it directly or with css and style builders.
     * @param background - the background object.
     * @param border - the border object.
     * @param zIndex - the render order number can be negative or positive.
     * @param padding - the inner bounds to other components.
     * @param margin - the outer bounds to other components.
     */
    public Style(Background background, Border border, int zIndex, Insets padding, Insets margin) {
        this.background = background;
        this.border = border;
        this.zIndex = zIndex;
        this.padding = padding;
        this.margin = margin;
    }

    /*
     * *************************************************
     * Background methods
     * *************************************************
     */

    /**
     * @param background - set a complete new background.
     */
    public void setBackground(Background background) {
        this.background = background;
    }

    /**
     * @param color - change background to color.
     */
    public void setBackgroundColor(Color color) {
        background.setType(Background.Type.COLOR);
        background.setColor(color);
    }

    /**
     * @param image - change background to image.
     */
    public void setBackgroundImage(Image image) {
        background.setType(Background.Type.IMAGE);
        background.setImage(image);
    }

    /**
     * @param gradient - change background to gradient.
     */
    public void setBackgroundGradient(Gradient gradient) {
        background.setType(Background.Type.LINEAR_GRADIENT);
        background.setGradient(gradient);
    }

    /**
     * @param radius - set the background radius.
     */
    public void setBackgroundRadius(Radius radius) {
        background.setRadius(radius);
    }

    /**
     * @return background - the current created background object.
     */
    public Background getBackground() {
        return this.background;
    }

    /*
     * *************************************************
     * Background methods
     * *************************************************
     */

    /**
     * @param border - set a complete new border.
     */
    public void setBorder(Border border) {
        this.border = border;
    }

    /**
     * @param color - change border to color.
     */
    public void setBorderColor(Color color) {
        border.setType(Border.Type.COLOR);
        border.setColor(color);
    }

    /**
     * @param gradient - change background to gradient.
     */
    public void setBorderGradient(Gradient gradient) {
        border.setType(Border.Type.LINEAR_GRADIENT);
        border.setGradient(gradient);
    }

    /**
     * @param radius - set the border radius.
     */
    public void setBorderRadius(Radius radius) {
        border.setRadius(radius);
    }

    /**
     * @param stroke - set the border stroke.
     */
    public void setBorderStroke(Insets stroke) {
        border.setStroke(stroke);
    }

    /**
     * @param style - set the border style.
     */
    public void setBorderStyle(Border.Style style) {
        border.setStyle(style);
    }

    /**
     * @return Border - the current created border object.
     */
    public Border getBorder() {
        return border;
    }

    /*
     * *************************************************
     * Z-index methods
     * *************************************************
     */

    /**
     * This method change the current z-index.
     * The index represent the render order of elements.
     * Child elements import the z-index of here parents.
     * @param zIndex - the new z-index can be negative or positive number.
     */
    public void setZIndex(int zIndex) {
        this.zIndex = zIndex;
    }

    /**
     * @return integer - current z-index from element.
     */
    public int getZIndex() {
        return this.zIndex;
    }

    /*
     * *************************************************
     * Padding and Margin methods
     * *************************************************
     */

    /**
     * This method change the current padding.
     * Padding is the inner bounds to other components.
     * @param padding - the new padding.
     */
    public void setPadding(Insets padding) {
        this.padding = padding;
    }

    /**
     * @return Insets - the current component padding.
     */
    public Insets getPadding() {
        return padding;
    }

    /**
     * This method change the current margin.
     * Margin is the outer bounds to other components.
     * @param margin - the new margin.
     */
    public void setMargin(Insets margin) {
        this.margin = margin;
    }

    /**
     * @return Insets - the current component margin.
     */
    public Insets getMargin() {
        return margin;
    }
}
