package de.byteterm.opengui.style;

import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.graphics.paint.Gradient;
import de.byteterm.opengui.graphics.paint.Image;
import de.byteterm.opengui.style.types.Background;
import de.byteterm.opengui.style.types.Border;
import de.byteterm.opengui.tools.formal.Insets;
import de.byteterm.opengui.tools.formal.Radius;

/**
 * This StyleBuilder is used to create a Style class.
 * You can use CSS for this or this class which is for old developers :p.
 * @author Daniel Ramke
 * @since 1.0.1
 * @see Style
 */
@SuppressWarnings("unused")
public class StyleBuilder {

    private int zIndex;
    private Background background;
    private Border border;
    private Insets padding;
    private Insets margin;

    /**
     * This constructor is used for style creations.
     * Note that this class is a builder class.
     */
    public StyleBuilder() {
        this.background = Background.get(Color.WHITE);
        this.border = Border.get(Color.BLACK);
        border.setStyle(Border.Style.NONE);
        this.zIndex = 0;
        this.padding = new Insets(0);
        this.margin = new Insets(0);
    }

    /*
     * *************************************************
     * Background methods
     * *************************************************
     */

    /**
     * This constructor is called for background building.
     * @param background - the new background.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder background(Background background) {
        this.background = background;
        return this;
    }

    /**
     * This constructor is called for backgroundColor building.
     * @param color - the new Color.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder backgroundColor(Color color) {
        Radius radius = background.getRadius();
        this.background = Background.get(color);
        this.background.setRadius(radius);
        return this;
    }

    /**
     * This constructor is called for backgroundImage building.
     * @param image - the new Image.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder backgroundImage(Image image) {
        Radius radius = background.getRadius();
        this.background = Background.get(image);
        this.background.setRadius(radius);
        return this;
    }

    /**
     * This constructor is called for backgroundGradient building.
     * @param gradient - the new Gradient.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder backgroundGradient(Gradient gradient) {
        Radius radius = background.getRadius();
        this.background = Background.get(gradient);
        this.background.setRadius(radius);
        return this;
    }

    /**
     * This constructor is called for backgroundRadius building.
     * @param radius - the new background radius.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder backgroundRadius(Radius radius) {
        this.background.setRadius(radius);
        return this;
    }

    /*
     * *************************************************
     * Background methods
     * *************************************************
     */

    /**
     * This constructor is called for border building.
     * @param border - the new border.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder border(Border border) {
        this.border = border;
        return this;
    }

    /**
     * This constructor is called for border building.
     * @param color - the new border color.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder borderColor(Color color) {
        border.setColor(color);
        border.setType(Border.Type.COLOR);
        return this;
    }

    /**
     * This constructor is called for border building.
     * @param gradient - the new border gradient.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder borderGradient(Gradient gradient) {
        border.setGradient(gradient);
        border.setType(Border.Type.LINEAR_GRADIENT);
        return this;
    }

    /**
     * This constructor is called for border building.
     * @param radius - the new border radius.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder borderRadius(Radius radius) {
        this.border.setRadius(radius);
        return this;
    }

    /**
     * This constructor is called for border building.
     * @param stroke - the new border stroke.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder borderStroke(Insets stroke) {
        this.border.setStroke(stroke);
        return this;
    }

    /**
     * This constructor is called for border building.
     * @param style - the new border style.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder borderStyle(Border.Style style) {
        this.border.setStyle(style);
        return this;
    }

    /*
     * *************************************************
     * Z-index methods
     * *************************************************
     */

    /**
     * This constructor is called for z indexing building.
     * @param zIndex - the new z index (parent can change this!).
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder zIndex(int zIndex) {
        this.zIndex = zIndex;
        return this;
    }

    /*
     * *************************************************
     * Padding methods
     * *************************************************
     */

    /**
     * This constructor is called for padding building.
     * @param padding - the new padding insets.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder padding(Insets padding) {
        this.padding = padding;
        return this;
    }

    /**
     * This constructor is called for padding building.
     * @param paddingTop - the new top padding.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder paddingTop(double paddingTop) {
        this.padding.set(paddingTop, padding.getBottom(), padding.getLeft(), padding.getRight());
        return this;
    }

    /**
     * This constructor is called for padding building.
     * @param paddingBottom - the new bottom padding.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder paddingBottom(double paddingBottom) {
        this.padding.set(padding.getTop(), paddingBottom, padding.getLeft(), padding.getRight());
        return this;
    }

    /**
     * This constructor is called for padding building.
     * @param paddingLeft - the new left padding.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder paddingLeft(double paddingLeft) {
        this.padding.set(padding.getTop(), padding.getBottom(), paddingLeft, padding.getRight());
        return this;
    }

    /**
     * This constructor is called for padding building.
     * @param paddingRight - the new right padding.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder paddingRight(double paddingRight) {
        this.padding.set(padding.getTop(), padding.getBottom(), padding.getLeft(), paddingRight);
        return this;
    }

    /*
     * *************************************************
     * Padding methods
     * *************************************************
     */

    /**
     * This constructor is called for margin building.
     * @param margin - the new margin insets.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder margin(Insets margin) {
        this.margin = margin;
        return this;
    }

    /**
     * This constructor is called for margin building.
     * @param marginTop - the new top margin.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder marginTop(double marginTop) {
        this.margin.set(marginTop, margin.getBottom(), margin.getLeft(), margin.getRight());
        return this;
    }

    /**
     * This constructor is called for margin building.
     * @param marginBottom - the new bottom margin.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder marginBottom(double marginBottom) {
        this.margin.set(margin.getTop(), marginBottom, margin.getLeft(), margin.getRight());
        return this;
    }

    /**
     * This constructor is called for margin building.
     * @param marginLeft - the new left margin.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder marginLeft(double marginLeft) {
        this.margin.set(margin.getTop(), margin.getBottom(), marginLeft, margin.getRight());
        return this;
    }

    /**
     * This constructor is called for margin building.
     * @param marginRight - the new right margin.
     * @return StyleBuilder - the builder constructor.
     */
    public StyleBuilder marginRight(double marginRight) {
        this.margin.set(margin.getTop(), margin.getBottom(), margin.getLeft(), marginRight);
        return this;
    }

    /*
     * *************************************************
     * Build method
     * *************************************************
     */

    /**
     * @return Style - the finished build.
     */
    public Style build() {
        return new Style(background, border, zIndex, padding, margin);
    }

}
