package de.byteterm.opengui.style.types;

import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.graphics.paint.Gradient;
import de.byteterm.opengui.tools.formal.Insets;
import de.byteterm.opengui.tools.formal.Radius;

/**
 * This class creates a border object for us.
 * Depending on the type, a border object can have new properties.
 * The default type is COLOR.
 * With this type we are allowed to use a single color as border.
 * With the type LINEAR_GRADIENT you can use a gradient.
 * For more information visit our website: [LINK].
 * @author Daniel Ramke
 * @since 1.0.2
 * @see Color
 * @see Gradient
 */
@SuppressWarnings("unused")
public class Border {

    private Type type;

    private Color color;
    private Gradient gradient;
    private Style style;
    private Radius radius;
    private Insets stroke;


    public Border(Type type) {
        this.type = type;
        this.setStyle(Style.SOLID);
        this.setRadius(new Radius(0));
        this.setStroke(new Insets(1));
        this.setColor(Color.GRAY);
        this.setGradient(Gradient.get(Color.LIGHT_GRAY, Color.GRAY));
    }

    /**
     * This constructor creates a new background object with a specified color.
     * The constructor called the default constructor too.
     * @param color - the specified color.
     */
    public Border(Color color) {
        this(Type.COLOR);
        this.setColor(color);
    }

    /**
     * This constructor creates a new background object with a specified color gradient.
     * The constructor called the default constructor too.
     * @param gradient - the specified color gradient.
     */
    public Border(Gradient gradient) {
        this(Type.LINEAR_GRADIENT);
        this.setGradient(gradient);
    }

    /**
     * This method set the current border style.
     * The default style is NONE.
     * @param style - the wish border style.
     */
    public void setStyle(Style style) {
        this.style = style;
    }

    /**
     * @return Border#Style - the current border style.
     */
    public Style getStyle() {
        return style;
    }

    /**
     * This method change the border stroke.
     * @param stroke - the stroke of the border.
     */
    public void setStroke(Insets stroke) {
        this.stroke = stroke;
    }

    /**
     * @return Insets - the current border stroke.
     */
    public Insets getStroke() {
        return stroke;
    }

    /**
     * This method change the current border radius.
     * @param radius - the new border radius.
     */
    public void setRadius(Radius radius) {
        this.radius = radius;
    }

    /**
     * @return Radius - the current border radius.
     */
    public Radius getRadius() {
        return radius;
    }

    /**
     * This change the current border color.
     * This will by default set to gray.
     * @param color - the new color.
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * @return Color - the current color.
     */
    public Color getColor() {
        return color;
    }

    /**
     * This method change the current linear gradient.
     * @param gradient - the color gradient.
     */
    public void setGradient(Gradient gradient) {
        this.gradient = gradient;
    }

    /**
     * @return Gradient - the current gradient.
     */
    public Gradient getGradient() {
        return gradient;
    }

    /**
     * This method change the type of this border.
     * @param type - the new used type.
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * @return Border#Type - the current used border type.
     */
    public Type getType() {
        return type;
    }

    /**
     * This method creates a new border color type.
     * @param color - the color for the border object.
     * @return Border - created border object.
     */
    public static Border get(Color color) {
        return new Border(color);
    }

    /**
     * This method creates a new border color gradient type.
     * @param gradient - the color gradient for the border object.
     * @return Border - created border object.
     */
    public static Border get(Gradient gradient) {
        return new Border(gradient);
    }

    /**
     * This enum stored the current allowed border styles.
     * By default, is the NONE style in use.
     * You can add styles by write an issues with the formal for the border code.
     * @author Daniel Ramke
     * @since 1.0.2
     */
    public enum Style {
        NONE("none"),
        DOTTED("dot"),
        LINE("line"),
        SOLID("solid");

        private final String cssName;

        Style(String cssName) {
            this.cssName = cssName;
        }

        /**
         * @return String - the css name of this style.
         */
        public String getCssName() {
            return cssName;
        }

        /**
         * This method searched for the given css name.
         * If the css name was found the return object is an enum type.
         * @param cssName - the key to search.
         * @return Style - an instance of this enum.
         */
        public static Style get(String cssName) {
            Style style = NONE;
            for(Style styles : values()) {
                if(styles.getCssName().equals(cssName)) {
                    style = styles;
                    break;
                }
            }
            return style;
        }
    }

    /**
     * This enum provides the existing border types.
     * The types ar needed for the renderer it will call the correct methods.
     * In the type enum, you can get the attribute names from css for help.
     * If you use css instanceof the default builder system note that this is not all attribute names which exist!
     * For more information visit our website [LINK].
     * @author Daniel Ramke
     * @since 1.0.2
     */
    public enum Type {

        COLOR("color", new String[]{"rgb", "rgba", "#"}),
        LINEAR_GRADIENT("color", new String[]{"linear-gradient"});

        private final String tag;
        private final String[] css_func_aliases;

        Type(String tag, String[] css_func_aliases) {
            this.tag = tag;
            this.css_func_aliases = css_func_aliases;
        }

        /**
         * @return String - the named end tag from css variables.
         */
        public String getTag() {
            return tag;
        }

        /**
         * @return String[] - the aliases which contains in properties of css attributes.
         */
        public String[] getCss_func_aliases() {
            return css_func_aliases;
        }
    }

}
