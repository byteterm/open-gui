package de.byteterm.opengui.style.types;

import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.tools.geometry.TextDirection;

/**
 * This class controls the font design, and his information.
 * The class is used for all renderers which used fonts.
 * @author Daniel Ramke
 * @since 1.0.2
 */
@SuppressWarnings("unused")
public class FontPattern {

    private double fontSize;
    private double fontBlur;
    private double lineHeight;
    private String fontFace;
    private TextDirection textDirection;
    private Color textColor;

    /**
     * This constructor creates the new font pattern.
     * @param fontFace - the font face.
     * @param textColor - the text color.
     * @param fontSize - the font face size.
     * @param lineHeight - the text line height.
     * @param fontBlur - the text blur.
     * @param textDirection - the text render direction.
     */
    public FontPattern(String fontFace, Color textColor, double fontSize, double lineHeight, double fontBlur, TextDirection textDirection) {
        this.fontFace = fontFace;
        this.textColor = textColor;
        this.fontSize = fontSize;
        this.lineHeight = lineHeight;
        this.fontBlur = fontBlur;
        this.textDirection = textDirection;
    }

    /**
     * @param fontFace - the new font face.
     */
    public void setFontFace(String fontFace) {
        this.fontFace = fontFace;
    }

    /**
     * @return String - the current font face.
     */
    public String getFontFace() {
        return fontFace;
    }

    /**
     * @param textColor - the new text color.
     */
    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    /**
     * @return Color - the current text color.
     */
    public Color getTextColor() {
        return textColor;
    }

    /**
     * @param fontSize - the new font size.
     */
    public void setFontSize(double fontSize) {
        this.fontSize = fontSize;
    }

    /**
     * @return double - the current font size.
     */
    public double getFontSize() {
        return fontSize;
    }

    /**
     * @param lineHeight - the new text line height.
     */
    public void setLineHeight(double lineHeight) {
        this.lineHeight = lineHeight;
    }

    /**
     * @return double - the current text line height.
     */
    public double getLineHeight() {
        return lineHeight;
    }

    /**
     * @param fontBlur - the new font blur.
     */
    public void setFontBlur(double fontBlur) {
        this.fontBlur = fontBlur;
    }

    /**
     * @return double - the current font blur.
     */
    public double getFontBlur() {
        return fontBlur;
    }

    /**
     * @param textDirection - the new text direction.
     */
    public void setTextDirection(TextDirection textDirection) {
        this.textDirection = textDirection;
    }

    /**
     * @return TextDirection - the current text direction.
     */
    public TextDirection getTextDirection() {
        return textDirection;
    }
}
