package de.byteterm.opengui.system;

import de.byteterm.jlogger.Logger;
import de.byteterm.jlogger.util.ConsoleColor;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * The monitor class is used to create an object from the physical monitor.
 * The monitor object can then be used in different classes.
 * You can get the resolution, name and memory address.
 * @author Daniel Ramke
 * @since 1.0.1
 */
@SuppressWarnings("unused")
public class Monitor {

    private static final Logger logger = Logger.getLogger();

    private static final List<Monitor> monitors = new ArrayList<>();

    /*
     * *************************************************
     * Identifier variables
     * *************************************************
     */

    private final long address;
    private final String name;
    private final String identifier;

    /*
     * *************************************************
     * Data variables
     * *************************************************
     */

    private final String resolution;
    private final String dimension;
    private final double width;
    private final double height;
    private final int physicalWidth;
    private final int physicalHeight;
    private final float refreshRate;
    private final int[] bits;
    private final boolean primary;

    /*
     * *************************************************
     * Constructor
     * *************************************************
     */

    /**
     * This constructor generates a normal monitor object.
     * This object can be used to catch needed monitor information.
     * @param identifier - the final name of the monitor.
     * @param address - the memory address location.
     * @param name - the real name from the driver.
     * @param width - the display width.
     * @param height - the display height.
     * @param refreshRate - the default refresh rate.
     * @param bits - the specified bits: red, green and blue.
     */
    public Monitor(String identifier, long address, String name
            , double width, double height, int physicalWidth
            , int physicalHeight, float refreshRate, int[] bits, boolean primary) {
        this.address = address;
        this.identifier = identifier;
        this.name = name;
        this.width = width;
        this.height = height;
        this.resolution = width + "x" + height;
        this.physicalWidth = physicalWidth;
        this.physicalHeight = physicalHeight;
        this.dimension = physicalWidth + "x" + physicalHeight;
        this.refreshRate = refreshRate;
        this.bits = bits;
        if(bits.length != 3) {
            logger.warn("You need red, green and blue bits!");
        }
        this.primary = primary;
        if(getMonitor(identifier) != null) {
            logger.warn("Monitor with the name [ "
                    + ConsoleColor.RED + identifier + ConsoleColor.RESET +
                    " ] was found! Monitor can't be added to the list!");
            return;
        }
        monitors.add(this);
        monitors.sort(Comparator.comparing(Monitor::getIdentifier));
        logger.debug("Added monitor with the name [ "
                + ConsoleColor.GREEN + identifier + ConsoleColor.RESET +
                " ] at address [ "
                + ConsoleColor.CYAN + address + ConsoleColor.RESET +
                " ]");
    }

    /**
     * @return long - memory address of the monitor.
     */
    public long getAddress() {
        return this.address;
    }

    /**
     * @return string - final name given by the user.
     */
    public String getIdentifier() {
        return this.identifier;
    }

    /**
     * @return string - the correct driver name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return string - resolution of the monitor.
     */
    public String getResolution() {
        return this.resolution;
    }

    /**
     * @return double - monitor work place width.
     */
    public double getWidth() {
        return this.width;
    }

    /**
     * @return double - monitor work place height.
     */
    public double getHeight() {
        return this.height;
    }

    /**
     * @return string - the physical monitor size.
     */
    public String getDimension() {
        return this.dimension;
    }

    /**
     * @return int - physical monitor width in mm.
     */
    public int getPhysicalWidth() {
        return this.physicalWidth;
    }

    /**
     * @return int - physical monitor height in mm.
     */
    public int getPhysicalHeight() {
        return this.physicalHeight;
    }

    /**
     * @return float - the refresh rate of the monitor.
     */
    public float getRefreshRate() {
        return this.refreshRate;
    }

    /**
     * @return int[] - bits of the monitor.
     */
    public int[] getBits() {
        return this.bits;
    }

    /**
     * @return boolean - is monitor the main desktop.
     */
    public boolean isPrimary() {
        return this.primary;
    }

    /**
     * This method storage all settings and attributes from the monitor as string.
     * @return string - all settings and attributes.
     */
    @Override
    public String toString() {
        return "{ID=" + identifier + ", Primary=" + primary + ", Address=" + address +
                ", Name=" + name + ", Resolution=" + resolution +
                ", Specs[width=" + width + ", height=" + height +
                ", refresh-rate=" + refreshRate + ", "
                + (bits.length != 3 ? "error:not correct format" : "red="
                + bits[0] + ", green=" + bits[1] + ", blue=" + bits[2]) + "]}";
    }

    /**
     * This method searched for a specified monitor by the given name.
     * @param identifier - the final name to search.
     * @return monitor - null if the monitor was not found.
     */
    public static Monitor getMonitor(String identifier) {
        Monitor monitor = null;
        for(Monitor monitors : monitors) {
            if(monitors.getIdentifier().equals(identifier)) {
                monitor = monitors;
                break;
            }
        }
        return monitor;
    }

    /**
     * This method searched for the system primary monitor.
     * If no monitor was found, the method will return null.
     * @return monitor - the primary monitor.
     */
    public static Monitor getPrimaryMonitor() {
        Monitor monitor = null;
        for(Monitor monitors : monitors) {
            if(monitors.isPrimary()) {
                monitor = monitors;
                break;
            }
        }
        return monitor;
    }

    /**
     * @return List<Monitor> - list of all registered monitors.
     */
    public static List<Monitor> getMonitors() {
        return monitors;
    }

}
