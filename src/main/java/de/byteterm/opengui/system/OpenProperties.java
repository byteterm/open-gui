package de.byteterm.opengui.system;

import de.byteterm.jlogger.Logger;

/**
 * Open Properties is the class with the respective configurations for OpenGUI.
 * Note that the configurations are set at the start of the library so that a default exists.
 * All settings made here are valid for every window!
 * The default window limit is 5.
 * Note that 5 threads are started with 5 windows!
 * a number above 10 can and will cause problems on some PCs.
 * All errors that happen due to too high a number of threads should not be reported!
 * @author Daniel Ramke
 * @since 1.0.2
 * @see OpenPropertiesBuilder
 */
@SuppressWarnings("unused")
public class OpenProperties {

    private int windowLimit;
    private boolean modernNVG;
    private boolean debugMode;

    /**
     * The default constructor which sets the default values for the library.
     * @param windowLimit - the limited window size.
     * @param modernNVG - true use nvg3 instance of nvg2.
     */
    public OpenProperties(int windowLimit, boolean modernNVG, boolean debugMode) {
        this.windowLimit = windowLimit;
        this.modernNVG = modernNVG;
        this.debugMode = debugMode;
    }

    /**
     * This method change the window limit property.
     * @param windowLimit - the range is recommended (1 - 10).
     */
    public void setWindowLimit(int windowLimit) {
        this.windowLimit = windowLimit;
    }

    /**
     * @return int - current window limit.
     */
    public int getWindowLimit() {
        return this.windowLimit;
    }

    /**
     * This method change the main render system property.
     * @param modernNVG - true use nanoVG 3, false use the older version nanoVG 2.
     */
    public void setModernNVG(boolean modernNVG) {
        this.modernNVG = modernNVG;
    }

    /**
     * @return boolean - modern or non-modern nanoVG usage.
     */
    public boolean isModernNVG() {
        return this.modernNVG;
    }

    /**
     * This method change the debug state of the library.
     * @param debugMode - true printing debug lines to the terminal, false hide debug prints.
     */
    public void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
        Logger.enableDebug(debugMode);
    }

    /**
     * @return boolean - is debug mode enabled or disabled.
     */
    public boolean isDebugEnabled() {
        return debugMode;
    }
}
