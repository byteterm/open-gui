package de.byteterm.opengui.system;

import de.byteterm.jlogger.Logger;

/**
 * Open Properties Builder This class, as the name suggests, is a builder class for creating a properties object.
 * It is used to simply create a new configuration for the library.
 * Note that you always have to use the build() end method at the end of the call.
 * This returns the OpenProperties object.
 * If you only create a builder object without making any settings, standard settings are transferred.
 * @author Daniel Ramke
 * @since 1.0.2
 * @see OpenProperties
 */
public class OpenPropertiesBuilder {

    private int windowLimit;
    private boolean modernNVG;
    private boolean debugMode;

    /**
     * This is the builder constructor which is absolute needed to build your properties.
     */
    public OpenPropertiesBuilder() {
        this.windowLimit = 2;
        this.modernNVG = true;
        this.debugMode = false;
    }

    /**
     * This constructor method change the window limit value.
     * @param windowLimit - the new window limit recommend range (1 - 10).
     * @return this - as build object.
     */
    public OpenPropertiesBuilder windowLimit(int windowLimit) {
        this.windowLimit = windowLimit;
        return this;
    }

    /**
     * This constructor method change the nvg render system value.
     * @param modernNVG - true for modern use. false for older pc's.
     * @return this - as build object.
     */
    public OpenPropertiesBuilder modernNVG(boolean modernNVG) {
        this.modernNVG = modernNVG;
        return this;
    }

    /**
     * This constructor method change the debug mode system value.
     * @param debugMode - true for debug print usage.
     * @return this - as build object.
     */
    public OpenPropertiesBuilder debugMode(boolean debugMode) {
        this.debugMode = debugMode;
        Logger.enableDebug(debugMode);
        return this;
    }

    /**
     * This method is building the results from other constructor methods to create a new OpenProperties object.
     * This object is returned by this method and can initialize as another object.
     * @return OpenProperties - properties object for library.
     */
    public OpenProperties build() {
        return new OpenProperties(windowLimit, modernNVG, debugMode);
    }


}
