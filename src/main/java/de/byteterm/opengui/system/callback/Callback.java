package de.byteterm.opengui.system.callback;

import java.util.ArrayList;
import java.util.List;

/**
 * This abstract class provided a new callback object. The callback can be extended with another sub object.
 * @param <T> the sub object. This is by example glfw callbacks.
 * @author Daniel Ramke
 * @since 1.0.0
 */
public abstract class Callback<T> {

    protected final List<T> callbacks = new ArrayList<>();

    /**
     * This method added a new sub callback to the main callback.
     * @param callback the new sub callback.
     */
    public void add(T callback) {
        if(callback != null) {
            callbacks.add(callback);
        }
    }


    /**
     * This method removed a sub callback witch exist.
     * @param callback the sub callback.
     */
    public void remove(T callback) {
        if(callback != null) {
            callbacks.remove(callback);
        }
    }

}
