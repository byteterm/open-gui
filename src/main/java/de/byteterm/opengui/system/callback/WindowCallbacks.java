package de.byteterm.opengui.system.callback;

import org.lwjgl.glfw.*;

/**
 * This class is a storage class for the window class triggers.
 * This triggers ar absolute needed for all windows which using inputs.
 * For more information visit our website: [LINK].
 * @author Daniel Ramke
 * @since 1.0.0
 * @see Callback
 */
@SuppressWarnings("unused")
public class WindowCallbacks {


    /**
     * This class is the trigger for close action.
     */
    public static class WindowCloseCallback extends Callback<GLFWWindowCloseCallbackI> implements GLFWWindowCloseCallbackI {
        @Override
        public void invoke(long window) {
            for(GLFWWindowCloseCallbackI callback : callbacks) {
                callback.invoke(window);
            }
        }
    }

    /**
     * This class is the trigger for size action.
     */
    public static class WindowSizeCallback extends Callback<GLFWWindowSizeCallbackI> implements GLFWWindowSizeCallbackI {
        @Override
        public void invoke(long window, int width, int height) {
            for(GLFWWindowSizeCallbackI callback : callbacks) {
                callback.invoke(window, width, height);
            }
        }
    }

    /**
     * This class is the trigger for position action.
     */
    public static class WindowPositionCallback extends Callback<GLFWWindowPosCallbackI> implements GLFWWindowPosCallbackI {
        @Override
        public void invoke(long window, int positionX, int positionY) {
            for(GLFWWindowPosCallbackI callback : callbacks) {
                callback.invoke(window, positionX, positionY);
            }
        }
    }

    /**
     * This class is the trigger for focus action.
     */
    public static class WindowFocusCallback extends Callback<GLFWWindowFocusCallbackI> implements GLFWWindowFocusCallbackI {
        @Override
        public void invoke(long window, boolean focused) {
            for(GLFWWindowFocusCallbackI callback : callbacks) {
                callback.invoke(window, focused);
            }
        }
    }

    /**
     * This class is the trigger for maximize action.
     */
    public static class WindowMaximizedCallback extends Callback<GLFWWindowMaximizeCallbackI> implements GLFWWindowMaximizeCallbackI {
        @Override
        public void invoke(long window, boolean maximized) {
            for(GLFWWindowMaximizeCallbackI callback : callbacks) {
                callback.invoke(window, maximized);
            }
        }
    }

    /**
     * This class is the trigger for iconify action.
     */
    public static class WindowIconifyCallback extends Callback<GLFWWindowIconifyCallbackI> implements GLFWWindowIconifyCallbackI {
        @Override
        public void invoke(long window, boolean iconified) {
            for(GLFWWindowIconifyCallbackI callback : callbacks) {
                callback.invoke(window, iconified);
            }
        }
    }

    /**
     * This class is the trigger for refresh action.
     */
    public static class WindowRefreshCallback extends Callback<GLFWWindowRefreshCallbackI> implements GLFWWindowRefreshCallbackI {
        @Override
        public void invoke(long window) {
            for(GLFWWindowRefreshCallbackI callback : callbacks) {
                callback.invoke(window);
            }
        }
    }

    /**
     * This class is the trigger for frame size action.
     */
    public static class FrameBufferSizeCallback extends Callback<GLFWFramebufferSizeCallbackI> implements GLFWFramebufferSizeCallbackI {
        @Override
        public void invoke(long window, int width, int height) {
            for(GLFWFramebufferSizeCallbackI callback : callbacks) {
                callback.invoke(window, width, height);
            }
        }
    }

    /**
     * This class is the trigger for keyboard action.
     */
    public static class KeyCallback extends Callback<GLFWKeyCallbackI> implements GLFWKeyCallbackI {
        @Override
        public void invoke(long window, int key, int scancode, int action, int mods) {
            for(GLFWKeyCallbackI callback : callbacks) {
                callback.invoke(window, key, scancode, action, mods);
            }
        }
    }

    /**
     * This class is the trigger for keyboard action.
     */
    public static class CharCallback extends Callback<GLFWCharCallbackI> implements GLFWCharCallbackI {
        @Override
        public void invoke(long window, int codepoint) {
            for(GLFWCharCallbackI callback : callbacks) {
                callback.invoke(window, codepoint);
            }
        }
    }

    /**
     * This class is the trigger for keyboard action.
     */
    public static class CharModsCallback extends Callback<GLFWCharModsCallbackI> implements GLFWCharModsCallbackI {
        @Override
        public void invoke(long window, int codepoint, int mods) {
            for(GLFWCharModsCallbackI callback : callbacks) {
                callback.invoke(window, codepoint, mods);
            }
        }
    }

    /**
     * This class is the trigger for mouse action.
     */
    public static class MouseButtonCallback extends Callback<GLFWMouseButtonCallbackI> implements GLFWMouseButtonCallbackI {
        @Override
        public void invoke(long window, int button, int action, int mods) {
            for(GLFWMouseButtonCallbackI callback : callbacks) {
                callback.invoke(window, button, action, mods);
            }
        }
    }

    /**
     * This class is the trigger for mouse action.
     */
    public static class MousePositionCallback extends Callback<GLFWCursorPosCallbackI> implements GLFWCursorPosCallbackI {
        @Override
        public void invoke(long window, double positionX, double positionY) {
            for(GLFWCursorPosCallbackI callback : callbacks) {
                callback.invoke(window, positionX, positionY);
            }
        }
    }

    /**
     * This class is the trigger for mouse action.
     */
    public static class MouseEnteredCallback extends Callback<GLFWCursorEnterCallbackI> implements GLFWCursorEnterCallbackI {
        @Override
        public void invoke(long window, boolean entered) {
            for(GLFWCursorEnterCallbackI callback : callbacks) {
                callback.invoke(window, entered);
            }
        }
    }

    /**
     * This class is the trigger for mouse action.
     */
    public static class ScrollCallback extends Callback<GLFWScrollCallbackI> implements GLFWScrollCallbackI {
        @Override
        public void invoke(long window, double xOffset, double yOffset) {
            for(GLFWScrollCallbackI callback : callbacks) {
                callback.invoke(window, xOffset, yOffset);
            }
        }
    }


}
