package de.byteterm.opengui.tools;

import de.byteterm.jlogger.Logger;
import de.byteterm.opengui.ui.Component;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used for working with list and maps.
 * @author Daniel Ramke
 * @since 1.0.2
 */
public class Collections {

    private static final Logger logger = Logger.getLogger();

    /**
     * This method sorted a list of components and removed the needed components.
     * @param list - the list which will remove the components.
     * @param identifier - the localizedName, familyName or groupName.
     */
    public static void reformComponentList(List<Component> list, @NotNull String identifier) {
        List<Component> toRemove = new ArrayList<>();
        for(Component component : list) {
            if(component.getLocalizedName().equals(identifier)) {
                toRemove.add(component);
                break;
            }
            if(component.getFamily().equals(identifier) || component.getGroup().equals(identifier)) {
                toRemove.add(component);
            }
        }
        if(toRemove.isEmpty()) {
            logger.warn("The remove able list is empty... nothing to remove!");
            return;
        }
        for(Component remove : toRemove) {
            list.remove(remove);
        }
        toRemove.clear();
    }

}
