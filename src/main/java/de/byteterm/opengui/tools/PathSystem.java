package de.byteterm.opengui.tools;

import java.net.URL;

/**
 * This class is a storage for random path methods.
 * The class can be used in all others classes.
 * @author Daniel Ramke
 * @since 1.0.2
 */
public final class PathSystem {

    /**
     * This method checked the given path for exist.
     * Note that this method called the resource url of the given class.
     * @param path - the extended path for the resource.
     * @return String - the complete resource path.
     */
    public static String INTERNAL(String path) {
        URL url = PathSystem.class.getResource(path);
        return url == null ? "" : url.getPath();
    }

}
