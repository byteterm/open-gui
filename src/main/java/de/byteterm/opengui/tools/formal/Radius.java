package de.byteterm.opengui.tools.formal;

import de.byteterm.opengui.tools.Maths;

/**
 * Radius class creates an object that calculates all corners.
 * You can use it in the styling section for backgrounds and borders.
 * This class is designed to improve CSS styling and make it easier.
 * @author Daniel Ramke
 * @since 1.0.1
 */
@SuppressWarnings("unused")
public class Radius {

    private double topLeft;
    private double topRight;

    private double bottomLeft;
    private double bottomRight;

    /**
     * This constructor created an empty radius. default set 0.
     */
    public Radius() {
        this.set(0);
    }

    /**
     * This constructor created a radius object with all sides the same.
     * @param radius - of all sides.
     */
    public Radius(double radius) {
        this.set(radius);
    }

    /**
     * This constructor created a radius object with new top and bottom radius.
     * @param top - the top radius.
     * @param bottom - the bottom radius.
     */
    public Radius(double top, double bottom) {
        this.set(top, bottom);
    }

    /**
     * This constructor created a radius object with new top left and right, bottom left and right radius.
     * @param topLeft - the top left radius.
     * @param topRight - the top right radius.
     * @param bottomLeft - the bottom left radius.
     * @param bottomRight - the bottom right radius.
     */
    public Radius(double topLeft, double topRight, double bottomLeft, double bottomRight) {
        this.set(topLeft, topRight, bottomLeft, bottomRight);
    }

    /**
     * This method change the current radius to the same.
     * @param radius - the new radius for all sides.
     */
    public void set(double radius) {
        this.set(radius, radius);
    }

    /**
     * This method change the top and bottom radius.
     * @param top - the new top radius.
     * @param bottom - the new bottom radius.
     */
    public void set(double top, double bottom) {
        this.set(top, top, bottom, bottom);
    }

    /**
     * This method change all sides with differed values.
     * @param topLeft - the top left radius.
     * @param topRight - the top right radius.
     * @param bottomLeft - the bottom left radius.
     * @param bottomRight - the bottom right radius.
     */
    public void set(double topLeft, double topRight, double bottomLeft, double bottomRight) {
        this.topLeft = Maths.changeNegative(topLeft);
        this.topRight = Maths.changeNegative(topRight);
        this.bottomLeft = Maths.changeNegative(bottomLeft);
        this.bottomRight = Maths.changeNegative(bottomRight);
    }

    /**
     * @return double - the current top left radius.
     */
    public double getTopLeft() {
        return this.topLeft;
    }

    /**
     * @return double - the current top right radius.
     */
    public double getTopRight() {
        return this.topRight;
    }

    /**
     * @return double - the current bottom left radius.
     */
    public double getBottomLeft() {
        return this.bottomLeft;
    }

    /**
     * @return double - the current bottom right radius.
     */
    public double getBottomRight() {
        return this.bottomRight;
    }

}
