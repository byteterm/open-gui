package de.byteterm.opengui.tools.geometry;

/**
 * A bunch of values for describing horizontal positioning and alignment.
 * <p></p>
 * For more information join our community (<a href="https://vogiez.com/server-id" style="color:#1dc2ba">https://vogiez.com/server-id</a>)
 * or our website (<a href="https://byteterm.de/projects/open-gui/" style="color:#1dc2ba">https://byteterm.de/projects/open-gui/</a>).
 * @author Daniel Ramke
 * @since 1.0.0
 */
@SuppressWarnings("unused")
public enum Horizontal {

    /**
     * Represents nothing.
     */
    NOTHING(-1),

    /**
     * Indicates left horizontal position.
     */
    LEFT(0),

    /**
     * Indicates centered horizontal position.
     */
    CENTER(1),

    /**
     * Indicates right horizontal position.
     */
    RIGHT(2);

    private final int index;

    Horizontal(int index) {
        this.index = index;
    }

    /**
     * @return integer - the index number for horizontal direction.
     */
    public int getIndex() {
        return this.index;
    }
}

