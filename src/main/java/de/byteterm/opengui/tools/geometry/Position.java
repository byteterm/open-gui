package de.byteterm.opengui.tools.geometry;

/**
 * This enum is used for the css position
 * @author Daniel Ramke
 * @since 1.0.2
 */
public enum Position {

    ABSOLUTE(0, "absolute"),
    RELATIVE(1, "relative"),
    FIXED(2, "fixed");

    private final int id;
    private final String cssName;

    Position(int id, String cssName) {
        this.id = id;
        this.cssName = cssName;
    }

    /**
     * @return integer - the indexing id of enum.
     */
    public int getId() {
        return this.id;
    }

    /**
     * @return String - the css variable name.
     */
    public String getCssName() {
        return this.cssName;
    }
}
