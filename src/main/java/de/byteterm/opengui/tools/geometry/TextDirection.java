package de.byteterm.opengui.tools.geometry;

/**
 * This enum allowed fonts to display here text in special directions.
 * Note that HORIZONTAL direction is the default preset.
 * @author Daniel Ramke
 * @since 1.0.1
 */
public enum TextDirection {

    HORIZONTAL(0),
    VERTICAL_TOP_DOWN(1),
    VERTICAL_DOWN_TOP(2);

    private final int direction;

    TextDirection(int direction) {
        this.direction = direction;
    }

    /**
     * @return integer - the current direction as id.
     */
    public int getDirection() {
        return this.direction;
    }

}
