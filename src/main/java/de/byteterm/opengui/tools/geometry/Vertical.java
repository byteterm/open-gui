package de.byteterm.opengui.tools.geometry;

/**
 * A bunch of values for describing vertical positioning and alignment.
 * <p></p>
 * For more information join our community (<a href="https://vogiez.com/server-id" style="color:#1dc2ba">https://vogiez.com/server-id</a>)
 * or our website (<a href="https://byteterm.de/projects/open-gui/" style="color:#1dc2ba">https://byteterm.de/projects/open-gui/</a>).
 * @author Daniel Ramke
 * @since 1.0.0
 */
@SuppressWarnings("unused")
public enum Vertical {

    /**
     * Represents nothing.
     */
    NOTHING(-1),

    /**
     * Indicates top vertical position
     */
    TOP(0),

    /**
     * Indicates centered vertical position
     */
    CENTER(1),

    /**
     * Indicates bottom vertical position
     */
    BOTTOM(2);

    private final int index;

    Vertical(int index) {
        this.index = index;
    }

    /**
     * @return integer - the index number for vertical direction.
     */
    public int getIndex() {
        return this.index;
    }
}
