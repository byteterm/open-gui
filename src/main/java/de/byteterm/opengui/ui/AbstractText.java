package de.byteterm.opengui.ui;

import de.byteterm.opengui.Registry;
import de.byteterm.opengui.graphics.font.Faces;
import de.byteterm.opengui.graphics.font.Font;
import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.style.types.FontPattern;
import de.byteterm.opengui.tools.geometry.Alignment;
import de.byteterm.opengui.tools.geometry.TextDirection;

/**
 * This class is the parent of the labeled classes for the OpenGUI Library.
 * You can use this class, but if you need a special calculation than use
 * the labeled interface instance of this.
 * For more information visit our website [LINK].
 * @author Daniel Ramke
 * @since 1.0.2
 * @see Component
 * @see Labeled
 */
@SuppressWarnings("unused")
public abstract class AbstractText extends Component implements Labeled {

    private final FontPattern PATTERN = new FontPattern(Faces.REGULAR.getName(), Color.BLACK, 12, 1, 0, TextDirection.HORIZONTAL);

    private String text;
    private String lastText;
    private int lineGap;

    private Font font;
    private FontPattern pattern;

    private Alignment textAlignment;
    private double textPositionX;
    private double textPositionY;
    private double textAbsoluteX;
    private double textAbsoluteY;
    private double labelWidth;
    private double labelHeight;

    private boolean warp;
    private boolean updated;

    /*
     * *************************************************
     * Constructors
     * *************************************************
     */

    /**
     * This constructor creates a new labeled class component.
     * The constructor called the default constructor.
     * @param text - the text to render.
     */
    public AbstractText(String text) {
        this(text, Registry.getFallbackFont(), null, true);
    }

    /**
     * This constructor creates a new abstract text object.
     * The object contains all need label information.
     * @param text - the text to render.
     * @param font - the text based font.
     * @param pattern - the font design pattern.
     * @param warp - the warp state.
     */
    public AbstractText(String text, Font font, FontPattern pattern, boolean warp) {
        this.setText(text);
        this.setFont(font);
        this.setFontPattern(pattern == null ? PATTERN : pattern);
        this.setWarp(warp);
        this.setTextAlignment(Alignment.NOTHING);
    }

    /*
     * *************************************************
     * Text and last text
     * *************************************************
     */

    /**
     * This method checks a text for his length.
     * If the text length bigger than the given gap size, the text will be shorter.
     * @param text - the new text which need a check.
     * @param gap - the gap length of the text.
     */
    public void setText(String text, int gap) {
        if(text.equals(getText())) return;

        if(gap <= -1) {
            this.setLastText(getText());
            this.text = text;
            this.updated = false;
            return;
        }
        String formatted = text;
        if(text.length() > gap) {
            formatted = text.substring(0, gap);
        }
        this.setLastText(getText());
        this.text = formatted;
        this.updated = false;
    }

    @Override
    public void setText(String text) {
        this.setText(text, -1);
    }

    @Override
    public String getText() {
        return text;
    }

    /**
     * This method set the last text from the #getText() method.
     * @param lastText - the last text.
     */
    protected void setLastText(String lastText) {
        this.lastText = lastText;
    }

    /**
     * @return String - the last text.
     */
    public String getLastText() {
        return lastText;
    }

    /*
     * *************************************************
     * Font
     * *************************************************
     */

    @Override
    public void setFont(String name) {
        Font font = Registry.getFont(name);
        if(font == null) font = Registry.getFallbackFont();
        this.setFont(font);
    }

    @Override
    public void setFont(Font font) {
        if(font == null) font = Registry.getFallbackFont();
        this.font = font;
        this.updated = false;
    }

    @Override
    public Font getFont() {
        return font;
    }

    /*
     * *************************************************
     * Font pattern
     * *************************************************
     */

    /**
     * @param face - the new font face.
     */
    public void setFontFace(String face) {
        if(pattern == null) pattern = PATTERN;
        pattern.setFontFace(face);
        this.updated = false;
    }

    /**
     * @param color - the new text color.
     */
    public void setTextColor(Color color) {
        if(pattern == null) pattern = PATTERN;
        pattern.setTextColor(color);
        this.updated = false;
    }

    /**
     * @param size - the new font size.
     */
    public void setFontSize(double size) {
        if(pattern == null) pattern = PATTERN;
        pattern.setFontSize(size);
        this.updated = false;
    }

    /**
     * @param lineHeight - the new line height.
     */
    public void setLineHeight(double lineHeight) {
        if(pattern == null) pattern = PATTERN;
        pattern.setLineHeight(lineHeight);
        this.updated = false;
    }

    /**
     * @param blur - the new font blur.
     */
    public void setFontBlur(double blur) {
        if(pattern == null) pattern = PATTERN;
        pattern.setFontBlur(blur);
        this.updated = false;
    }

    /**
     * @param direction - the new text direction.
     */
    public void setTextDirection(TextDirection direction) {
        if(pattern == null) pattern = PATTERN;
        pattern.setTextDirection(direction);
        this.updated = false;
    }

    @Override
    public void setFontPattern(FontPattern pattern) {
        if(pattern == null) pattern = PATTERN;
        this.pattern = pattern;
        this.updated = false;
    }

    @Override
    public FontPattern getFontPattern() {
        return pattern;
    }

    /*
     * *************************************************
     * Position / Alignment
     * *************************************************
     */

    @Override
    public void setTextAbsolute(double x, double y) {
        this.textAbsoluteX = x;
        this.textAbsoluteY = y;
    }

    public void setTextPosition(double x, double y) {
        this.setTextPositionX(x);
        this.setTextPositionY(y);
    }


    public void setTextPositionX(double textPositionX) {
        this.textPositionX = textPositionX;
        this.updated = false;
    }

    @Override
    public double getTextPositionX() {
        return textPositionX;
    }

    @Override
    public double getTextAbsoluteX() {
        return textAbsoluteX;
    }


    public void setTextPositionY(double textPositionY) {
        this.textPositionY = textPositionY;
        this.updated = false;
    }

    @Override
    public double getTextPositionY() {
        return textPositionY;
    }

    @Override
    public double getTextAbsoluteY() {
        return textAbsoluteY;
    }

    @Override
    public void setTextAlignment(Alignment textAlignment) {
        this.textAlignment = textAlignment;
        this.updated = false;
    }

    @Override
    public Alignment getTextAlignment() {
        return textAlignment;
    }

    /*
     * *************************************************
     * Text bounds
     * *************************************************
     */

    /**
     * This method set the new label width for the component.
     * The width define the maximal text box width.
     * @param labelWidth - the new width.
     */
    public void setLabelWidth(double labelWidth) {
        double internal = (getWidth() - (getStyle().getPadding().getLeft() + getStyle().getPadding().getRight())) - lineGap;
        if(labelWidth > internal || labelWidth < 0) labelWidth = internal;
        this.labelWidth = labelWidth;
        this.updated = false;
    }

    @Override
    public double getLabelWidth() {
        return labelWidth;
    }

    /**
     * This method set the new label height for the component.
     * The height define the maximal text box height.
     * @param labelHeight - the new height.
     */
    public void setLabelHeight(double labelHeight) {
        double internal = (getHeight() - (getStyle().getPadding().getTop() + getStyle().getPadding().getBottom()));
        if(labelHeight > internal || labelHeight < 0) labelHeight = internal;
        this.labelHeight = labelHeight;
        this.updated = false;
    }

    @Override
    public double getLabelHeight() {
        return labelHeight;
    }

    /*
     * *************************************************
     * Checks
     * *************************************************
     */

    @Override
    public void setWarp(boolean warp) {
        this.warp = warp;
    }

    @Override
    public boolean isWarp() {
        return warp;
    }

    @Override
    public void update() {
        updated = true;
    }

    @Override
    public boolean isUpdated() {
        return updated;
    }

    /*
     * *************************************************
     * Line rows
     * *************************************************
     */

    @Override
    public int getMaxRows() {
        return (int) ((int) getWidth() / (pattern == null ? PATTERN : pattern).getFontSize());
    }

    /*
     * *************************************************
     * Container
     * *************************************************
     */

    @Override
    public Component getContainer() {
        return this;
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
        this.setLabelWidth(width);
    }

    @Override
    public void setHeight(double height) {
        super.setHeight(height);
        this.setLabelHeight(height);
    }
}
