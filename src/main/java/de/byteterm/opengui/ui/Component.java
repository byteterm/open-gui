package de.byteterm.opengui.ui;

import de.byteterm.jlogger.Logger;
import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.style.Style;
import de.byteterm.opengui.style.StyleBuilder;
import de.byteterm.opengui.tools.formal.Bounds;
import de.byteterm.opengui.tools.geometry.Alignment;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Component class, all available elements are created in this class.
 * This concerns for example: Buttons, TextFiled and Labels.
 * Note that our scene system works with elements.
 * That means if you want to create a new element, you have to implement this class as a parent.
 * This class provides each element created with the basic functions so that they can be created.
 * These features include: size, name, position, and children.
 * Since this is one of the main classes in this project, if you have any questions, check out the documentation on our website: [LINK]
 * @author Daniel Ramke
 * @since 1.0.0
 */
@SuppressWarnings("unused")
public abstract class Component {

    private final Logger logger = Logger.getLogger();

    private static final List<String> reservedLocalizedNames = new ArrayList<>();
    private static final List<String> reservedNames = new ArrayList<>();

    /*
     * *************************************************
     * General variables
     * *************************************************
     */

    private final String localizedName;
    private final String family;
    private String name;
    private String group;

    private Component parent;
    private final List<Component> children = new ArrayList<>();

    /*
     * *************************************************
     * Attributes variables
     * *************************************************
     */

    private double width;
    private double height;
    private double oldWidth;
    private double oldHeight;
    private double scale;
    private boolean stretchSizeToParent;

    private double absoluteX;
    private double absoluteY;
    private double layoutX;
    private double layoutY;
    private Alignment alignment;

    /*
     * *************************************************
     * Check variables
     * *************************************************
     */

    private boolean hovered;
    private boolean selected;
    private boolean clicked;
    private boolean dragged;
    private boolean enabled;
    private boolean visible;

    /*
     * *************************************************
     * Information variables
     * *************************************************
     */

    private Bounds bounds;
    private Window window;

    private Style style;
    private Style hoveredStyle;
    private Style selectedStyle;

    /*
     * *************************************************
     * Constructors
     * *************************************************
     */

    /**
     * This constructor creates a new element.
     * You don't need to set thinks here because the localized name is the class name with a count.
     */
    public Component() {
        this(null);
    }

    /**
     * This constructor allows us to set a localized name.
     * If the localized name was null then the name will be set to class name.
     * @param localizedName - the final name can't be duplicated!
     */
    public Component(String localizedName) {
        this.localizedName = this.checkLocalizedNames(localizedName);
        this.setName(getLocalizedName());
        this.family = getClass().getSimpleName().toLowerCase();

        this.style = new StyleBuilder()
                .backgroundColor(Color.GRAY)
                .zIndex(0)
                .build();

        this.setScale(1.0D);
        this.setSize(0);
        this.setWindow(null);
        this.setAbsolutePosition(0, 0);
        this.setAlignment(Alignment.NOTHING);
    }

    /*
     * *************************************************
     * LocalizedName methods
     * *************************************************-
     */

    /**
     * This method checks is the given localizedName already in use or usable.
     * If the localizedName usable the method will skip the most thinks.
     * But if the localizedName null, empty, duplicated or blank, than will this method generate a new localizedName.
     * The Formal: (familyName + - + count) of founded family members.
     * @param localizedName - the final name of this element, default the same as the class name.
     * @return String - the checked or new generated localizedName.
     */
    private String checkLocalizedNames(String localizedName) {
        String result;
        if(localizedName == null || localizedName.isEmpty() || localizedName.isBlank()) {
            result = this.generateLocalizedName();
        } else {
            result = localizedName;
            if(reservedLocalizedNames.contains(result)) {
                result = this.generateLocalizedName();
            }
        }
        reservedLocalizedNames.add(result);
        Collections.sort(reservedLocalizedNames);
        return result;
    }

    /**
     * This method generates localizedNames by the family name and his counts.
     * @return String - new generated localizedName.
     */
    private String generateLocalizedName() {
        String generated;
        int count = 0;
        String family = this.getClass().getSimpleName().toLowerCase();
        for(String localizedNames : reservedLocalizedNames) {
            if(localizedNames.startsWith(family)) {
                count++;
            }
        }
        generated = family + "-" + count;
        return generated;
    }

    /**
     * @return String - the final element name.
     */
    public String getLocalizedName() {
        return this.localizedName;
    }

    /*
     * *************************************************
     * Family, Name and Group methods
     * *************************************************
     */

    /**
     * @return String - final family name is instanceof the simple class name as lowercase.
     */
    public String getFamily() {
        return this.family;
    }

    /**
     * This method set a new name for this component.
     * The name can be used in events to catch the right component.
     * @param name - the name you wish.
     */
    public void setName(String name) {
        this.name = checkName(name);
    }

    /**
     * This method checks the name for us and create new names if it needed.
     * To example if the name (test) was found by another component, then create this method the name (test-1).
     * @param name - the specified wish name.
     * @return String - the generated or allowed name.
     */
    private String checkName(String name) {
        String result;
        if(name == null || name.isEmpty() || name.isBlank()) {
            result = getLocalizedName();
        } else {
            result = name;
        }
        if(reservedNames.contains(result)) {
            String generated;
            int index = 0;
            for(String names : reservedNames) {
                if(names.startsWith(result)) {
                    index++;
                }
            }
            if(index > 0) {
                result = result + "-" + index;
            }
        }
        reservedNames.add(result);
        Collections.sort(reservedNames);
        return result;
    }

    /**
     * @return String - the name of this component to find it easily as human.
     */
    public String getName() {
        return name;
    }

    /**
     * This method set a new group for this element.
     * The group is useful for searching options.
     * @param group - the correct group name.
     */
    public void setGroup(String group) {
        if(group.length() < 3) {
            logger.warn("A group name can't be smaller than 3! Please chose another one.");
            return;
        }
        this.group = group;
    }

    /**
     * @return String - the current group from this element.
     */
    public String getGroup() {
        return group;
    }

    /*
     * *************************************************
     * Children methods
     * *************************************************
     */

    /**
     * This method change the current parent of this component.
     * Note if the parent hasn't contains this component as child, it will add it!
     * @param parent - the component which stored this component.
     */
    protected void setParent(Component parent) {
        Component current = this.parent;
        if(parent == null) {
            if(current != null) {
                current.removeChild(this.getLocalizedName());
            }
            this.parent = null;
            return;
        }
        this.parent = parent;
        if(parent.getChildren(this.getLocalizedName()) == null) {
            parent.addChild(this);
        }
    }

    /**
     * @return Component - the current parent component.
     */
    public Component getParent() {
        return parent;
    }

    /**
     * @return boolean - true if the component a parent.
     */
    public boolean isParent() {
        return !children.isEmpty();
    }

    /**
     * @return boolean - true if the component has a parent.
     */
    public boolean hasParent() {
        return parent != null;
    }

    /**
     * This method added a new child to the children list.
     * Note that this method called the setParent() method.
     * @param component - that's needed to add to the children list.
     */
    public void addChild(@NotNull Component component) {
        if(component instanceof Scene) {
            logger.warn("A component with the type Scene can't added to other components!");
            return;
        }
        if(getChildren(component.getLocalizedName()) != null) {
            logger.warn("This component [ " + component.getLocalizedName() + " ] is already added to the children list!");
            return;
        }
        this.children.add(component);
        component.setParent(this);
    }

    /**
     * This method added a static array to the children list.
     * @param components - the static array list.
     */
    public void addChildren(@NotNull Component... components) {
        this.addChildren(Arrays.asList(components));
    }

    /**
     * This method added an array to the children list.
     * @param components - the array list.
     */
    public void addChildren(@NotNull List<Component> components) {
        if(components.isEmpty()) {
            logger.warn("We can't add a empty list to the children list.");
            return;
        }
        for(Component component : components) {
            this.addChild(component);
        }
    }

    /**
     * This method removed all components which have the same identifier.
     * @param identifier - is the localizedName, familyName or groupName.
     */
    public void removeChild(String identifier) {
        de.byteterm.opengui.tools.Collections.reformComponentList(children, identifier);
    }

    /**
     * This method removed a single component from the children list.
     * @param component - to remove component.
     */
    public void removeChild(@NotNull Component component) {
        this.removeChild(component.getLocalizedName());
    }

    /**
     * This method removed all components of the children list.
     */
    public void removeAllChildren() {
        //Todo: Call event ComponentDestroyEvent.
        this.children.clear();
    }

    /**
     * This method filtered the children list by a stream for the localizedName.
     * @param localizedName - the final component name.
     * @return Component - which was found, can be null.
     */
    public Component getChildren(String localizedName) {
        if(children.isEmpty()) {
            return null;
        }
        Component child = null;
        for(Component component : children) {
            if(component.getLocalizedName().equals(localizedName)) {
                child = component;
                break;
            }
        }
        return child;
    }

    /**
     * @return List<Component> - the complete children list of the current component.
     */
    public List<Component> getChildren() {
        return children;
    }

    /**
     * @return boolean - true if the component have children.
     */
    public boolean hasChildren() {
        return !getChildren().isEmpty();
    }

    /*
     * *************************************************
     * Dimension methods
     * *************************************************
     */

    /**
     * This method change the current width and height.
     * Note that the size is manipulated by the parent, if the stretchToParentSize boolean enabled.
     * @param width - the width of the element.
     * @param height - the height of the element.
     */
    public void setSize(double width, double height) {
        this.setWidth(width);
        this.setHeight(height);
    }

    /**
     * This method change the current width and height, to the same.
     * Note that the size is manipulated by the parent, if the stretchToParentSize boolean enabled.
     * @param size - the width and the height of element.
     */
    public void setSize(double size) {
        this.setSize(size, size);
    }

    /**
     * This method change the current width.
     * Note that the width is manipulated by the parent, if the stretchToParentSize boolean enabled.
     * @param width - the new element width.
     */
    public void setWidth(double width) {
        if(width < 0) {
            width = 0;
        }
        if(stretchSizeToParent && hasParent()) {
            width = getParent().getWidth();
        } else {
            width = width * scale;
        }
        this.oldWidth = getWidth();
        this.width = width;
    }

    /**
     * @return double - the current using width.
     */
    public double getWidth() {
        return width;
    }

    /**
     * @return double - the last width of this component.
     */
    protected double getOldWidth() {
        return oldWidth;
    }

    /**
     * This method change the current height.
     * Note that the height is manipulated by the parent, if the stretchToParentSize boolean enabled.
     * @param height - the new element height.
     */
    public void setHeight(double height) {
        if(height < 0) {
            height = 0;
        }
        if(stretchSizeToParent && hasParent()) {
            height = getParent().getHeight();
        } else {
            height = height * scale;
        }
        this.oldHeight = getHeight();
        this.height = height;
    }

    /**
     * @return double - the current using height.
     */
    public double getHeight() {
        return height;
    }

    /**
     * @return double - the last height of this component.
     */
    protected double getOldHeight() {
        return oldHeight;
    }

    /**
     * @param stretchSizeToParent - allow the element copy the size of parent.
     */
    public void setStretchSizeToParent(boolean stretchSizeToParent) {
        this.stretchSizeToParent = stretchSizeToParent;
    }

    /**
     * @return boolean - the stretch to size state.
     */
    public boolean isStretchSizeToParent() {
        return this.stretchSizeToParent;
    }

    /**
     * This method set the current scale of the component.
     * @param scale - the scale multiply the current size.
     */
    public void setScale(double scale) {
        this.scale = scale;
        this.setSize(getWidth(), getHeight());
    }

    /**
     * @return double - the current component scale.
     */
    public double getScale() {
        return scale;
    }

    /*
     * *************************************************
     * Position methods
     * *************************************************
     */

    /**
     * This method set the current absolute position.
     * @param absoluteX - the x position from the component.
     * @param absoluteY - the y position from the component.
     */
    public void setAbsolutePosition(double absoluteX, double absoluteY) {
        this.setAbsoluteX(absoluteX);
        this.setAbsoluteY(absoluteY);
    }

    /**
     * This method set the current absolute position.
     * @param absolutePosition - the x and y position from the component.
     */
    public void setAbsolutePosition(double absolutePosition) {
        this.setAbsolutePosition(absolutePosition, absolutePosition);
    }

    /**
     * @param absoluteX - change the x location of the element.
     */
    public void setAbsoluteX(double absoluteX) {
        this.absoluteX = absoluteX;
        this.calculateBounds();
    }

    /**
     * @return double - the current x location.
     */
    public double getAbsoluteX() {
        return absoluteX;
    }

    /**
     * @param absoluteY - change the y location of the element.
     */
    public void setAbsoluteY(double absoluteY) {
        this.absoluteY = absoluteY;
        this.calculateBounds();
    }

    /**
     * @return double - the current y location.
     */
    public double getAbsoluteY() {
        return absoluteY;
    }

    /**
     * This method change the current layout position.
     * @param layoutX - the layout x position.
     * @param layoutY - the layout y position.
     */
    public void setLayoutPosition(double layoutX, double layoutY) {
        this.setLayoutX(layoutX);
        this.setLayoutY(layoutY);
    }

    /**
     * This method change the current layout position.
     * @param layoutPosition - the layout x and y position.
     */
    public void setLayoutPosition(double layoutPosition) {
        this.setLayoutPosition(layoutPosition, layoutPosition);
    }

    /**
     * This method change the current x layout position of this component.
     * The layout position is used to move the component inner parent location.
     * If no parent set, the layout position will change the absolute position.
     * @param layoutX - the new x layout position.
     */
    public void setLayoutX(double layoutX) {
        if(hasParent()) {
            double allowed = (parent.getWidth() - getWidth());
            if(layoutX > allowed) {
                layoutX = allowed;
            }
            if(layoutX < 0) {
                layoutX = 0;
            }
        } else {
            this.setAbsoluteX(layoutX);
        }
        this.layoutX = layoutX;
    }

    /**
     * @return double - current layout x position inner parent.
     */
    public double getLayoutX() {
        return layoutX;
    }

    /**
     * This method change the current y layout position of this component.
     * The layout position is used to move the component inner parent location.
     * If no parent set, the layout position will change the absolute position.
     * @param layoutY - the new y layout position.
     */
    public void setLayoutY(double layoutY) {
        if(hasParent()) {
            double allowed = (parent.getHeight() - getHeight());
            if(layoutY > allowed) {
                layoutY = allowed;
            }
            if(layoutY < 0) {
                layoutY = 0;
            }
        } else {
            this.setAbsoluteY(layoutY);
        }
        this.layoutY = layoutY;
    }

    /**
     * @return double - current layout y position inner parent.
     */
    public double getLayoutY() {
        return layoutY;
    }

    /**
     * This method change the alignment of an element.
     * This will only work if the element in a parent.
     * @param alignment - the alignment of the element.
     */
    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }

    /**
     * @return alignment - the current alignment.
     */
    public Alignment getAlignment() {
        return this.alignment;
    }

    /**
     * @return bounds - the current calculated bounds.
     */
    public Bounds getBounds() {
        return bounds;
    }

    /**
     * This method calculated the element bounds by the given locations and the size.
     */
    private void calculateBounds() {
        this.bounds = new Bounds(getAbsoluteX(), getAbsoluteY(), (getAbsoluteX() + getWidth()), (getAbsoluteY() + getHeight()));
    }

    /*
     * *************************************************
     * Window methods
     * *************************************************
     */

    /**
     * This method set the current used window of this component.
     * Note if the component has children, all of there will change here window to the new one.
     * @param window - the using window.
     */
    public void setWindow(Window window) {
        if(hasChildren()) {
            for(Component child : children) {
                child.setWindow(window);
            }
        }
        this.window = window;
    }

    /**
     * @return Window - the current used window.
     */
    public Window getWindow() {
        return window;
    }

    /*
     * *************************************************
     * Style methods
     * *************************************************
     */

    /**
     * This method replaced the current used style with the new given.
     * @param style - style object can be created by StyleBuilder class.
     * @see StyleBuilder
     */
    public void setStyle(Style style) {
        this.style = style;
    }

    /**
     * @return style - the current used style.
     */
    public Style getStyle() {
        return style;
    }

    /*
     * *************************************************
     * State methods
     * *************************************************
     */

    /**
     * @param hovered - set the hovered state.
     */
    public void setHovered(boolean hovered) {
        this.hovered = hovered;
    }

    /**
     * @return boolean - the current hover state.
     */
    public boolean isHovered() {
        return this.hovered;
    }

    /**
     * @param selected - set the selected state.
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /**
     * @return boolean - the current select state.
     */
    public boolean isSelected() {
        return this.selected;
    }

    /**
     * @param clicked - set the clicked state.
     */
    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    /**
     * @return boolean - the current click state.
     */
    public boolean isClicked() {
        return this.clicked;
    }

    /**
     * @param dragged - set the dragged state.
     */
    public void setDragged(boolean dragged) {
        this.dragged = dragged;
    }

    /**
     * @return boolean - the current drag state.
     */
    public boolean isDragged() {
        return this.dragged;
    }

    /**
     * @param enabled - set the enabled state.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return boolean - the current enable state.
     */
    public boolean isEnabled() {
        return this.enabled;
    }

}
