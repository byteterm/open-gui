package de.byteterm.opengui.ui;

import de.byteterm.opengui.graphics.font.Font;
import de.byteterm.opengui.style.types.FontPattern;
import de.byteterm.opengui.tools.geometry.Alignment;

/**
 * This interface controls all text components which implement this interface.
 * The interface declares a class for render usage.
 * If the component send to the renderer than it will be displayed.
 * @author Daniel Ramke
 * @since 1.0.2
 */
@SuppressWarnings("unused")
public interface Labeled {

    /**
     * This method change the current displayed test.
     * The text is the important information for labels.
     * @param text - the new test.
     */
    void setText(String text);

    /**
     * @return String - the current using text.
     */
    String getText();

    /**
     * This method controls the text to warp his lines in multiply rows.
     * If the text greater than the main component width it will break into lines.
     * @param warp - the text warp state.
     */
    void setWarp(boolean warp);

    /**
     * @return boolean - the current warping state.
     */
    boolean isWarp();

    /**
     * This method called that's the label is updated.
     */
    void update();

    /**
     * @return boolean - the labeled update state.
     */
    boolean isUpdated();

    /**
     * @return integer - the current max rows which allowed.
     */
    int getMaxRows();

    /**
     * This method set a new font for the label.
     * The name which is given need checked by the font class.
     * @param name - the specified font name.
     */
    void setFont(String name);

    /**
     * This method set a new font.
     * @param font - the font to set.
     */
    void setFont(Font font);

    /**
     * @return Font - the current label font.
     */
    Font getFont();

    /**
     * This method set a new font design pattern.
     * The font pattern define the render design information.
     * @param pattern - the new font pattern.
     */
    void setFontPattern(FontPattern pattern);

    /**
     * @return FontPattern - the current font pattern.
     */
    FontPattern getFontPattern();

    /**
     * @return double - the current layout x location.
     */
    double getTextPositionX();

    /**
     * @return double - the current layout y location.
     */
    double getTextPositionY();

    /**
     * This method set the current absolute text position.
     * @param x - the new absolute x position.
     * @param y - the new absolute y position.
     */
    void setTextAbsolute(double x, double y);

    /**
     * @return double - the current absolute x position.
     */
    double getTextAbsoluteX();

    /**
     * @return double - the current absolute y position.
     */
    double getTextAbsoluteY();

    /**
     * @return double - the allowed maximal text box width.
     */
    double getLabelWidth();

    /**
     * @return double - the allowed maximal text box height.
     */
    double getLabelHeight();

    /**
     * This method set the text alignment for the label.
     * @param alignment - the new alignment.
     */
    void setTextAlignment(Alignment alignment);

    /**
     * @return Alignment - the current text alignment.
     */
    Alignment getTextAlignment();

    /**
     * @return Component - the holder component of the text box.
     */
    Component getContainer();

}
