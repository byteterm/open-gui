package de.byteterm.opengui.ui;

import de.byteterm.jlogger.Logger;
import de.byteterm.opengui.ui.layout.Layout;
import de.byteterm.opengui.ui.layout.Pane;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * The scene class is the base class for all elements and layouts.
 * Please note that this class is very important for the library and is required for all gui based objects.
 * The scene class is mainly represented in the window class.
 * The class is used to properly order and orient the elements.
 * Note that the methods for adding and removing elements also call the root functions.
 * For more information visit our website: [LINK].
 * @author Daniel Ramke
 * @since 1.0.0
 */
public final class Scene extends Component {

    private final Logger logger = Logger.getLogger();

    private final List<Component> allComponents = new ArrayList<>();
    private Component root;

    /**
     * This constructor creates a new scene with a specified root for the start layout.
     * Note that the default size is the window size.
     * @param root - the root component like panes.
     */
    public Scene(@NotNull Component root) {
        this(root, root.getWidth(), root.getHeight());
    }

    /**
     * This constructor creates a new scene.
     * @param width - specified width of the scene.
     * @param height - specified height of the scene.
     */
    public Scene(double width, double height) {
        this(new Pane(), width, height);
    }

    /**
     * This is the default constructor to create scenes.
     * @param root - the root component for this scene.
     * @param width - the specified width of scene.
     * @param height - the specified height of scene.
     */
    public Scene(@NotNull Component root, double width, double height) {
        if(!(root instanceof Layout)) {
            logger.warn("The given root is not a Layout! Note that layouts recommended as roots!");
        }
        this.setSize(width, height);
        root.setStretchSizeToParent(true);
        this.setRoot(root);
        this.setStretchSizeToParent(true);
    }

    /**
     * This method update all components from the run thread in window.
     * This is needed to draw and update the ui.
     */
    public void update() {
        this.updateAlgorithm();
        if(getWindow() != null) {
            if (root != null) {
                if(root.isStretchSizeToParent()) {
                    root.setSize(getWidth(), getHeight());
                }
            }
            if(allComponents.isEmpty()) return;
            for(Component component : allComponents) {
                stretch(component);
            }
        }
    }

    /**
     * This method checks the current components for his size.
     * @param component - component to check.
     */
    private void stretch(Component component) {
        if(component.hasParent()) {
            Component parent = component.getParent();
            if(component.isStretchSizeToParent()) {
                component.setSize(parent.getWidth(), parent.getHeight());
            }
        }
    }

    /**
     * This method set a new root component.
     * Root components define the using layout for the scene.
     * @param root - the new root component, recommend panes.
     */
    public void setRoot(Component root) {
        if(getRoot() != null && getRoot().getLocalizedName().equals(root.getLocalizedName())) {
            logger.warn("This root component is already in use! [ " + root.getLocalizedName() + " ]");
            return;
        }
        if(getRoot() != null) {
            allComponents.remove(getRoot());
        }
        allComponents.add(root);
        this.root = root;
    }

    /**
     * @return Component - the current used root component.
     */
    public Component getRoot() {
        return root;
    }

    @Override
    public void addChild(@NotNull Component child) {
        if(root != null) {
            root.addChild(child);
        }
        super.addChild(child);
        child.getStyle().setZIndex(getStyle().getZIndex() + 1);
        allComponents.add(child);
        this.controlADD(child);
        allComponents.sort((o1, o2) -> o2.getStyle().getZIndex() - o1.getStyle().getZIndex());
    }

    private void controlADD(Component component) {
        if(component.hasChildren()) {
            for(Component child : component.getChildren()) {
                allComponents.add(child);
                child.getStyle().setZIndex(component.getStyle().getZIndex() + 1);
                if(child.hasChildren()) {
                    this.controlADD(child);
                }
            }
        }
    }

    @Override
    public void addChildren(@NotNull Component... components) {
        if(root != null) {
            root.addChildren(components);
        }
        super.addChildren(components);
    }

    @Override
    public void addChildren(@NotNull List<Component> components) {
        if(root != null) {
            root.addChildren(components);
        }
        super.addChildren(components);
    }

    @Override
    public void removeChild(String identifier) {
        if(root != null) {
            root.removeChild(identifier);
        }
        if(allComponents.contains(getChildren(identifier))) {
            this.controlRemove(getChildren(identifier));
        }
        super.removeChild(identifier);
    }

    private void controlRemove(Component component) {
        if(component.hasChildren()) {
            for(Component child : component.getChildren()) {
                allComponents.remove(child);
                this.controlRemove(child);
            }
        }
        allComponents.remove(component);
    }

    @Override
    public void removeAllChildren() {
        if(root != null) {
            root.removeAllChildren();
        }
        super.removeAllChildren();
    }

    public List<Component> getAllComponents() {
        return allComponents;
    }

    /**
     * This method calculate all element positions and moved the correct alignment.
     * The scene is looking for the correct alignment, if a root loaded, than used the scene the root layout.
     * Layouts define which direction is the element alignment or the orientation.
     */
    private void updateAlgorithm() {
        for(Component component : allComponents) {
            if(root != null && component.getLocalizedName().equals(root.getLocalizedName())) {
                continue;
            }
            if(!(component.hasParent())) {
                component.setAbsolutePosition(component.getAbsoluteX(), component.getAbsoluteY());
                continue;
            }
            //Todo: ignored by fixed and absolute css value (position).
            Component parent = component.getParent();
            double minX = parent.getAbsoluteX();
            double minY = parent.getAbsoluteY();
            double maxX = ((parent.getAbsoluteX() + parent.getWidth()) - (component.getWidth()));
            double maxY = ((parent.getAbsoluteY() + parent.getHeight()) - (component.getHeight()));

            double componentX = minX + component.getLayoutX();
            double componentY = minY + component.getLayoutY();

            componentX = Math.min(componentX, maxX);
            componentX = Math.max(componentX, minX);
            componentY = Math.min(componentY, maxY);
            componentY = Math.max(componentY, minY);

            switch (component.getAlignment()) {
                case TOP_LEFT -> component.setAbsolutePosition(minX, minY);
                case TOP_RIGHT -> component.setAbsolutePosition(maxX, minY);
                case TOP_CENTER -> {
                    double center = minX + (parent.getWidth() / 2) - (component.getWidth() / 2);
                    component.setAbsolutePosition(center, minY);
                }
                case BOTTOM_LEFT -> component.setAbsolutePosition(minX, maxY);
                case BOTTOM_RIGHT -> component.setAbsolutePosition(maxX, maxY);
                case BOTTOM_CENTER -> {
                    double center = minX + (parent.getWidth() / 2) - (component.getWidth() / 2);
                    component.setAbsolutePosition(center, maxY);
                }
                case CENTER_LEFT -> {
                    double centerY = minY + (parent.getHeight() / 2) - (component.getHeight() / 2);
                    component.setAbsolutePosition(minX, centerY);
                }
                case CENTER_RIGHT -> {
                    double centerY = minY + (parent.getHeight() / 2) - (component.getHeight() / 2);
                    component.setAbsolutePosition(maxX, centerY);
                }
                case CENTER -> {
                    double centerX = minX + (parent.getWidth() / 2) - (component.getWidth() / 2);
                    double centerY = minY + (parent.getHeight() / 2) - (component.getHeight() / 2);
                    component.setAbsolutePosition(centerX, centerY);
                }
                case NOTHING -> component.setAbsolutePosition(componentX, componentY);
            }
        }
    }
}
