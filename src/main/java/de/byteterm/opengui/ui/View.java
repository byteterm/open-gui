package de.byteterm.opengui.ui;

import de.byteterm.jlogger.Logger;
import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.graphics.paint.Image;
import de.byteterm.opengui.style.StyleBuilder;
import de.byteterm.opengui.style.types.Border;
import org.jetbrains.annotations.NotNull;

import java.io.File;

/**
 * This class provided functions for view typed components like ImageView or AvatarView.
 * The functions ar needed for these types of components to perform his action.
 * @author Daniel Ramke
 * @since 1.0.2
 * @see Image
 * @see Component
 */
public abstract class View extends Component {

    private final Logger logger = Logger.getLogger();

    private Image viewImage;
    private ViewForm shape;

    /*
     * *************************************************
     * Constructors
     * *************************************************
     */

    /**
     * This constructor generates the default preset for all view types.
     * The background is set to color black for a better effect by hovering.
     * A default view have no borders.
     */
    public View() {
        this.setStyle(new StyleBuilder()
                .borderStyle(Border.Style.NONE)
                .backgroundColor(Color.BLACK)
                .zIndex(0).build());
        this.setShape(ViewForm.CIRCLE);
    }

    /**
     * This abstract method allow us to use the custom ui renderer.
     * Note that the child class need the annotation @CustomRenderer to perform render actions.
     * @param window - the window which is given by the ui renderer.
     */
    public abstract void render(Window window);

    /*
     * *************************************************
     * View methods
     * *************************************************
     */

    /**
     * This method set the image which will displayed in the view component.
     * Views have all time an image but all types have special functions for his work.
     * @param name_or_path - can the image name or path.
     */
    public void setViewImage(@NotNull String name_or_path) {
        Image image = Image.get(name_or_path);
        if(image != null) {
            this.setViewImage(image);
            return;
        }
        if(!name_or_path.contains(File.separator) && !name_or_path.contains("/")) {
            logger.warn("The value [ " + name_or_path + " ] is not a path!");
            return;
        }
        this.setViewImage(Image.create(name_or_path));
    }

    /**
     * This method set the image as raw.
     * The method is used by {@link View#setViewImage(String)} and is marked as protected.
     * @param image - the image object.
     */
    protected void setViewImage(Image image) {
        this.viewImage = image;
    }

    /**
     * @return Image - the current image which is displaying.
     */
    public Image getViewImage() {
        return viewImage;
    }

    /*
     * *************************************************
     * Shape methods
     * *************************************************
     */

    public void setShape(ViewForm shape) {
        this.shape = shape;
    }

    public ViewForm getShape() {
        return shape;
    }

    //Todo: make the fit relevant, currently the view is used the shape width and height.

    /**
     * This enum is created to select the view form.
     * This means the shape of the component like QUAD or CIRCLE.
     * Current available shapes:
     * <ul>
     *    <li>QUAD - to render the view as quad shape.</li>
     *    <li>CIRCLE - to render the view as circle shape.</li>
     *    <li>TRIANGLE - to render the view as triangle shape.</li>
     * </ul>
     */
    public enum ViewForm {
        QUAD,
        CIRCLE,
        TRIANGLE
    }
}
