package de.byteterm.opengui.ui;

import de.byteterm.event.EventAPI;
import de.byteterm.jlogger.Logger;
import de.byteterm.jlogger.level.LogLevel;
import de.byteterm.jlogger.util.ConsoleColor;
import de.byteterm.opengui.OpenGUI;
import de.byteterm.opengui.Registry;
import de.byteterm.opengui.events.component.ButtonClickEvent;
import de.byteterm.opengui.events.component.ComponentScrollEvent;
import de.byteterm.opengui.events.component.MouseEnteredEvent;
import de.byteterm.opengui.events.component.MouseLeavingEvent;
import de.byteterm.opengui.events.window.*;
import de.byteterm.opengui.graphics.RenderPriority;
import de.byteterm.opengui.graphics.Renderer;
import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.graphics.render.UIComponentBorderRenderer;
import de.byteterm.opengui.graphics.render.UIComponentCustomRenderer;
import de.byteterm.opengui.graphics.render.UIComponentTextRenderer;
import de.byteterm.opengui.graphics.render.UiComponentBackgroundRenderer;
import de.byteterm.opengui.system.Monitor;
import de.byteterm.opengui.system.callback.WindowCallbacks.*;
import de.byteterm.opengui.system.input.Action;
import de.byteterm.opengui.system.input.KeyBoard;
import de.byteterm.opengui.system.input.Mouse;
import de.byteterm.opengui.tools.Collections;
import de.byteterm.opengui.tools.formal.Bounds;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.nanovg.NanoVG;
import org.lwjgl.nanovg.NanoVGGL2;
import org.lwjgl.nanovg.NanoVGGL3;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

import java.nio.IntBuffer;
import java.util.*;
import java.util.concurrent.CountDownLatch;

/**
 * Window The window class is one of the most important classes used.
 * It is used to create a window on the monitor.
 * You can then work with a scene in a window, or you can use your own renderer.
 * With the IRenderer interface you can make your own renderer and embed it in the window.
 * Please note that a window can have a scene at the same time.
 * So if you use several scenes, you have to create a list so that they don't get lost.
 * Our window class does not do this by itself!
 * Since this function is more used for games than for normal GUI applications.
 * You can see all further information on our wiki: [LINK].
 * @author Daniel Ramke
 * @since 1.0.0
 */
@SuppressWarnings("unused")
public class Window {

    private final Logger logger = Logger.getLogger();

    /*
     * *************************************************
     * Final variables
     * *************************************************
     */

    private final Thread windowThread;
    private final CountDownLatch wait;
    private final String identifier;
    private final List<Renderer> renderers = new ArrayList<>();
    private final List<Component> components = new ArrayList<>();

    /*
     * *************************************************
     * Attributes variables
     * *************************************************
     */

    private String title;
    private double width;
    private double height;
    private double scale;
    private double positionX;
    private double positionY;
    private Color background;
    private float frameRate;

    /*
     * *************************************************
     * Check variables
     * *************************************************
     */

    private boolean created;
    private boolean terminateAble;
    private boolean decorated;
    private boolean fullscreen;

    private Component hovered;
    private Component selected;

    /*
     * *************************************************
     * Internal variables
     * *************************************************
     */

    private int fps;
    private GLCapabilities capabilities;
    private long windowID;
    private long contextID;
    private Monitor monitor;
    private Scene scene;

    /*
     * *************************************************
     * Callbacks variables
     * *************************************************
     */

    private WindowRefreshCallback refreshCallback;
    private WindowSizeCallback sizeCallback;
    private WindowCloseCallback closeCallback;
    private WindowPositionCallback positionCallback;
    private WindowFocusCallback focusCallback;
    private WindowIconifyCallback iconifyCallback;
    private WindowMaximizedCallback maximizedCallback;
    private FrameBufferSizeCallback frameBufferSizeCallback;

    private KeyCallback keyCallback;
    private CharCallback charCallback;
    private CharModsCallback charModsCallback;

    private MouseButtonCallback mouseButtonCallback;
    private MouseEnteredCallback mouseEnteredCallback;
    private MousePositionCallback mousePositionCallback;
    private ScrollCallback scrollCallback;

    /*
     * *************************************************
     * Constructor
     * *************************************************
     */

    /**
     * This constructor represents the default of window configurations.
     * All of these parameters ar needed to create a functional window.
     * We recommend using the registry class to create a window!
     * Note: The first created window is all time marked as terminateAble.
     * This can be change in the library properties if it not needs.
     * Warning if you change the default marked window, the program will continue its run after closing the window.
     * This can be damaged your computer! Change it on own risk!
     * @param identifier - the thread and final window name.
     * @param title - the current window title.
     * @param width - window width in pixels.
     * @param height - window height in pixels.
     * @param positionX - position x on the desktop to display.
     * @param positionY - position y on the desktop to display.
     * @param terminateAble - allowed the window to shut down a program by closing it.
     * @see Registry
     */
    public Window(@NotNull String identifier, String title, double width, double height
            , double positionX, double positionY, boolean terminateAble) {
        this.wait = new CountDownLatch(1);
        this.identifier = identifier;

        this.title = title;
        this.scale = 1.0D;
        this.width = width;
        this.height = height;
        this.positionX = positionX;
        this.positionY = positionY;

        this.created = false;
        this.decorated = true;
        this.terminateAble = terminateAble;
        this.monitor = Monitor.getPrimaryMonitor();
        this.setScene(new Scene(width, height));
        if(monitor == null) {
            logger.error("No monitor was detected! Make sure you have plugin a monitor!");
            this.windowThread = null;
            return;
        }
        this.frameRate = monitor.getRefreshRate();
        this.setBackground(Color.WHITE);
        if(Registry.getRegisteredWindows().isEmpty()) {
            this.terminateAble = true;
        }

        if(OpenGUI.getProperties().getWindowLimit() > Registry.getRegisteredWindows().size()) {
            Registry.registerWindow(this);
            this.windowThread = new Thread(this::run, identifier);
            this.windowThread.start();
            return;
        }
        this.windowThread = null;
    }

    /*
     * *************************************************
     * Internal methods
     * *************************************************
     */

    /**
     * This initialized the window and set all properties.
     * The window is created by default with decorations.
     */
    private void initialize() {
        GLFW.glfwDefaultWindowHints();
        GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_TRUE);

        this.windowID = GLFW.glfwCreateWindow((int) width, (int) height, title, MemoryUtil.NULL, MemoryUtil.NULL);
        if(windowID == MemoryUtil.NULL) {
            logger.fatal(new IllegalStateException("Can't create window - " + identifier));
        }

        this.registerCallbacks();
        this.registerRenderers();
        this.calculatePosition();

        GLFW.glfwMakeContextCurrent(windowID);
        GLFW.glfwSwapInterval(1);

        logger.info("Initialize window [ " + ConsoleColor.GREEN + identifier + ConsoleColor.RESET + " ] was successfully!");
        logger.debug("Window was created with memory address [ " + ConsoleColor.CYAN + windowID + ConsoleColor.RESET + " ]");
        this.wait.countDown();
        this.created = true;
        EventAPI.callEvent(new WindowCreatedEvent(identifier, this));
    }

    /**
     * This method rendered the elements and scenes to the window.
     * The window can have multi render systems.
     */
    private void update() {
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
        this.refresh();
        if(!isIconified()) {
            GL11.glClearColor(background.getPercentRed(), background.getPercentGreen(), background.getPercentBlue(), background.getPercentAlpha());
            NanoVG.nvgBeginFrame(contextID, (int) width, (int) height, 1f);

            if(scene != null) {
                scene.update();
            }
            if(!renderers.isEmpty()) {
                render();
            }

            NanoVG.nvgRestore(contextID);
            NanoVG.nvgEndFrame(contextID);
        }
        GLFW.glfwSwapBuffers(this.windowID);
        GLFW.glfwPollEvents();
    }

    /**
     * This method rendered the stuff to the window.
     * All registered renderers from all sources will display here.
     */
    private void render() {
        boolean need_sort = false;
        for(Renderer renderer : renderers) {
            if(renderer.changeDetected()) {
                need_sort = true;
                break;
            }
        }
        if(need_sort) {
            renderers.sort(Comparator.comparing(renderer -> renderer.getPriority().getPriority()));
        }
        for(Renderer renderer : renderers) {
            renderer.setChangeDetect(false);
            if(renderer.isInitialized()) {
                renderer.render(this);
                continue;
            }
            renderer.initialize();
        }
    }

    /**
     * This method creates the current thread object.
     */
    private void run() {
        this.initialize();
        this.capabilities = GL.createCapabilities();

        int flags;
        if(OpenGUI.getProperties().isModernNVG()) {
            flags = NanoVGGL3.NVG_STENCIL_STROKES | NanoVGGL3.NVG_ANTIALIAS;
            this.contextID = NanoVGGL3.nvgCreate(flags);
        } else {
            flags = NanoVGGL2.NVG_STENCIL_STROKES | NanoVGGL2.NVG_ANTIALIAS;
            this.contextID = NanoVGGL2.nvgCreate(flags);
        }
        logger.debug("Generate render context for [ "
                + ConsoleColor.GREEN + identifier + ConsoleColor.RESET +
                " ] at address [ "
                + ConsoleColor.CYAN + contextID + ConsoleColor.RESET
                + " ]");

        GL11.glClearColor(background.getPercentRed(), background.getPercentGreen(), background.getPercentBlue(), background.getPercentAlpha());

        List<String> info = new ArrayList<>();
        info.add("OpenGL Version: " + Objects.requireNonNull(GL11.glGetString(GL11.GL_VERSION)).substring(0, 3));
        info.add("Graphic Card: " + GL11.glGetString(GL11.GL_RENDERER));
        info.add("Graphic Provider: " + GL11.glGetString(GL11.GL_VENDOR));
        logger.list(info, "Graphics", ConsoleColor.CYAN, LogLevel.DEBUG);

        GL11.glDisable(GL11.GL_DEPTH_TEST);
        GL11.glDisable(GL11.GL_SCISSOR_TEST);

        GL11.glEnable(GL11.GL_CULL_FACE);
        GL11.glCullFace(GL11.GL_BACK);
        GL11.glFrontFace(GL11.GL_CCW);

        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        GL11.glColorMask(true, true, true, true);
        GL11.glStencilMask(0xffffffff);
        GL11.glStencilOp(GL11.GL_KEEP, GL11.GL_KEEP, GL11.GL_KEEP);
        GL11.glStencilFunc(GL11.GL_ALWAYS, 0, 0xffffffff);

        this.refresh();

        long nanoSecond = 1000_000_000L;
        int frames = 0;
        long frameCounter = 0;
        long lastTime = System.nanoTime();
        final float frameTime = 1.0f / frameRate;
        double uncompressedTime = 0;

        while (!(isClosed())) {
            boolean render = false;
            long startTime = System.nanoTime();
            long passedTime = startTime - lastTime;
            lastTime = startTime;

            uncompressedTime += passedTime / (double) nanoSecond;
            frameCounter += passedTime;

            while (uncompressedTime > frameTime) {
                render = true;
                uncompressedTime -= frameTime;

                if(frameCounter >= nanoSecond) {
                    this.setFps(frames);
                    frames = 0;
                    frameCounter = 0;
                }
            }

            if(render) {
                this.update();
                frames++;
            }
        }

    }

    /**
     * This method refresh the opengl view for the window.
     */
    private void refresh() {
        GL11.glViewport(0, 0, (int) width, (int) height);
    }

    /**
     * This method create all callbacks which a needed for windows.
     * Alle callbacks can be extended.
     */
    private void registerCallbacks() {
        this.sizeCallback = new WindowSizeCallback();
        this.sizeCallback.add(GLFW.glfwSetWindowSizeCallback(windowID, sizeCallback));
        this.sizeCallback.add(this::sizeCallback);

        this.positionCallback = new WindowPositionCallback();
        this.positionCallback.add(GLFW.glfwSetWindowPosCallback(windowID, positionCallback));
        this.positionCallback.add(this::positionCallback);

        this.closeCallback = new WindowCloseCallback();
        this.closeCallback.add(GLFW.glfwSetWindowCloseCallback(windowID, closeCallback));
        this.closeCallback.add(this::closeCallback);

        this.refreshCallback = new WindowRefreshCallback();
        this.refreshCallback.add(GLFW.glfwSetWindowRefreshCallback(windowID, refreshCallback));
        this.refreshCallback.add(this::refreshCallback);

        this.focusCallback = new WindowFocusCallback();
        this.focusCallback.add(GLFW.glfwSetWindowFocusCallback(windowID, focusCallback));
        this.focusCallback.add(this::focusCallback);

        this.iconifyCallback = new WindowIconifyCallback();
        this.iconifyCallback.add(GLFW.glfwSetWindowIconifyCallback(windowID, iconifyCallback));
        this.iconifyCallback.add(this::iconifyCallback);

        this.maximizedCallback = new WindowMaximizedCallback();
        this.maximizedCallback.add(GLFW.glfwSetWindowMaximizeCallback(windowID, maximizedCallback));
        this.maximizedCallback.add(this::maximizedCallback);

        this.frameBufferSizeCallback = new FrameBufferSizeCallback();
        this.frameBufferSizeCallback.add(GLFW.glfwSetFramebufferSizeCallback(windowID, frameBufferSizeCallback));
        this.frameBufferSizeCallback.add(this::frameBufferSizeCallback);

        this.charCallback = new CharCallback();
        this.charCallback.add(GLFW.glfwSetCharCallback(windowID, charCallback));
        this.charCallback.add(this::charCallback);

        this.charModsCallback = new CharModsCallback();
        this.charModsCallback.add(GLFW.glfwSetCharModsCallback(windowID, charModsCallback));
        this.charModsCallback.add(this::charModsCallback);

        this.keyCallback = new KeyCallback();
        this.keyCallback.add(GLFW.glfwSetKeyCallback(windowID, keyCallback));
        this.keyCallback.add(this::keyCallback);
        this.keyCallback.add(KeyBoard::callback);

        this.mousePositionCallback = new MousePositionCallback();
        this.mousePositionCallback.add(GLFW.glfwSetCursorPosCallback(windowID, mousePositionCallback));
        this.mousePositionCallback.add(this::mousePositionCallback);
        this.mousePositionCallback.add(Mouse::positionCallback);

        this.mouseButtonCallback = new MouseButtonCallback();
        this.mouseButtonCallback.add(GLFW.glfwSetMouseButtonCallback(windowID, mouseButtonCallback));
        this.mouseButtonCallback.add(this::mouseButtonCallback);
        this.mouseButtonCallback.add(Mouse::callback);

        this.mouseEnteredCallback = new MouseEnteredCallback();
        this.mouseEnteredCallback.add(GLFW.glfwSetCursorEnterCallback(windowID, mouseEnteredCallback));
        this.mouseEnteredCallback.add(this::mouseEnteredCallback);
        this.mouseEnteredCallback.add(Mouse::enteredCallback);

        this.scrollCallback = new ScrollCallback();
        this.scrollCallback.add(GLFW.glfwSetScrollCallback(windowID, scrollCallback));
        this.scrollCallback.add(this::scrollCallback);
        this.scrollCallback.add(Mouse::scrollCallback);
    }

    /**
     * This method registered the default renders for the ui.
     */
    private void registerRenderers() {
        addRenderer(new UiComponentBackgroundRenderer());
        addRenderer(new UIComponentBorderRenderer());
        addRenderer(new UIComponentTextRenderer());
        addRenderer(new UIComponentCustomRenderer());
    }

    /*
     * *************************************************
     * Important methods
     * *************************************************
     */

    /**
     * This method will wait before the window is created.
     * This is useful for methods that's ar needed contextID or windowID.
     */
    private void await() {
        try {
            this.wait.await();
        } catch (InterruptedException ignored) {}
    }

    /**
     * @return Thread - the running window thread.
     */
    public Thread getWindowThread() {
        return this.windowThread;
    }

    /**
     * @return GLCapabilities - the created opengl context.
     */
    public GLCapabilities getCapabilities() {
        return this.capabilities;
    }

    /**
     * @return string - the final name of the window and thread.
     */
    public String getIdentifier() {
        return this.identifier;
    }

    /**
     * @return long - opengl identifier.
     */
    public long getWindowID() {
        this.await();
        return this.windowID;
    }

    /**
     * @return long - nvg context identifier.
     */
    public long getContextID() {
        this.await();
        return this.contextID;
    }

    /*
     * *************************************************
     * Frame methods
     * *************************************************
     */

    /**
     * This method sets the current frames.
     * @param fps - the current frames set by SYSTEM.
     */
    private void setFps(int fps) {
        this.fps = fps;
    }

    /**
     * @return int - the current rendered frames.
     */
    public int getFps() {
        return this.fps;
    }

    /**
     * @param frameCap - set the frame cap of the window (10 - 1000) [-1 or under = 1000]
     */
    public void setFrameCap(float frameCap) {
        if(frameCap < -1 || frameCap > 1000) {
            frameCap = 1000;
        } else if(frameCap < 10) {
            frameCap = 10;
        }
        this.frameRate = frameCap;
    }

    /*
     * *************************************************
     * Scene methods
     * *************************************************
     */

    /**
     * This method set the current scene.
     * The addComponent method and removeComponent ar called.
     * @param scene - the new scene.
     */
    public void setScene(Scene scene) {
        if(scene == getScene()) {
            return;
        }
        if(getScene() != null) {
            removeComponents(scene);
        }
        addComponent(scene);
        this.scene = scene;
    }

    /**
     * @return Scene - the current used scene.
     */
    public Scene getScene() {
        return scene;
    }

    /*
     * *************************************************
     * Children methods
     * *************************************************
     */

    /**
     * This method adds new components to the window list.
     * If the window contains a scene than will this method
     * add the new components to the scene children list!
     * @param component - the component which need to added.
     */
    public void addComponent(@NotNull Component component) {
        if(scene != null) {
            if(component instanceof Scene) {
                logger.warn("You can't add more scenes!");
                return;
            }
            scene.addChild(component);
            return;
        }
        if(getComponents(component.getLocalizedName()) != null) {
            logger.warn("The component [ " + component.getLocalizedName() + " ] is already exist!");
            return;
        }
        if(component instanceof Scene scene) {
            scene.setWindow(this);
            scene.setSize(width, height);
        }
        components.add(component);
        logger.debug("Added new component [ " + component.getLocalizedName() + " ] to window [ " + getIdentifier() + " ]");
    }

    /**
     * This method add a bunch of components to the window.
     * @param components - the static array of components.
     */
    public void addComponents(@NotNull Component... components) {
        this.addComponents(Arrays.asList(components));
    }

    /**
     * This method add a bunch of components to the window.
     * @param components - the array list of components.
     */
    public void addComponents(List<Component> components) {
        this.components.addAll(components);
    }

    /**
     * This method removed components from the window with the given identifier.
     * The identifier can be the localizedName, familyName or groupName.
     * @param identifier - the given identifier.
     */
    public void removeComponents(@NotNull String identifier) {
        Collections.reformComponentList(components, identifier);
    }

    /**
     * This method removed the given component.
     * @param component - to remove.
     */
    public void removeComponents(@NotNull Component component) {
        this.removeComponents(component.getLocalizedName());
    }

    /**
     * This method searched for components in the window component list.
     * If a component was found by the given identifier, it will be returned.
     * @param identifier - the localizedName from a component.
     * @return Component - the founded object, it can be null.
     */
    public Component getComponents(@NotNull String identifier) {
        Component component = null;
        for(Component components : components) {
            if(components.getLocalizedName().equals(identifier)) {
                component = components;
                break;
            }
        }
        return component;
    }

    /**
     * @return List<Component> - the complete components list.
     */
    public List<Component> getComponents() {
        return components;
    }

    /*
     * *************************************************
     * Renderer methods
     * *************************************************
     */

    /**
     * This method added a new renderer. The renderer can be self created by you!
     * If you need other render stuffs in the window you can use this method.
     * @param renderer - the new render.
     */
    public void addRenderer(@NotNull Renderer renderer) {
        this.addRenderer(renderer, RenderPriority.MODERATE);
    }

    /**
     * This method added a new renderer. The renderer can be self created by you!
     * If you need other render stuffs in the window you can use this method.
     * You can specify your render priority.
     * @param renderer - new renderer object.
     * @param priority - specified priority.
     */
    public void addRenderer(@NotNull Renderer renderer, RenderPriority priority) {
        if(getRenderer(renderer.getName()) != null) {
            logger.warn("This renderer was found and can't be duplicated [ " + renderer.getName() + " ]");
            return;
        }
        renderer.setPriority(priority);
        renderer.setChangeDetect(false);
        renderers.add(renderer);
        logger.debug("Added new renderer [ " + renderer.getName() + " ] with priority [ " + renderer.getPriority() + " ]");
        renderers.sort(Comparator.comparing(value -> value.getPriority().getPriority()));
    }

    /**
     * This method removed an active renderer from the list.
     * Warning!!! The render called the method destroy which delete him.
     * @param renderer - the renderer which will be removed.
     */
    public void removeRenderer(@NotNull Renderer renderer) {
        this.removeRenderer(renderer.getName());
    }

    /**
     * This method removed an active renderer from the list.
     * Warning!!! The render called the method destroy which delete him.
     * @param name - the name of the renderer which need to be removed.
     */
    public void removeRenderer(@NotNull String name) {
        if(getRenderer(name) == null) {
            logger.warn("No renderer with name [ " + name + " ] was found!");
            return;
        }
        Renderer renderer = getRenderer(name);
        renderer.destroy();
        renderers.remove(renderer);
    }

    /**
     * This method returned one renderer which you searched for.
     * @param name - the name of the renderer.
     * @return renderer - the render if he is not found it will return null.
     */
    public Renderer getRenderer(@NotNull String name) {
        Renderer renderer = null;
        for(Renderer renderers : renderers) {
            if(renderers.getName().equals(name)) {
                renderer = renderers;
                break;
            }
        }
        return renderer;
    }

    /**
     * @return List<Renderer> - the current list with the renderers.
     */
    public List<Renderer> getRenderers() {
        return renderers;
    }

    /*
     * *************************************************
     * General methods
     * *************************************************
     */

    /**
     * This method close a window but let the elements alive.
     */
    public void close() {
        GLFW.glfwSetWindowShouldClose(getWindowID(), true);
    }

    /**
     * This method set the window to his default state.
     */
    public void restore() {
        GLFW.glfwRestoreWindow(getWindowID());
    }

    /**
     * This method will show this window.
     */
    public void show() {
        if(!(isVisible())) {
            GLFW.glfwShowWindow(getWindowID());
        }
    }

    /**
     * This method will hide this window.
     */
    public void hide() {
        if(isVisible()) {
            GLFW.glfwHideWindow(getWindowID());
        }
    }

    /**
     * @return boolean - is window showing or hiding.
     */
    public boolean isVisible() {
        return GLFW.glfwGetWindowAttrib(getWindowID(), GLFW.GLFW_VISIBLE) == GLFW.GLFW_TRUE;
    }

    /**
     * @return boolean - will the window next loop closed.
     */
    public boolean isClosed() {
        return GLFW.glfwWindowShouldClose(getWindowID());
    }

    /**
     * @return boolean - is the window finished with the initialing.
     */
    public boolean isCreated() {
        return this.created;
    }

    /*
     * *************************************************
     * Decorations methods
     * *************************************************
     */

    /**
     * This method change the default system style.
     * If this is false the window will be undecorated.
     * If is decorated the y position added +50px.
     * @param decorated - state of decoration.
     */
    public void setDecorated(boolean decorated) {
        this.decorated = decorated;
        if(getWindowID() != MemoryUtil.NULL) {
            GLFW.glfwSetWindowAttrib(getWindowID(), GLFW.GLFW_DECORATED, decorated ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE);
        }
    }

    /**
     * @return boolean - the state of the window is it decorated or not.
     */
    public boolean isDecorated() {
        return this.decorated;
    }

    /**
     * This method set a new background color form rgba.
     * @param red - the red component.
     * @param green - the green component.
     * @param blue - the blue component.
     * @param alpha - the alpha component.
     */
    public void setBackground(int red, int green, int blue, int alpha) {
        this.setBackground(Color.rgba(red, green, blue, alpha));
    }

    /**
     * This method set a new background color form rgb.
     * @param red - the red component.
     * @param green - the green component.
     * @param blue - the blue component.
     */
    public void setBackground(int red, int green, int blue) {
        this.setBackground(Color.rgb(red, green, blue));
    }

    /**
     * This method change the background color from a hexadecimal code.
     * @param hexadecimal - the hexadecimal code.
     */
    public void setBackground(String hexadecimal) {
        this.setBackground(Color.hexadecimal(hexadecimal));
    }

    /**
     * This method set a new background color. A window can change its color all-time.
     * Color codes supported:
     * <ul>
     *     <li>hex - the most used color format. (#ffffff)</li>
     *     <li>rgb - the regular color format. (255, 255, 255)</li>
     *     <li>rgba - the regular color format with alpha. (0, 0, 0, 0.5)</li>
     * </ul>
     * @param background - color class, its stored the color information.
     */
    public void setBackground(Color background) {
        this.background = background;
    }

    /**
     * @return color - the current background color.
     */
    public Color getBackground() {
        return this.background;
    }

    /*
     * *************************************************
     * Position methods
     * *************************************************
     */

    /**
     * This method change the current position of the window.
     * @param positionX - the x location on the virtual desktop.
     * @param positionY - the y location on the virtual desktop.
     */
    public void setPosition(double positionX, double positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.calculatePosition();
    }

    /**
     * This method change the x location of the window.
     * @param positionX - the x location on the virtual desktop.
     */
    public void setPositionX(double positionX) {
        this.positionX = positionX;
        this.calculatePosition();
    }

    /**
     * This method change the y location of the window.
     * @param positionY - the y location on the virtual desktop.
     */
    public void setPositionY(double positionY) {
        this.positionY = positionY;
        this.calculatePosition();
    }

    /**
     * @return double - the current x location from the window.
     */
    public double getPositionX() {
        return this.positionX;
    }

    /**
     * @return double - the current y location from the window.
     */
    public double getPositionY() {
        return this.positionY;
    }

    /**
     * This method triggered the window to check its location on the virtual desktop.
     * You can call this method all time to calculate the new position.
     * Note if you don't change the positions, it will reset it to the last given location.
     */
    public void calculatePosition() {
        double monitorWidth = monitor.getWidth();
        double monitorHeight = monitor.getHeight();

        long windowAddress;
        if(created) {
            windowAddress = this.getWindowID();
        } else {
            windowAddress = windowID;
        }
        try(MemoryStack stack = MemoryStack.stackPush()) {
            IntBuffer currentWidth = stack.callocInt(1);
            IntBuffer currentHeight = stack.callocInt(1);
            GLFW.glfwGetWindowSize(windowAddress, currentWidth, currentHeight);
            if (positionX < 0) {
                this.positionX = (monitorWidth - currentWidth.get()) / 2;
            }
            if(positionY < 0) {
                this.positionY = (monitorHeight - currentHeight.get()) / 2;
            }
            GLFW.glfwSetWindowPos(windowAddress, (int) positionX, (int) positionY);
        }
    }

    /*
     * *************************************************
     * Dimension methods
     * *************************************************
     */

    /**
     * This method set the size for the window.
     * @param width - the new width.
     * @param height - the new height.
     */
    public void setSize(double width, double height) {
        this.setWidth(width);
        this.setHeight(height);
    }

    /**
     * This method set the size of the window.
     * @param size - the new width and the height.
     */
    public void setSize(double size) {
        this.setSize(size, size);
    }

    /**
     * This method set only the window width.
     * @param width - the new window width.
     */
    public void setWidth(double width) {
        width = width * scale;
        GLFW.glfwSetWindowSize(getWindowID(), (int) width, (int) this.height);
        this.width = width;
    }

    /**
     * This method set only the window height.
     * @param height - the new window height.
     */
    public void setHeight(double height) {
        height = height * scale;
        GLFW.glfwSetWindowSize(getWindowID(), (int) this.width, (int) height);
        this.height = height;
    }

    /**
     * @return double - the current window width.
     */
    public double getWidth() {
        return this.width;
    }

    /**
     * @return double - the current window height.
     */
    public double getHeight() {
        return this.height;
    }

    /**
     * This method scaled the window dimension.
     * @param scale - multiply the dimension variables.
     */
    public void setScale(double scale) {
        this.scale = scale;
        this.setSize(width, height);
    }

    /**
     * @return double - the scale number for dimension.
     */
    public double getScale() {
        return this.scale;
    }

    /*
     * *************************************************
     * Options methods
     * *************************************************
     */

    /**
     * This method define the window as important.
     * If a window significant and become the close command, then close all other windows two.
     * By default, is the first created window terminateAble.
     * @param terminateAble - is this window important.
     */
    public void setTerminateAble(boolean terminateAble) {
        this.terminateAble = terminateAble;
    }

    /**
     * @return boolean - true if the window is important.
     */
    public boolean isTerminateAble() {
        return this.terminateAble;
    }

    /**
     * This method change the current window title. The limit is the width - 50px.
     * @param title - the new window title.
     */
    public void setTitle(String title) {
        this.setTitle(title, (int) (width - 50));
    }

    /**
     * This method change the current window title. The title is limited by 50 chars.
     * If the title longer than 50 chars, then will this method convert the title.
     * @param title - the new window title.
     */
    public void setTitle(String title, int limit) {
        this.title = title;
        if(limit > 0 && title.length() > limit) {
            this.title = title.subSequence(0, limit) + "...";
        }
        GLFW.glfwSetWindowTitle(getWindowID(), this.title);
    }

    /**
     * @return string - the current window title.
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * This method maximized the current window object.
     * @param maximized - this window.
     */
    public void setMaximized(boolean maximized) {
        if(maximized) {
            GLFW.glfwMaximizeWindow(getWindowID());
            return;
        }
        this.restore();
    }

    /**
     * @return boolean - the window maximize state.
     */
    public boolean isMaximized() {
        return GLFW.glfwGetWindowAttrib(getWindowID(), GLFW.GLFW_MAXIMIZED) == GLFW.GLFW_TRUE;
    }

    /**
     * This method set the current window iconified.
     * @param iconified - this window.
     */
    public void setIconified(boolean iconified) {
        if(iconified) {
            GLFW.glfwIconifyWindow(getWindowID());
            return;
        }
        this.restore();
    }

    /**
     * @return boolean - the window iconified state.
     */
    public boolean isIconified() {
        return GLFW.glfwGetWindowAttrib(getWindowID(), GLFW.GLFW_ICONIFIED) == GLFW.GLFW_TRUE;
    }

    /**
     * This method focused the current window.
     * This means that the input target this window.
     * @param focused - this window.
     */
    public void setFocused(boolean focused) {
        if(focused) {
            GLFW.glfwFocusWindow(getWindowID());
            return;
        }
        this.restore();
    }

    /**
     * @return boolean - the window focused state.
     */
    public boolean isFocused() {
        return GLFW.glfwGetWindowAttrib(getWindowID(), GLFW.GLFW_FOCUSED) == GLFW.GLFW_TRUE;
    }

    /**
     * This method set the window all time on the top layer of your desktop.
     * @param alwaysOnTop - force always on top.
     */
    public void setAlwaysOnTop(boolean alwaysOnTop) {
        GLFW.glfwSetWindowAttrib(getWindowID(), GLFW.GLFW_FLOATING, alwaysOnTop ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE);
    }

    /**
     * @return boolean - the window always on top state.
     */
    public boolean isAlwaysOnTop() {
        return GLFW.glfwGetWindowAttrib(getWindowID(), GLFW.GLFW_FLOATING) == GLFW.GLFW_TRUE;
    }

    /**
     * This method allowed the user to resize the current window.
     * @param resizeable - state for this window.
     */
    public void setResizeable(boolean resizeable) {
        GLFW.glfwSetWindowAttrib(getWindowID(), GLFW.GLFW_RESIZABLE, resizeable ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE);
    }

    /**
     * @return boolean - the window can resize state.
     */
    public boolean isResizeable() {
        return GLFW.glfwGetWindowAttrib(getWindowID(), GLFW.GLFW_RESIZABLE) == GLFW.GLFW_TRUE;
    }

    /*
     * *************************************************
     * Monitor methods
     * *************************************************
     */

    /**
     * This method change the current monitor which displayed the window.
     * @param monitor - the monitor object which storage the window.
     */
    public void setMonitor(Monitor monitor) {
        if(monitor == null) {
            logger.warn("This monitor was not found!");
            return;
        }
        double tmpX = (monitor.getWidth() - width) / 2;
        double tmpY = (monitor.getHeight() - height) / 2;
        double goTo = 0.0D;
        for(Monitor monitors : Monitor.getMonitors()) {
            if(monitors.getIdentifier().equals(monitor.getIdentifier())) {
                break;
            }
            goTo = goTo + monitors.getWidth();
        }
        this.monitor = monitor;
        this.setPosition((goTo + tmpX), tmpY);
        this.setSize(width, height);
    }

    /**
     * This method change the current monitor which displayed the window.
     * @param identifier - the monitor object which storage the window.
     */
    public void setMonitor(@NotNull String identifier) {
        this.setMonitor(Monitor.getMonitor(identifier));
    }

    /**
     * @return the current used monitor for displaying the window.
     */
    public Monitor getMonitor() {
        return this.monitor;
    }

    /*
     * *************************************************
     * Callbacks methods
     * *************************************************
     */

    /**
     * @return the size callback, is null if the window not initialized.
     */
    public WindowSizeCallback getSizeCallback() {
        return this.sizeCallback;
    }

    /**
     * @return the position callback, is null if the window not initialized.
     */
    public WindowPositionCallback getPositionCallback() {
        return this.positionCallback;
    }

    /**
     * @return the refresh callback, is null if the window not initialized.
     */
    public WindowRefreshCallback getRefreshCallback() {
        return this.refreshCallback;
    }

    /**
     * @return the close callback, is null if the window not initialized.
     */
    public WindowCloseCallback getCloseCallback() {
        return this.closeCallback;
    }

    /**
     * @return the focus callback, is null if the window not initialized.
     */
    public WindowFocusCallback getFocusCallback() {
        return this.focusCallback;
    }

    /**
     * @return the iconify callback, is null if the window not initialized.
     */
    public WindowIconifyCallback getIconifyCallback() {
        return this.iconifyCallback;
    }

    /**
     * @return the maximized callback, is null if the window not initialized.
     */
    public WindowMaximizedCallback getMaximizedCallback() {
        return this.maximizedCallback;
    }

    /**
     * @return the frame buffer callback, is null if the window not initialized.
     */
    public FrameBufferSizeCallback getFrameBufferSizeCallback() {
        return this.frameBufferSizeCallback;
    }

    /**
     * @return the key callback, is null if the window not initialized.
     */
    public KeyCallback getKeyCallback() {
        return this.keyCallback;
    }

    /**
     * @return the char callback, is null if the window not initialized.
     */
    public CharCallback getCharCallback() {
        return this.charCallback;
    }

    /**
     * @return the char mods callback, is null if the window not initialized.
     */
    public CharModsCallback getCharModsCallback() {
        return this.charModsCallback;
    }

    /**
     * @return the mouse button callback, is null if the window not initialized.
     */
    public MouseButtonCallback getMouseButtonCallback() {
        return this.mouseButtonCallback;
    }

    /**
     * @return the mouse entered callback, is null if the window not initialized.
     */
    public MouseEnteredCallback getMouseEnteredCallback() {
        return this.mouseEnteredCallback;
    }

    /**
     * @return the mouse position callback, is null if the window not initialized.
     */
    public MousePositionCallback getMousePositionCallback() {
        return this.mousePositionCallback;
    }

    /**
     * @return the scroll callback, is null if the window not initialized.
     */
    public ScrollCallback getScrollCallback() {
        return this.scrollCallback;
    }

    /*
     * *************************************************
     * Internal callback methods
     * *************************************************
     */

    /**
     * This trigger is called if the window change his current size.
     * @param windowID the window which is resizing.
     * @param width the new width.
     * @param height the new height.
     */
    private void sizeCallback(long windowID, int width, int height) {

    }

    /**
     * This trigger is called if the window change his current position.
     * @param windowID the window which is reposing.
     * @param positionX the new x position on the desktop.
     * @param positionY the new y position on the desktop.
     */
    private void positionCallback(long windowID, int positionX, int positionY) {
        if(created) {
            EventAPI.callEvent(new WindowChangePositionEvent(windowID, positionX, positionY, getPositionX(), getPositionY()));
            this.positionX = positionX;
            this.positionY = positionY;
        }
    }

    /**
     * This trigger is called if the window refreshed by maximize or iconified.
     * @param windowID the window which is refreshed.
     */
    private void refreshCallback(long windowID) {
        EventAPI.callEvent(new WindowRefreshEvent(windowID));
    }

    /**
     * This trigger is called before the window is closing.
     * @param windowID the window which will close.
     */
    private void closeCallback(long windowID) {
        EventAPI.callEvent(new WindowCloseEvent(windowID));
    }

    /**
     * This trigger is called if the window is focused or lost his focus.
     * @param windowID the window which focus.
     * @param focus the current focus state.
     */
    private void focusCallback(long windowID, boolean focus) {
        EventAPI.callEvent(new WindowFocusedEvent(windowID, focus));
    }

    /**
     * This trigger is called if the window set to iconified.
     * @param windowID the window which the iconified state.
     * @param iconify the current state.
     */
    private void iconifyCallback(long windowID, boolean iconify) {
        EventAPI.callEvent(new WindowIconifyEvent(windowID, iconify));
    }

    /**
     * This trigger is called if the window to maximize screen or leave.
     * @param windowID the window which the maximized state.
     * @param maximize the current state
     */
    private void maximizedCallback(long windowID, boolean maximize) {
        EventAPI.callEvent(new WindowMaximizedEvent(windowID, maximize));
    }

    /**
     * This trigger is called if the window change his current size inner box.
     * @param windowID the window which is resizing.
     * @param width the new width.
     * @param height the new height.
     */
    private void frameBufferSizeCallback(long windowID, int width, int height) {
        this.width = width;
        this.height = height;
        if(scene != null) {
            scene.setSize(width, height);
        }
        EventAPI.callEvent(new WindowChangeSizeEvent(windowID, width, height));
    }

    /**
     * This trigger is called if the window detected keyboard input.
     * @param windowID the window which detect key inputs.
     * @param key the key as integer (char)
     * @param scancode the final key id.
     * @param action the current action
     *               <ul>
     *               <li><span style="color: #1ac7b0">GLFW_PRESS</span> this means you only tipped the key.</li>
     *               <li><span style="color: #1ac7b0">GLFW_REPEAT</span> this means you hold the key pressed.</li>
     *               <li><span style="color: #1ac7b0">GLFW_RELEASE</span> this means you released the key.</li>
     *               </ul>
     * @param mods the hotkey which is used example: CTRL + A = 2.
     */
    private void keyCallback(long windowID, int key, int scancode, int action, int mods) {
        boolean controlDown = (mods & GLFW.GLFW_MOD_CONTROL) == GLFW.GLFW_MOD_CONTROL || (mods & GLFW.GLFW_MOD_SUPER) == GLFW.GLFW_MOD_SUPER;
        boolean altDown = (mods & GLFW.GLFW_MOD_ALT) == GLFW.GLFW_MOD_ALT;
        boolean shiftDown = (mods & GLFW.GLFW_MOD_SHIFT) == GLFW.GLFW_MOD_SHIFT;

        if(selected != null) {

        }
    }

    /**
     * This trigger is called if the window detect key input if is a char.
     * @param windowID the window which detect the key input.
     * @param codepoint the char which was tipped.
     */
    private void charCallback(long windowID, int codepoint) {

    }

    /**
     * This trigger is called if the window detect key input if is a char type or mod type.
     * @param windowID the window which detect the key input.
     * @param codepoint the char which was tipped.
     * @param mods the current mod like SHIFT + a = A | 1.
     */
    private void charModsCallback(long windowID, int codepoint, int mods) {

    }

    /**
     * This trigger is called if the window detect mouse inputs.
     * @param windowID the window which detect mouse inputs.
     * @param button the pressed and released button.
     * @param action the current action
     *               <ul>
     *               <li><span style="color: #1ac7b0">GLFW_PRESS</span> this means you only tipped the key.</li>
     *               <li><span style="color: #1ac7b0">GLFW_RELEASE</span> this means you released the key.</li>
     *               </ul>
     * @param mods the hotkey which is used example: CTRL + Button0 = 2.
     */
    private void mouseButtonCallback(long windowID, int button, int action, int mods) {
        if(hovered != null) {
            if(selected != null && !hovered.equals(selected)) {
                selected.setSelected(false);
                selected = hovered;
            }
            if(action == Action.PRESS.getCode()) {
                hovered.setSelected(true);
                selected = hovered;
            }
            EventAPI.callEvent(new ButtonClickEvent(windowID, hovered, button, action, mods));
        }
    }

    /**
     * This trigger is called if the mouse entered the window.
     * @param windowID the window which detect mouse movement.
     * @param positionX the current mouse x position on the window screen.
     * @param positionY the current mouse y position on the window screen.
     */
    private void mousePositionCallback(long windowID, double positionX, double positionY) {
        this.notifyComponents(windowID, positionX, positionY);
    }

    /**
     * This method is internal and controls the hovered components.
     * If a component hovered the variable hovered will be set to this component.
     * @param x - the mouse x location.
     * @param y - the mouse y location.
     * @param windowID - the window mem address.
     * @see de.byteterm.event.Event
     * @see EventAPI
     */
    private void notifyComponents(long windowID, double x, double y) {
        if(scene != null) {
            List<Component> components = scene.getAllComponents();
            for(Component component : components) {
                Bounds bounds = component.getBounds();
                if(bounds.inside(x, y)) {
                    if(hovered != null && !hovered.equals(component)) {
                        if(hovered.isHovered()) {
                            hovered.setHovered(false);
                            EventAPI.callEvent(new MouseLeavingEvent(windowID, hovered));
                        }
                    }
                    if(!component.isHovered()) {
                        hovered = component;
                        component.setHovered(true);
                        EventAPI.callEvent(new MouseEnteredEvent(windowID, component));
                    }
                    break;
                } else {
                    if(component.isHovered()) {
                        component.setHovered(false);
                        hovered = null;
                        EventAPI.callEvent(new MouseLeavingEvent(windowID, component));
                    }
                }
            }
        }
    }

    /**
     * This trigger is called if the mouse entered and leaved the window border.
     * @param windowID the window which detect the entered and the leaving.
     * @param entered the state of the mouse entered.
     */
    private void mouseEnteredCallback(long windowID, boolean entered) {
        EventAPI.callEvent(new WindowMouseEnteredEvent(windowID, entered));
    }

    /**
     * This trigger is called if the mouse on the window and scroll.
     * @param windowID the window which detect the scroll from the entered mouse.
     * @param xOffset the x offset of the scroll.
     * @param yOffset the y offset of the scroll.
     */
    private void scrollCallback(long windowID, double xOffset, double yOffset) {
        Component component = selected != null ? selected : hovered;
        if(component == null) {
            return;
        }
        EventAPI.callEvent(new ComponentScrollEvent(windowID, component, xOffset, yOffset));
    }
}
