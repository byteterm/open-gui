package de.byteterm.opengui.ui.control;

import de.byteterm.jlogger.Logger;
import de.byteterm.jlogger.util.FileUtils;
import de.byteterm.opengui.graphics.CustomRenderer;
import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.graphics.utils.NanoBackground;
import de.byteterm.opengui.style.types.Background;
import de.byteterm.opengui.tools.PathSystem;
import de.byteterm.opengui.tools.formal.Radius;
import de.byteterm.opengui.ui.View;
import de.byteterm.opengui.ui.Window;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * This class is a sup class of View and controls avatars.
 * This is usefully by creating logins, signups or panels.
 * You can generate a custom avatar image by using the method
 * {@link AvatarView#genImageByUserName(String, String, Background, Color)} this will
 * generate a new image by the first and lastname of the user.
 * @author Daniel Ramke
 * @since 1.0.2
 */
@CustomRenderer
public class AvatarView extends View {

    /*
     * Method to allow image direct cut tool usage. This means that the user can
     * cut a big image to the fit size by self. Default is auto and looked for the middle
     * of the given image.
     */

    private final Logger logger = Logger.getLogger();

    private boolean initialize;
    private NanoBackground nanoBackground;

    /**
     * This loaded the default image by create a new object from this.
     * The default image can be change in the assets' folder under "avatar_placeholder.png".
     * Make sure you spell that right before starting the library!
     */
    public AvatarView() {
        this.setViewImage(PathSystem.INTERNAL("/assets/images/avatar_placeholder.png"));
    }

    /**
     * This method creates a new avatar image from the given first and lastname.
     * It will catch the first letters from both of them and make there to uppercase.
     * @param firstname - the users firstname z.b by signup.
     * @param lastname - the users lastname z.b by signup.
     * @param background - the background for the created image.
     * @param textColor - the text color for the generated chars.
     */
    public void genImageByUserName(@NotNull String firstname, @NotNull String lastname, Background background, Color textColor) {
        int width = (int) getWidth();
        int height = (int) getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics2D = image.createGraphics();

        if(lastname.isEmpty() || lastname.isBlank()) lastname = "0";
        if(firstname.isEmpty() || firstname.isBlank()) firstname = "0";
        if(background.getType().equals(Background.Type.IMAGE)) background.setType(Background.Type.COLOR);

        if(background.getType().equals(Background.Type.LINEAR_GRADIENT)) {
            getStyle().setBackgroundGradient(background.getGradient());
            graphics2D.setPaint(Color.getAsAWTColor(Color.transparent));
        } else {
            graphics2D.setPaint(Color.getAsAWTColor(background.getColor()));
        }
        graphics2D.fillRect(0, 0, width, height);

        Font font = new Font("Arial", Font.BOLD, 45);
        graphics2D.setFont(font);
        graphics2D.setPaint(Color.getAsAWTColor(textColor));

        String text = String.valueOf(firstname.charAt(0)).toUpperCase() + String.valueOf(lastname.charAt(0)).toUpperCase();

        TextLayout layout = new TextLayout(text.toUpperCase(), graphics2D.getFont(), graphics2D.getFontRenderContext());
        double textHeight = layout.getBounds().getHeight();
        double textWidth = layout.getBounds().getWidth();

        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        graphics2D.drawString(text, width / 2 - ((int) textWidth / 2) - 4,
                height / 2 + (int) textHeight / 2);

        File output;
        try {
            output = new File(FileUtils.getRunningJarLocation(AvatarView.class).getParent(), "avatar.png");
            ImageIO.write(image, "png", output); //ToDo: Make this as resource and not as physical object.
        } catch (IOException exception) {
            logger.error(exception);
            return;
        }
        this.setViewImage(output.getAbsolutePath());
    }

    @Override
    public void render(Window window) {
        if(!initialize) {
            nanoBackground = new NanoBackground(window);
            initialize = true;
        }

        switch (getShape()) {
            case QUAD -> nanoBackground.drawImage(getAbsoluteX(), getAbsoluteY(), getWidth(), getHeight(), getViewImage(), new Radius(0));
            case CIRCLE -> {
                getStyle().setBackgroundRadius(new Radius(getWidth() / 2));
                getStyle().setBorderRadius(new Radius(getWidth() / 2));
                nanoBackground.drawImage(getAbsoluteX(), getAbsoluteY(), getWidth(), getHeight(), getViewImage(), new Radius(getWidth() / 2));
            }
            case TRIANGLE -> System.out.println("Nothing to do...");
        }
    }
}
