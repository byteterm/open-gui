package de.byteterm.opengui.ui.control;

import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.style.StyleBuilder;
import de.byteterm.opengui.style.types.Border;
import de.byteterm.opengui.tools.formal.Radius;
import de.byteterm.opengui.tools.geometry.Alignment;
import de.byteterm.opengui.ui.ButtonBase;

public class Button extends ButtonBase {

    public Button() {
        this("button");
    }

    public Button(String text) {
        super(text);
        this.setSize(150, 40);
        this.setStyle(new StyleBuilder()
                .backgroundColor(Color.hexadecimal("#e8eded"))
                .borderRadius(new Radius(20))
                .borderColor(Color.hexadecimal("#cfd1d1"))
                .backgroundRadius(new Radius(20))
                .borderStyle(Border.Style.SOLID)
                .build());
        this.setText(text);
        this.setTextAlignment(Alignment.CENTER);
        this.setFontSize(14);
    }
}
