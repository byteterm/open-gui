package de.byteterm.opengui.ui.control;

import de.byteterm.opengui.graphics.CustomRenderer;
import de.byteterm.opengui.graphics.paint.Color;
import de.byteterm.opengui.graphics.utils.NanoBackground;
import de.byteterm.opengui.graphics.utils.NanoBorder;
import de.byteterm.opengui.style.Style;
import de.byteterm.opengui.style.StyleBuilder;
import de.byteterm.opengui.style.types.Border;
import de.byteterm.opengui.tools.formal.Insets;
import de.byteterm.opengui.tools.formal.Radius;
import de.byteterm.opengui.tools.geometry.Alignment;
import de.byteterm.opengui.ui.AbstractText;
import de.byteterm.opengui.ui.Window;

/**
 * This class allowed us to create checkboxes.
 * A checkbox is a component which check user inputs in form of booleans.
 * This means you can only click it, and it will change from true to false or reverse.
 * This component rendered via {@link CustomRenderer} and can be overridden.
 * @author Daniel Ramke
 * @since 1.0.2
 */
@CustomRenderer
@SuppressWarnings("unused")
//Todo: make it more user friendly...
public class CheckBox extends AbstractText {

    private boolean marked;
    private double markedWidth;
    private double markedHeight;
    private boolean initialize;
    private NanoBackground nanoBackground;
    private NanoBorder nanoBorder;
    private Style markStyle;
    private Style markBoxStyle;

    /**
     * This constructor creates a default checkbox component with an example text.
     * The text and marked state can be change all time.
     */
    public CheckBox() {
        this("example text.", false);
    }

    /**
     * This constructor allowed us the absolute needed parameter to change.
     * If you don't know what this parameters doing, then use the empty constructor and
     * see the results on the display.
     * @param text - the text which displaying.
     * @param marked - if it marked or not.
     */
    public CheckBox(String text, boolean marked) {
        super(text);
        this.marked = marked;
        this.setSize(125, 35);
        this.setMarkedSize(25);

        this.setStyle(new StyleBuilder()
                .borderStyle(Border.Style.NONE)
                .backgroundColor(Color.WHITE)
                .padding(new Insets(2, 2))
                .zIndex(0)
                .build());
        this.setMarkBoxStyle(new StyleBuilder()
                .borderStyle(Border.Style.SOLID)
                .backgroundColor(Color.transparent)
                .borderColor(Color.DARK_GRAY)
                .build());
        this.setMarkStyle(new StyleBuilder()
                .borderStyle(Border.Style.NONE)
                .backgroundColor(Color.LIGHT_GRAY)
                .build());
        this.setAlignment(Alignment.NOTHING);
        this.setTextAlignment(Alignment.CENTER_LEFT);
        this.setTextColor(Color.GRAY);
        this.initialize = false;
    }

    /**
     * This method is absolute needed because the class have the annotation {@link CustomRenderer}.
     * The method render is self calling in the class of {@link de.byteterm.opengui.graphics.render.UIComponentCustomRenderer}.
     * @param window - the window which is given by the renderer.
     */
    public void render(Window window) {
        if(!initialize) {
            nanoBackground = new NanoBackground(window);
            nanoBorder = new NanoBorder(window);
            this.initialize = true;
        }

        this.setLabelWidth((getWidth() - getMarkedWidth()) - 14);
        double width = getWidth() - getLabelWidth();
        double height = getHeight();
        double posX = getAbsoluteX() + ((width / 2) - (getMarkedWidth() / 2));
        double posY = getAbsoluteY() + ((height / 2) - (getMarkedHeight() / 2));
        nanoBackground.drawColor(posX, posY , getMarkedWidth(), getMarkedHeight(), getMarkBoxStyle().getBackground().getColor(), new Radius(0));
        nanoBorder.drawColor(posX, posY, getMarkedWidth(), getMarkedHeight(), getMarkBoxStyle().getBorder().getColor(), getMarkBoxStyle().getBorder().getStyle(), new Insets(0.5), new Radius(0));

        double boxWidth = getMarkedWidth() - (getMarkedWidth() / 2);
        double boxHeight = getMarkedHeight() - (getMarkedHeight() / 2);
        double boxX = posX + ((getMarkedWidth() / 2) - (boxWidth / 2));
        double boxY = posY + ((getMarkedHeight() / 2) - (boxHeight / 2));
        nanoBackground.drawColor(boxX, boxY , boxWidth, boxHeight, marked ? getMarkStyle().getBackground().getColor() : Color.transparent, new Radius(0));
    }

    /**
     * This method change the complete style of the mark box.
     * @param markBoxStyle - the new style.
     */
    public void setMarkBoxStyle(Style markBoxStyle) {
        this.markBoxStyle = markBoxStyle;
    }

    /**
     * This method change the mark box background.
     * @param color - the new background color.
     */
    public void setMarkBackgroundColor(Color color) {
        if(getMarkBoxStyle() != null) getMarkBoxStyle().setBackgroundColor(color);
    }

    /**
     * @return Style - the current mark box style.
     */
    public Style getMarkBoxStyle() {
        return markBoxStyle;
    }

    /**
     * This method change the complete style of the mark.
     * @param markStyle - the new style.
     */
    public void setMarkStyle(Style markStyle) {
        this.markStyle = markStyle;
    }

    /**
     * This method change the mark color.
     * @param color - new color.
     */
    public void setMarkColor(Color color) {
        if(getMarkStyle() != null) getMarkStyle().setBackgroundColor(color);
    }

    /**
     * @return Style - the current mark style.
     */
    public Style getMarkStyle() {
        return markStyle;
    }

    /**
     * This method set the mark box sizing.
     * @param size - the new size of the mark box.
     */
    public void setMarkedSize(double size) {
        this.setMarkedWidth(size);
        this.setMarkedHeight(size);
    }

    /**
     * This method change the mark box width.
     * Note that is recommended to use the {@link CheckBox#setMarkedSize(double)} method!
     * @param markedWidth - the new width for the box.
     */
    public void setMarkedWidth(double markedWidth) {
        if((getWidth() / 2) < markedWidth || markedWidth < 0) markedWidth = getWidth() / 2;
        this.markedWidth = markedWidth;
    }

    /**
     * @return double - the current mark box width.
     */
    public double getMarkedWidth() {
        return markedWidth;
    }

    /**
     * This method change the mark box height.
     * Note that is recommended to use the {@link CheckBox#setMarkedSize(double)} method!
     * @param markedHeight - the new height for the box.
     */
    public void setMarkedHeight(double markedHeight) {
        if((getHeight() - 10) < markedHeight || markedWidth < 0) markedHeight = getHeight() - 10;
        this.markedHeight = markedHeight;
    }

    /**
     * @return double - the current mark box height.
     */
    public double getMarkedHeight() {
        return markedHeight;
    }

    /**
     * This method set the marked state of the checkbox.
     * If it trues it's will display an icon in the box.
     * @param marked - the marked state you wish.
     */
    public void setMarked(boolean marked) {
        this.marked = marked;
        this.update();
    }

    /**
     * @return boolean - is the current box marked.
     */
    public boolean isMarked() {
        return marked;
    }
}
