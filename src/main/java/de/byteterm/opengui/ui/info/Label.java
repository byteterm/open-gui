package de.byteterm.opengui.ui.info;

import de.byteterm.opengui.ui.AbstractText;

public class Label extends AbstractText {

    public Label() {
        this("label");
    }

    public Label(String text) {
        super(text);
    }
}
