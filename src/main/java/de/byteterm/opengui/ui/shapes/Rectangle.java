package de.byteterm.opengui.ui.shapes;

import de.byteterm.opengui.tools.geometry.Alignment;
import de.byteterm.opengui.ui.Component;

public class Rectangle extends Component {

    public Rectangle() {
        this.setSize(150, 50);
        this.setAlignment(Alignment.NOTHING);
    }

}
